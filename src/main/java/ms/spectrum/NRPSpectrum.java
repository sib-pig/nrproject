package ms.spectrum;

import com.google.common.base.Preconditions;
import molecules.NRP;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessor;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.core.ms.spectrum.Spectrum;


/**
 * Created by ericart on 23.02.2016.
 */

public class NRPSpectrum extends Spectrum<NRPFragmentAnnotation> {
    private final NRP nrp;
    private final int charge;

    public NRPSpectrum(NRP nrp, int charge, Precision precision) {
        super(0, precision);
        Preconditions.checkNotNull(nrp);
        Preconditions.checkArgument(charge != 0);
        this.nrp= nrp;
        this.charge = charge;
        this.setMsLevel(2);
    }

    public NRPSpectrum(NRP nrp, int charge, int initialCapacity, double constantIntensity, Precision precision) {
        super(initialCapacity, constantIntensity, precision);
        Preconditions.checkNotNull(nrp);
        this.nrp = nrp;
        this.charge = charge;
        this.setMsLevel(2);
    }

    protected NRPSpectrum(NRPSpectrum src, PeakProcessor<NRPFragmentAnnotation, NRPFragmentAnnotation> peakProcessor) {
        super(src, peakProcessor);
        this.nrp = src.nrp;
        this.charge = src.charge;
    }

    protected NRPSpectrum(NRPSpectrum src, PeakProcessorChain<NRPFragmentAnnotation> peakProcessorChain) {
        super(src, peakProcessorChain);
        this.nrp= src.nrp;
        this.charge = src.charge;
    }

    public NRP getNRP() {

        return this.nrp;
    }

    public int getCharge() {

        return this.charge;
    }

    public NRPSpectrum copy(PeakProcessor<NRPFragmentAnnotation, NRPFragmentAnnotation> peakProcessor) {
        return new NRPSpectrum(this, peakProcessor);
    }

    public NRPSpectrum copy(PeakProcessorChain<NRPFragmentAnnotation> peakProcessorChain) {
        return new NRPSpectrum(this, peakProcessorChain);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        NRPSpectrum that = (NRPSpectrum) o;

        if (charge != that.charge) return false;
        return nrp.equals(that.nrp);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + nrp.hashCode();
        result = 31 * result + charge;
        return result;
    }
}
