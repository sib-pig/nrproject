package ms.spectrum.util;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;

import java.text.DecimalFormat;
import java.util.*;


/**
 * Created by ericart on 05.10.2016.
 */


public class MassDifferencesCalculator {


    private Multiset<Double> mzDifferences = HashMultiset.create();
    private DecimalFormat df = new DecimalFormat("#.##");

    public Multiset<Double> calculateDifferences(double [] mzValues){

        Queue<Double> mzsQueue = new PriorityQueue<>();

        for (double d : mzValues){
            mzsQueue.add(d);
        }

        getDifferences(mzsQueue);

        return this.mzDifferences;
    }

    private void getDifferences(Queue<Double>  mzsQueue){

        if (mzsQueue.isEmpty()){
            return;
        }

        Double minMz =  mzsQueue.poll();


        for (Double mz : mzsQueue){
            double difference= mz-minMz;

            double pdifference = Double.parseDouble(this.df.format(difference));
            /*
            if (pdifference==18.01){
                System.out.println("masseees-> "+mz+" "+minMz);
            }
            */
            this.mzDifferences.add(pdifference);

        }
        getDifferences(mzsQueue);

    }

}
