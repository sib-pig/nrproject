package ms.spectrum;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import org.expasy.mzjava.core.ms.spectrum.LibPeakAnnotation;


/**
 * Created by ericart on 23.02.2016.
 */
public class NRPLibPeakAnnotation extends LibPeakAnnotation {
    private final NRPFragmentAnnotation fragmentAnnotation;

    public NRPLibPeakAnnotation(NRPLibPeakAnnotation src) {
        super(src);
        NRPFragmentAnnotation srcFrag = src.fragmentAnnotation;
        this.fragmentAnnotation = srcFrag != null?srcFrag.copy():null;
    }

    public NRPLibPeakAnnotation(int mergedPeakCount, double mzStd, double intensityStd) {
        this(mergedPeakCount, mzStd, intensityStd, Optional.absent());
    }

    public NRPLibPeakAnnotation(int mergedPeakCount, double mzStd, double intensityStd, Optional<NRPFragmentAnnotation> optFragmentAnnotation) {
        super(mergedPeakCount, mzStd, intensityStd);
        Preconditions.checkNotNull(optFragmentAnnotation);
        this.fragmentAnnotation = (NRPFragmentAnnotation)optFragmentAnnotation.orNull();

    }

    public Optional<NRPFragmentAnnotation> getOptFragmentAnnotation() {
        return Optional.fromNullable(this.fragmentAnnotation);
    }

    public NRPLibPeakAnnotation copy() {
        return new NRPLibPeakAnnotation(this);
    }

    public boolean equals(Object o) {
        if(this == o) {
            return true;
        } else if(!(o instanceof NRPLibPeakAnnotation)) {
            return false;
        } else if(!super.equals(o)) {
            return false;
        } else {
            boolean var10000;
            label39: {
                NRPLibPeakAnnotation that = (NRPLibPeakAnnotation)o;
                if(this.fragmentAnnotation != null) {
                    if(this.fragmentAnnotation.equals(that.fragmentAnnotation)) {
                        break label39;
                    }
                } else if(that.fragmentAnnotation == null) {
                    break label39;
                }

                var10000 = false;
                return var10000;
            }

            var10000 = true;
            return var10000;
        }
    }

    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (this.fragmentAnnotation != null?this.fragmentAnnotation.hashCode():0);
        return result;
    }

    public String toString() {
        return "NRPLibPeakAnnotation{mergedPeakCount=" + this.getMergedPeakCount() + ", mzStd=" + this.getMzStd() + ", intensityStd=" + this.getIntensityStd() + ", fragmentAnnotation=" + this.fragmentAnnotation + '}';
    }

    public int getCharge() {
        return this.fragmentAnnotation == null?super.getCharge():this.fragmentAnnotation.getCharge();
    }
}
