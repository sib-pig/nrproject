package ms.spectrum;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Preconditions;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import serializers.NRPFragmentAnnotationSerializer;
import utils.CompositionsJoiner;
import molecules.Monomer;
import molecules.NRPFragment;
import molecules.Compound;
import molecules.modification.Modification;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;

import java.util.*;


/**
 * Created by ericart on 17.03.2016.
 */

public class NRPFragmentAnnotation implements PeakAnnotation{

    private final int charge;
    private final NRPFragment fragment;

    private Composition chargedGroup;
    private Composition neutralLoss;

    private Composition modifications;//calculate it here
    private ListMultimap<Monomer, Modification> modificationPositions;


    public NRPFragmentAnnotation(int charge, NRPFragment fragment, Composition chargedGroup) {
        this(charge,fragment,ArrayListMultimap.create(), chargedGroup, new Composition());

    }

    public NRPFragmentAnnotation(int charge, NRPFragment fragment, ListMultimap<Monomer, Modification> modificationPositions, Composition chargedGroup,Composition neutralLoss) {
        Preconditions.checkNotNull(charge);
        Preconditions.checkNotNull(fragment);
        Preconditions.checkNotNull(modificationPositions);
        Preconditions.checkNotNull(chargedGroup);
        Preconditions.checkNotNull(neutralLoss);

        this.charge = charge;
        this.fragment = fragment;
        this.chargedGroup = chargedGroup;
        this.modificationPositions = modificationPositions;
        this.modifications =CompositionsJoiner.joinCompositions(modificationPositions.values());
        this.neutralLoss = neutralLoss;

    }


    public NRPFragmentAnnotation(NRPFragmentAnnotation src) {
        Preconditions.checkNotNull(src);
        this.charge = src.charge;
        this.fragment = src.fragment;
        this.chargedGroup = src.chargedGroup;
        this.modifications = src.modifications;
        this.modificationPositions=src.modificationPositions;

    }

    private <T extends Compound> Composition generateComposition(ListMultimap<Monomer, T> mapModifications){

        List<Composition> listCompositions = new ArrayList<>();

        for (T adduct : mapModifications.values()){
            listCompositions.add( adduct.getComposition());
        }

        return new Composition(listCompositions.toArray(new Composition[listCompositions.size()]));
    }

    @Override
    public int getCharge() {

        return charge;
    }

    public Composition getChargedGroup() {

        return chargedGroup;

    }

    public NRPFragment getFragment()
    {
        return fragment;
    }


    public Composition getModifications() {

        return modifications;
    }

    public Set<Monomer> getAllModifiedMonomers(){

        return modificationPositions.keySet();
    }

    public List<Modification> getAllModifications(){

        List<Modification> list = new ArrayList<>();
        list.addAll(modificationPositions.values());
        return list;

    }

    public List<Modification> getModifications(Monomer monomer){

        return modificationPositions.get(monomer);

    }

    public Composition getNeutralLoss() {
        return neutralLoss;
    }

    public final NRPFragmentAnnotation copy() {

        return new NRPFragmentAnnotation(this);

    }



}
