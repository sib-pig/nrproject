package ms.spectrum;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.ms.consensus.ConsensusSpectrum;
import org.expasy.mzjava.core.ms.peaklist.PeakSink;


import java.util.List;

/**
 * Created by ericart on 06.06.2016.
 */
public class NRPConsensusPeakSink implements PeakSink<NRPLibPeakAnnotation> {

    private final int minPeakCount;
    private final ConsensusSpectrum<NRPLibPeakAnnotation> consensus;

    public NRPConsensusPeakSink(NRPConsensusSpectrum consensus, double peakFraction, int minPeakCount) {
        this.consensus = consensus;
        int calculatedPeakCount = Math.max(minPeakCount, (int)Math.floor((double)consensus.nrOfMembers() * peakFraction));
        if(consensus.nrOfMembers() == 1) {
            calculatedPeakCount = 1;
        }

        this.minPeakCount = calculatedPeakCount;
    }

    public void start(int size) {
        this.consensus.ensureCapacity(size);
    }

    public void processPeak(double mz, double intensity, List<NRPLibPeakAnnotation> annotations) {
        int peakCount = ((NRPLibPeakAnnotation)annotations.get(0)).getMergedPeakCount();
        Optional annotation = ((NRPLibPeakAnnotation)annotations.get(0)).getOptFragmentAnnotation();

        if(annotation.isPresent()) {
            this.consensus.add(mz, intensity, annotations);
        } else if(peakCount >= this.minPeakCount) {
            this.consensus.add(mz, intensity, annotations);
        }

    }

    public void end() {
        this.consensus.trimToSize();
    }
}
