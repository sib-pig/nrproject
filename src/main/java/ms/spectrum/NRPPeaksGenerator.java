package ms.spectrum;

import com.google.common.base.Predicates;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimaps;
import molecules.Compound;
import molecules.Monomer;
import molecules.NRPFragment;
import molecules.NRPMassCalculator;
import molecules.modification.Modification;
import ms.fragment.NRPFragmentTree;
import org.expasy.mzjava.core.mol.AtomicSymbol;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.AnnotatedPeak;
import utils.CompositionsJoiner;


import java.util.*;

/**
 * Created by ericart on 16.03.2016.
 */

public class NRPPeaksGenerator {

    private NRPFragmentTree simpleDirectedGraph;
    private List<List<ModifiedMonomer>> modifications;
    private List<List<ModifiedMonomer>> ions;
    private List<List<ModifiedMonomer>> neutralLosses;


    private Map<NRPFragment, Set<Composition>> ionsMap;
    private int ionsCharge;
    private Map<NRPFragment, Set<Composition>> NLMap;
    private int maxNL;

    private NRPPeaksGenerator(NRPFragmentTree simpleDirectedGraph, List<List<ModifiedMonomer>> modifications, List<List<ModifiedMonomer>> ions, int ionsCharge,List<List<ModifiedMonomer>> neutralLosses, int maxNL) {

        this.simpleDirectedGraph = simpleDirectedGraph;
        this.modifications = modifications;
        this.ions = ions;
        this.ionsCharge = ionsCharge;
        this.neutralLosses = neutralLosses;
        this.maxNL = maxNL;

    }

    public  List<NRPSpectrum> generatePeaks(){

        List<NRPSpectrum>  containerSpectra = new ArrayList<>();

        setIons();
        setNL();
        if (!modifications.isEmpty()){

            //we get all the possible combinations of modifications in the precursor and we allow multiple modifications in a monomer
            List<List<ModifiedMonomer>> combinations = Combinator.getCombinations(modifications,modifications.size(),false,false);

            //we get change the data structure that stores the combinations list<list> to Listmultimap
            for (List<ModifiedMonomer> combination :combinations) {

                ListMultimap<Monomer, Modification> monomersModifications = ArrayListMultimap.create();

                for (ModifiedMonomer modifiedMonomer : combination){
                    monomersModifications.put(modifiedMonomer.getMonomer(), (Modification) modifiedMonomer.getModification());
                }

                NRPSpectrum spectrum = new NRPSpectrum(simpleDirectedGraph.getNRP(), ionsCharge, PeakList.Precision.DOUBLE);

                for (AnnotatedPeak<NRPFragmentAnnotation> annotatedPeak : getPeaks(monomersModifications) ){
                    spectrum.add(annotatedPeak.getMz(),annotatedPeak.getIntensity(),annotatedPeak.getAnnotations());
                    if (annotatedPeak.getAnnotation(0).getFragment().equals(simpleDirectedGraph.getRoot()) & (annotatedPeak.getAnnotation(0).getNeutralLoss().isEmpty())){
                        spectrum.setPrecursor(new Peak(annotatedPeak.getMz(),100,new int[]{annotatedPeak.getCharge()}));
                    }
                }
                spectrum.setId(UUID.randomUUID());
                containerSpectra.add(spectrum);

            }


        }else{

            ListMultimap<Monomer, Modification> monomersModifications = ArrayListMultimap.create();

            NRPSpectrum spectrum = new NRPSpectrum(simpleDirectedGraph.getNRP(), ionsCharge, PeakList.Precision.DOUBLE);


            for (AnnotatedPeak<NRPFragmentAnnotation> annotatedPeak : getPeaks(monomersModifications) ){

                spectrum.add(annotatedPeak.getMz(),annotatedPeak.getIntensity(),annotatedPeak.getAnnotations());
                if (annotatedPeak.getAnnotation(0).getFragment().equals(simpleDirectedGraph.getRoot()) & (annotatedPeak.getAnnotation(0).getNeutralLoss().isEmpty())){
                    spectrum.setPrecursor(new Peak(annotatedPeak.getMz(),100,new int[]{annotatedPeak.getCharge()}));
                }
            }
            spectrum.setId(UUID.randomUUID());
            containerSpectra.add(spectrum);

        }


        return containerSpectra;
    }

    private List<AnnotatedPeak<NRPFragmentAnnotation>> getPeaks( ListMultimap<Monomer, Modification> monomersModifications) {

        List<AnnotatedPeak<NRPFragmentAnnotation>> containerAnnotatedPeaks = new ArrayList<>();

        for (NRPFragment monomericGraph : this.simpleDirectedGraph.getSetNotRepeatedFragment()) {
            //we obtain the modified monomers of the fragment checking which monomers match with the modifiedMonomers of the precursor
            ListMultimap<Monomer, Modification> matchingModMonomers = Multimaps.filterKeys(monomersModifications, Predicates.in(monomericGraph.getAllMonomerNode()));

            //for each ion combination of a fragment we create a different peak
            for (Composition ionsComposition : this.ionsMap.get(monomericGraph)) {

                for (Composition NLComposition :  this.NLMap.get(monomericGraph)){

                    double fragmentMz=NRPMassCalculator.calculateMz(monomericGraph.getMolecularMass(),ionsComposition,NLComposition.getMolecularMass(), CompositionsJoiner.joinCompositions(matchingModMonomers.values()).getMolecularMass());
                    AnnotatedPeak<NRPFragmentAnnotation> peak = new AnnotatedPeak<>(fragmentMz,100.00, ionsComposition.getCharge(), new NRPFragmentAnnotation(ionsComposition.getCharge(), monomericGraph, matchingModMonomers, ionsComposition,NLComposition));
                    containerAnnotatedPeaks.add(peak);
                }
            }
        }


        return containerAnnotatedPeaks;
    }

    private void setIons(){
        //we get all the possible combinations of ions in the precursor and we don't allow multiple charges in a monomer
        List<List<ModifiedMonomer>> combinations = Combinator.getCombinations(ions,ions.size(),false,true);
        this.ionsMap=getCompositionsMap(combinations, false);
    }

    private void setNL(){
        //we get all the possible combinations of neutralLosses
        List<List<ModifiedMonomer>> combinations = Combinator.getCombinations(neutralLosses,maxNL,true,true);
        this.NLMap=getCompositionsMap(combinations, true);
    }

    private Map<NRPFragment, Set<Composition>> getCompositionsMap(List<List<ModifiedMonomer>> combinations,boolean allowEmptyComp){

        Map<NRPFragment, Set<Composition>> compositionsMap = new HashMap<>();
        for (NRPFragment monomericGraph : this.simpleDirectedGraph.getSetNotRepeatedFragment()) {
            Set<Composition> compositionSet = new HashSet<>();
            for (List<ModifiedMonomer> combination : combinations) {
                List<Composition> listCompositions = new ArrayList<>();

                for (ModifiedMonomer modifiedMonomer : combination) {
                    if (monomericGraph.containsMonomer(modifiedMonomer.getMonomer())) {
                        listCompositions.add(modifiedMonomer.getModification().getComposition());
                    }
                }

                if (!listCompositions.isEmpty()){
                    compositionSet.add(new Composition(listCompositions.toArray(new Composition[listCompositions.size()])));
                }

            }
            if (allowEmptyComp){
                compositionSet.add(new Composition(new Composition[0]));
            }
            compositionsMap.put(monomericGraph, compositionSet);
        }
        return compositionsMap;

    }

    private static class Combinator {

        public static List<List<ModifiedMonomer>> getCombinations(List<List<ModifiedMonomer>> listTotal, int numberComb,boolean numberCombAsRange,boolean removeDuplicates)
        {
            List<List<ModifiedMonomer>> results = new ArrayList<>();
            List<ModifiedMonomer> tempList = new ArrayList<>();

            if (numberCombAsRange){
                for (int i=1;i<=numberComb;i++){
                    combinate(listTotal ,tempList , 0,0,i, results);
                }
            }else{
                combinate(listTotal ,tempList , 0,0,numberComb, results);
            }


            if (removeDuplicates){
                results=removeDuplicates(results);
            }

            return results;

        }

        private static void combinate(List<List<ModifiedMonomer>> listTotal, List<ModifiedMonomer> tempList, int start, int index, int numberComb, List<List<ModifiedMonomer>> results)
        {

            if (index == numberComb)
            {
                List<ModifiedMonomer> result = new ArrayList<>(tempList);
                results.add(result);
                return;
            }

            for (int i = start; i<=listTotal.size() && listTotal.size()-i>=numberComb-index;i++){

                for (ModifiedMonomer object :listTotal.get(i))
                {

                    if(tempList.size()>index){
                        tempList.set(index,object);
                    }else{
                        tempList.add(index,object);
                    }

                    combinate(listTotal, tempList,i+1,index+1,numberComb,results);
                }
            }
        }

        private static List<List<ModifiedMonomer>> removeDuplicates(List<List<ModifiedMonomer>> results){

            List<List<ModifiedMonomer>> combinationsFiltered = new ArrayList<>();
            for (List<ModifiedMonomer> combination : results){
                Set<Monomer> modifiedMonomers = new HashSet<>();
                int mark = 0;
                for (ModifiedMonomer modifiedMonomer : combination){
                    if (modifiedMonomers.contains(modifiedMonomer.getMonomer())){
                        mark=1;
                        break;
                    }else{
                        modifiedMonomers.add(modifiedMonomer.getMonomer());
                    }
                }
                if (mark==0){
                    combinationsFiltered.add(combination);
                }
            }
            return combinationsFiltered;
        }

    }

    public static class Builder{

        private NRPFragmentTree simpleDirectedGraph;
        private List<List<ModifiedMonomer>> modifications = new ArrayList<>();

        private List<List<ModifiedMonomer>> neutralLosses = new ArrayList<>();
        private int maxNL = 2;

        private List<List<ModifiedMonomer>> ions = new ArrayList<>();
        int ionsCharge=0;

        public Builder(NRPFragmentTree simpleDirectedGraph){

            this.simpleDirectedGraph=simpleDirectedGraph;

        }

        public void addVariableModification(Composition varModification, Set<Monomer> targetMonomers){
            List<ModifiedMonomer> modifiedMonomers = new ArrayList<>();
            for (Monomer monomer : targetMonomers){
                modifiedMonomers.add(new ModifiedMonomer(monomer, new Modification(varModification, Modification.ModificationType.VARIABLE)));
            }
            this.modifications.add(modifiedMonomers);
        }

        public void addFixModification(Composition fixModification, Set<Monomer> targetMonomers){

            for (Monomer monomer : targetMonomers){
                List<ModifiedMonomer> modifiedMonomers = new ArrayList<>();
                modifiedMonomers.add(new ModifiedMonomer(monomer,new Modification(fixModification, Modification.ModificationType.FIX)));
                this.modifications.add(modifiedMonomers);
            }

        }

        public void addIon(AtomicSymbol atomicSymbol, int charge, Set<Monomer> targetMonomers) {

            Composition ion = new Composition.Builder().add(atomicSymbol).charge(charge).build();
            List<ModifiedMonomer> modifiedMonomers = new ArrayList<>();

            for (Monomer monomer : targetMonomers){
                modifiedMonomers.add(new ModifiedMonomer(monomer, new Modification(ion, Modification.ModificationType.ION)));
            }
            this.ionsCharge=this.ionsCharge+charge;
            this.ions.add(modifiedMonomers);

        }

        public void addNeutralLoss(Composition neutralLoss, Set<Monomer> targetMonomers) {

            for (Monomer monomer : targetMonomers){
                List<ModifiedMonomer> modifiedMonomers = new ArrayList<>();
                modifiedMonomers.add(new ModifiedMonomer(monomer,new Modification(neutralLoss, Modification.ModificationType.NEUTRAL_LOSS)));
                this.neutralLosses.add(modifiedMonomers);
            }

        }

        public void setMaxNL(int maxNeutralLosses) {

            this.maxNL = maxNeutralLosses;

        }

        //it would be interesting to include "addProton" so it is more practical


        public NRPPeaksGenerator build(){

            if (this.ionsCharge==0){
                throw new IllegalStateException("At least a single ion needs to be added for the peaks generation.");
            }

            if (maxNL>3){
                throw new IllegalStateException("No more than 3 neutral losses per fragment are allowed");
            }

            return new NRPPeaksGenerator(simpleDirectedGraph, modifications, ions, ionsCharge,neutralLosses,maxNL);

        }

    }

    /**
     * Created by ericart on 23.03.2016.
     */
    private static class ModifiedMonomer <T extends Compound> {
        Monomer monomer;
        T modification;

        public ModifiedMonomer(Monomer monomer,T modification) {
            this.monomer = monomer;
            this.modification = modification;
        }

        public Monomer getMonomer() {
            return monomer;
        }

        public T getModification() {
            return modification;
        }
    }


}
