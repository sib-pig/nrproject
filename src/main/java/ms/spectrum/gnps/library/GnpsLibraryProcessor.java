package ms.spectrum.gnps.library;

import io.MgfSpectrumReader;
import org.expasy.mzjava.core.io.ms.spectrum.MgfWriter;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by ericart on 13.10.2016.
 */
public class GnpsLibraryProcessor {

    private static GnpsLibraryProcessor ourInstance = new GnpsLibraryProcessor();;

    private String gnpsProcessedResource=System.getProperty("gnps.processed.library.resource","GNPS-LIBRARY-PROCESSED.mgf");

    private final MgfSpectrumReader mgfSpectrumReader = new MgfSpectrumReader();
    private final List<MsnSpectrum> gnpsSpectra;
    private Set<String> gnpsPeptides = null;

    public static GnpsLibraryProcessor getInstance() {

        return ourInstance;
    }

    private GnpsLibraryProcessor(){
        //System.out.println(this.getClass().getResource(gnpsProcessedResource));
        if (this.getClass().getResource(gnpsProcessedResource)!=null){
            if (!this.getClass().getResource(gnpsProcessedResource).toString().endsWith("-PROCESSED.mgf")){
                throw new IllegalStateException("GNPS library needs to me modified before processing it.");
            }
        }else{
            throw new NullPointerException("GNPS resource not found. Placed by default in gnps/library resource");
        }

        try {

            File gnpsSpectraFile = new File(this.getClass().getResource(gnpsProcessedResource).toURI());
            this.gnpsSpectra = mgfSpectrumReader.readSpectrum(gnpsSpectraFile);

        } catch (URISyntaxException e) {

            throw new IllegalStateException("The GNPS library could not be loaded. Please check the files.", e);

        } catch (IOException e) {

            throw new IllegalStateException("The GNPS library could not be loaded. Please check the files.", e);
        }

    }

    public boolean createMgfFromLib(String nrpName ,String mgfOutputFolder) throws URISyntaxException, IOException {
        int n = 1;

        for (MsnSpectrum msnSpectrum : gnpsSpectra){
            if (msnSpectrum.getComment().toLowerCase().contains(nrpName)){

                String nrpFileName =mgfOutputFolder+msnSpectrum.getComment().replaceAll(" ","_").replace("+","")+"_"+n+".mgf";
                //System.out.println(nrpFileName);
                File nrpFile = new File(nrpFileName);
                MgfWriter mgfWriter = new MgfWriter(nrpFile, PeakList.Precision.DOUBLE);
                mgfWriter.write(msnSpectrum);
                mgfWriter.close();
                n++;
            }
        }

        if (n==1){
            return false;
        }

        return true;

    }

    public  Set<String> getNRPLibset(){

        if(gnpsPeptides==null){
            gnpsPeptides=new HashSet<>();
            for (MsnSpectrum msnSpectrum : gnpsSpectra){
                gnpsPeptides.add(msnSpectrum.getComment().toLowerCase());
            }
        }
        return gnpsPeptides;
    }
}
