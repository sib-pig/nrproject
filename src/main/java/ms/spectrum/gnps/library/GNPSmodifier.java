package ms.spectrum.gnps.library;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ericart on 14.10.2016.
 */
public class GNPSmodifier {

    public void modifyLibrary(String originalLibrary) throws IOException {

        Path path = Paths.get(originalLibrary);
        List<String> fileContent = new ArrayList<>(Files.readAllLines(path, StandardCharsets.UTF_8));

        for (int i = 0; i < fileContent.size(); i++) {
            String line =fileContent.get(i);

            if (line.startsWith("NAME=")) {
                String newline=line.replace("NAME=","TITLE=");

                fileContent.set(i, newline);
            }
        }

        String processedLibrary= originalLibrary.replace(".mgf","")+"-PROCESSED.mgf";
        Path path2 = Paths.get(processedLibrary);

        Files.write(path2, fileContent, StandardCharsets.UTF_8);
    }
}
