package ms.spectrum;

import com.google.common.base.*;
import molecules.NRP;
import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.core.ms.consensus.ConsensusSpectrum;
import org.expasy.mzjava.core.ms.peaklist.*;
import org.expasy.mzjava.core.ms.peaklist.peakfilter.AbstractMergePeakFilter;
import org.expasy.mzjava.core.ms.spectrum.LibPeakAnnotation;
import org.expasy.mzjava.utils.URIBuilder;

import java.net.URI;
import java.util.*;
import java.util.Objects;
import java.util.Optional;

/**
 * Created by ericart on 23.02.2016.
*/

public class NRPConsensusSpectrum extends ConsensusSpectrum<NRPLibPeakAnnotation> {

    private NRP nrp;
    private NRPConsensusSpectrum.Status status;
    private URI spectrumSource;


    public NRPConsensusSpectrum(NRP nrp, Precision precision, Set<UUID> memberIds) {
        super(0, precision, memberIds);
        this.status = NRPConsensusSpectrum.Status.UNKNOWN;
        Preconditions.checkNotNull(nrp);
        this.nrp = nrp;
        this.spectrumSource = URIBuilder.UNDEFINED_URI;
        this.setMsLevel(2);
    }

    public NRP getNRP() {
        return this.nrp;
    }

    public void setNRP(NRP nrp) {
        this.nrp = nrp;
    }

    public String toString() {
        return "LibrarySpectrum{nrp=" + this.nrp + " precursor=" + this.getPrecursor() + '}';
    }

    public void setStatus(NRPConsensusSpectrum.Status status) {
        this.status = status;
    }

    public NRPConsensusSpectrum.Status getStatus() {
        return this.status;
    }

    public URI getSpectrumSource() {
        return this.spectrumSource;
    }

    public void setSpectrumSource(URI spectrumSource) {
        this.spectrumSource = spectrumSource;
    }

    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        NRPConsensusSpectrum that = (NRPConsensusSpectrum) o;
        return Objects.equals(this.nrp, that.nrp) && Arrays.equals(this.getPrecursor().getChargeList(), that.getPrecursor().getChargeList());

    }

    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + Objects.hashCode(this.nrp);
        result = 31 * result + Arrays.hashCode(this.getPrecursor().getChargeList());
        return result;
    }

    public static NRPConsensusSpectrum.ConsensusParametersSetter builder(Precision precision, URI spectrumSource) {
        return new NRPConsensusSpectrum.Builder(precision, spectrumSource);
    }

    public static class Builder implements NRPConsensusSpectrum.ConsensusParametersSetter, NRPConsensusSpectrum.AnnotationParametersSetter, NRPConsensusSpectrum.FilterParametersSetter {
        protected final Precision precision;
        protected final URI spectrumSource;
        protected double maxMzDiff;
        protected double maxMzClusterWidth;
        protected AbstractMergePeakFilter.IntensityMode intensityMode;
        protected Tolerance fragmentTolerance;
        protected int minPeakCount;
        protected double peakFraction;

        protected Builder(Precision precision, URI spectrumSource) {
            this.precision = precision;
            this.spectrumSource = spectrumSource;
        }

        public NRPConsensusSpectrum.FilterParametersSetter setAnnotationParameters(Tolerance fragmentTolerance) {
            Preconditions.checkNotNull(fragmentTolerance);
            this.fragmentTolerance = fragmentTolerance;
            return this;
        }


        public NRPConsensusSpectrum.AnnotationParametersSetter setConsensusParameters(double maxMzDiff, double maxMzClusterWidth, AbstractMergePeakFilter.IntensityMode intensityMode) {
            Preconditions.checkNotNull(intensityMode);
            this.maxMzDiff = maxMzDiff;
            this.maxMzClusterWidth = maxMzClusterWidth;
            this.intensityMode = intensityMode;
            return this;
        }


        public NRPConsensusSpectrum.Builder setFilterParameters(double peakFraction, int minPeakCount) {
            this.minPeakCount = minPeakCount;
            this.peakFraction = peakFraction;
            return this;
        }


        public <A extends PeakAnnotation, S extends PeakList<A>> NRPConsensusSpectrum buildConsensus(int charge, NRP nrp, NRPSpectrum theoreticalSpectrum, Collection<S> spectra) {

            Preconditions.checkNotNull(nrp);
            Preconditions.checkNotNull(spectra);
            HashSet memberIDs = new HashSet();
            if(spectra.isEmpty())
                throw new IllegalStateException("Attempting to build a consensus from spectra collection that is empty");

            for (S spectrum : spectra){
                memberIDs.add(spectrum.getId());
            }

            NRPConsensusSpectrum consensus = new NRPConsensusSpectrum(nrp, this.precision, memberIDs);
            this.calculatePrecursor(consensus,spectra,theoreticalSpectrum.getPrecursor());
            //consensus.setPrecursor(this.calculatePrecursor(charge, theoreticalSpectrum.getPrecursor(), spectra));
            consensus.setSpectrumSource(this.spectrumSource);

            NRPFragmentAnnotator fragmentAnnotator = new NRPFragmentAnnotator(this.fragmentTolerance,theoreticalSpectrum);
            NRPConsensusSpectrum.Builder.MergePeakFilter mergePeakFilter = new NRPConsensusSpectrum.Builder.MergePeakFilter(this.maxMzDiff, this.maxMzClusterWidth, this.intensityMode, spectra.size());

            NRPConsensusPeakSink peakFilter = new NRPConsensusPeakSink(consensus, this.peakFraction, this.minPeakCount);
            fragmentAnnotator.setSink(peakFilter);

            if(spectra.size() > 1) {
                PeakListMerger peakListMerger = new PeakListMerger();
                mergePeakFilter.setSink(fragmentAnnotator);
                peakListMerger.setSink(mergePeakFilter);
                peakListMerger.merge(spectra);
            } else {
                PeakList spectrum = spectra.iterator().next();
                consensus.ensureCapacity(spectrum.size());

                for(int i = 0; i < spectrum.size(); ++i) {
                    fragmentAnnotator.processPeak(spectrum.getMz(i), spectrum.getIntensity(i), Collections.singletonList(mergePeakFilter.createPeakAnnotation(1, 0.0D, 0.0D)));
                }
                consensus.trimToSize();
            }
            /*
            System.out.println(consensus.size());
            System.out.println(consensus.getAnnotations(1));
            double[] k = consensus.getMzs(new double[consensus.size()]);

            System.out.println(Doubles.asList(k));
            System.out.println("NUM ANNOTATIONS "+consensus.getAnnotationIndexes().length);

            System.out.println(consensus.getMzs(new double[consensus.size()]));

            System.out.println(consensus.getAnnotations(0).get(0).getOptFragmentAnnotation().get().getFragment());
            System.out.println(consensus.getAnnotations(1).get(0).getMergedPeakCount());
            System.out.println(consensus.getFirstAnnotation(0).get().getOptFragmentAnnotation().get().getFragment());
            */
            consensus.setStatus(spectra.size() == 1?NRPConsensusSpectrum.Status.SINGLETON:NRPConsensusSpectrum.Status.NORMAL);

            return consensus;

        }

        private void calculatePrecursor(ConsensusSpectrum<? extends LibPeakAnnotation> consensus,Collection<? extends PeakList> spectra, Peak theoreticalPrecursor) {

            double meanIntensity = 0.0D;
            double meanMz = 0.0D;
            double sigmaMz = 0.0D;
            if(!spectra.isEmpty()) {
                int n = 0;

                double delta;
                for(Iterator charge = spectra.iterator(); charge.hasNext(); meanIntensity += delta / (double)n) {

                    PeakList precursor = (PeakList)charge.next();
                    ++n;
                    delta = precursor.getPrecursor().getMz() - meanMz;
                    meanMz += delta / (double)n;
                    sigmaMz += delta * (precursor.getPrecursor().getMz() - meanMz);
                    delta = precursor.getPrecursor().getIntensity() - meanIntensity;
                }

                if(n > 1) {
                    sigmaMz /= (double)(n - 1);
                }

                Peak precursorf = new Peak(theoreticalPrecursor.getMz(), meanIntensity, new int[]{theoreticalPrecursor.getCharge()});
                consensus.setPrecursor(precursorf);
                consensus.setPrecursorStats(meanMz, Math.sqrt(sigmaMz));

            }

        }

/*
        private Peak calculatePrecursor(int charge, Peak precursor, Collection<? extends PeakList> spectra) {
            double sumIntensity = 0.0D;

            PeakList pl;
            for(Iterator i$ = spectra.iterator(); i$.hasNext(); sumIntensity += pl.getPrecursor().getExperimentalIntensity()) {
                pl = (PeakList)i$.next();
            }

            return new Peak(precursor.getExperimentalMz(), sumIntensity, new int[]{charge});
            //return new Peak(nrp.calculateMz(charge), sumIntensity, new int[]{charge}); original

        }

*/
        private static class MergePeakFilter<A extends PeakAnnotation> extends AbstractMergePeakFilter<A, NRPLibPeakAnnotation> {
            public MergePeakFilter(double mzMaxDiff, double maxMzClusterWidth, IntensityMode intCombMeth, int totSpectrumCount) {
                super(mzMaxDiff, maxMzClusterWidth, intCombMeth, totSpectrumCount);
            }

            protected NRPLibPeakAnnotation createPeakAnnotation(int peakCount, double mzStd, double intensityStd) {
                return new NRPLibPeakAnnotation(peakCount, mzStd, intensityStd);
            }
        }
    }

    public interface FilterParametersSetter {
        NRPConsensusSpectrum.Builder setFilterParameters(double var1, int var3);
    }

    public interface AnnotationParametersSetter {
        NRPConsensusSpectrum.FilterParametersSetter setAnnotationParameters(Tolerance var1);
    }

    public interface ConsensusParametersSetter {
        NRPConsensusSpectrum.AnnotationParametersSetter setConsensusParameters(double var1, double var3, AbstractMergePeakFilter.IntensityMode var5);
    }

    public static enum Status {
        UNKNOWN,
        SINGLETON,
        NORMAL,
        INQUORATE_UNCONFIRMED,
        CONFLICTING_ID,
        IMPURE,
        DECOY;

        private Status() {
        }
    }

}

