package ms.spectrum;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.core.ms.peaklist.AbstractPeakProcessor;
import org.expasy.mzjava.core.ms.peaklist.PeakCursor;



import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by ericart on 06.06.2016.
 */
public class NRPFragmentAnnotator  extends AbstractPeakProcessor<NRPLibPeakAnnotation, NRPLibPeakAnnotation> {

    private final PeakCursor<NRPFragmentAnnotation> theoreticalSpectrumCursor;
    private final Tolerance tolerance;

    public NRPFragmentAnnotator(Tolerance tolerance, NRPSpectrum theoreticalSpectrum) {
        this.tolerance = tolerance;
        this.theoreticalSpectrumCursor = theoreticalSpectrum.cursor();
    }

    public void processPeak(double mz, double intensity, List<NRPLibPeakAnnotation> annotations) {
        ArrayList newAnnotations = new ArrayList();

        while(this.theoreticalSpectrumCursor.next()) {
            Tolerance.Location location = this.tolerance.check(mz, this.theoreticalSpectrumCursor.currMz());
            if(location == Tolerance.Location.WITHIN) {
                NRPLibPeakAnnotation libPeakAnnotation = (NRPLibPeakAnnotation)annotations.get(0);
                List fragAnnots = this.theoreticalSpectrumCursor.currAnnotations();
                Iterator i$ = fragAnnots.iterator();

                while(i$.hasNext()) {
                    NRPFragmentAnnotation fragAnnot = (NRPFragmentAnnotation)i$.next();
                    NRPLibPeakAnnotation newAnnot = new NRPLibPeakAnnotation(libPeakAnnotation.getMergedPeakCount(), libPeakAnnotation.getMzStd(), libPeakAnnotation.getIntensityStd(), Optional.of(fragAnnot));
                    newAnnotations.add(newAnnot);
                }
            } else if(location == Tolerance.Location.LARGER) {
                this.theoreticalSpectrumCursor.previous();
            }

            if(location != Tolerance.Location.SMALLER) {
                break;
            }
        }

        if(newAnnotations.size() > 0) {
            this.sink.processPeak(mz, intensity, newAnnotations);
            //System.out.println(newAnnotations);
        } else {
            this.sink.processPeak(mz, intensity, annotations);
        }

    }

}
