package ms.fragment;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import molecules.Bond;
import molecules.Monomer;
import molecules.NRP;
import org.jgrapht.graph.SimpleDirectedGraph;
import molecules.NRPFragment;
import org.jgrapht.graph.SimpleGraph;
import serializers.TreeSerializer;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by ericart on 18.04.2016.
 */
@JsonSerialize(using = TreeSerializer.class)
public class NRPFragmentTree {

    private NRP nrp;
    private NRPFragment root;
    private Map<SimpleGraph<Monomer,Bond>,List<NRPFragment>> graphMap;//map fragmentID listNRPFragments
    private Set<NRPFragment> setNotRepeated;
    private Set<NRPFragment> setUnique;
    private SimpleDirectedGraph<NRPFragment,Loss> tree;


    public NRPFragmentTree(NRP nrp,NRPFragment root,Map<SimpleGraph<Monomer,Bond>,List<NRPFragment>> graphMap,SimpleDirectedGraph<NRPFragment, Loss> tree ) {
        this.nrp= nrp;
        this.root = root;
        this.graphMap = graphMap;
        this.tree = tree;
        setOtherParameters();
    }

    public NRP getNRP() {

        return nrp;
    }


    public NRPFragment getRoot() {

        return root;
    }


    public SimpleDirectedGraph<NRPFragment, Loss> getTree() {

        return tree;
    }

    public Set<NRPFragment> getSetNotRepeatedFragment(){

        return this.setNotRepeated;
    }

    public List<NRPFragment> getRepeats(NRPFragment nrpFragment){
        return graphMap.get(nrpFragment.getSimpleGraph());

    }

    public Set<NRPFragment> getSetUniqueFragment(){

        return this.setUnique;
    }

    public boolean isUnique(NRPFragment nrpFragment){
        if (setUnique.contains(nrpFragment)){
            return true;
        }
        return false;
    }

    //get list of unique
    public void setOtherParameters(){
        int i =0;
        this.setNotRepeated=new HashSet<>();
        this.setUnique=new HashSet<>();
        for (List <NRPFragment> listNRPFrag :graphMap.values()){

            this.setNotRepeated.add(listNRPFrag.get(0));

            if (listNRPFrag.size()==1){
                this.setUnique.add(listNRPFrag.get(0));
            }
            for (NRPFragment nrpFragment : listNRPFrag){
                String graphID=nrpFragment.getPrecursorId().replace("NOR","G")+i;
                nrpFragment.setGraphID(graphID);
            }
            i++;
        }
    }

}
