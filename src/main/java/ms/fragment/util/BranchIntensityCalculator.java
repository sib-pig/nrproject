package ms.fragment.util;

import molecules.NRPFragment;
import ms.fragment.Loss;
import ms.fragment.NRPFragmentTree;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by ericart on 03.10.2016.
 */
public class BranchIntensityCalculator {

    private final NRPFragmentTree nrpFragmentTree;
    private Set<Double> mzMasses;
    private double sumInt;

    public BranchIntensityCalculator(NRPFragmentTree nrpFragmentTree) {
        this.nrpFragmentTree=nrpFragmentTree;

    }
    public double calculateBranchIntensity(NRPFragment nrpFragment){

        this.mzMasses=new HashSet<>();
        this.sumInt=0;

        Queue<NRPFragment> childrenVertex = new ConcurrentLinkedQueue();
        childrenVertex.add(nrpFragment);
        sumIntensities(childrenVertex);

        return this.sumInt;
    }

    private void sumIntensities(Queue<NRPFragment>  childrenVertex){

        if (childrenVertex.isEmpty()){
            return;
        }

        NRPFragment vertex =  childrenVertex.poll();

        if (!mzMasses.contains(vertex.getExperimentalMz())){
            this.sumInt=this.sumInt+vertex.getExperimentalIntensity();
            this.mzMasses.add(vertex.getExperimentalMz());
        }

        for (Loss loss: this.nrpFragmentTree.getTree().outgoingEdgesOf(vertex)){
            childrenVertex.add(this.nrpFragmentTree.getTree().getEdgeTarget(loss));
        }

        sumIntensities(childrenVertex);
    }
}
