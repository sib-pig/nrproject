package ms.fragment;

import molecules.Bond;
import molecules.MonomericGraph;
import org.jgrapht.graph.DefaultEdge;

/**
 * Created by ericart on 10.03.2016.
 */
public class Loss extends DefaultEdge {
    private MonomericGraph monomericGraph1;
    private MonomericGraph monomericGraph2;
    Bond lostEdge;

    public Loss(MonomericGraph m1, MonomericGraph m2, Bond lostEdge) {
        this.monomericGraph1 = m1;
        this.monomericGraph2 = m2;
        this.lostEdge = lostEdge;
    }


    public MonomericGraph getM1() {
        return monomericGraph1;
    }

    public MonomericGraph getM2() {
        return monomericGraph2;
    }

    public Bond getLostEdge() {
        return lostEdge;
    }


    public String toString() {

        return this.lostEdge.toString();
    }
}
