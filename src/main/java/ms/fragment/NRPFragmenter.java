package ms.fragment;

import molecules.*;
import org.jgrapht.alg.ConnectivityInspector;
import org.jgrapht.graph.SimpleDirectedGraph;
import org.jgrapht.graph.SimpleGraph;
import utils.CompositionsJoiner;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by ericart on 04.03.2016.
 */
public class NRPFragmenter {

    NRPFragment root;
    SimpleDirectedGraph<NRPFragment,Loss> tree;
    Map<SimpleGraph<Monomer,Bond>,List<NRPFragment>> graphMap = new HashMap<>();

    int fragmentationStage=0;

    public NRPFragmenter(){

    }

    public NRPFragmentTree createFragmentationTree(NRP nrp){

        SimpleGraph<Monomer,Bond> simpleGraph = nrp.cloneSimpleGraph();
        NRPFragment nrpFragment= new NRPFragment(simpleGraph, nrp.getComposition(),nrp.getNrpId(),fragmentationStage);

        this.root = nrpFragment;
        this.tree = new SimpleDirectedGraph<>(Loss.class);

        List<NRPFragment> nrpFragmentList = new ArrayList<>();
        nrpFragmentList.add(nrpFragment);

        tree.addVertex(nrpFragment);
        graphMap.put(nrpFragment.getSimpleGraph(),nrpFragmentList);
        Queue<NRPFragment> nrpFragmentQueue = new ConcurrentLinkedQueue();
        nrpFragmentQueue.add(nrpFragment);
        addChildren(nrpFragmentQueue);

        return new NRPFragmentTree(nrp,this.root,graphMap,tree);
    }


    //when there is a single parent we use this method
    private void addChildren(Queue<NRPFragment> nrpFragmentQueue){
        //this list is created to store the children graphs created, because they will be the parents for the next round
        //unless they contain a single monomer (cant be split anymore).
        if (nrpFragmentQueue.isEmpty())
            return;

        NRPFragment parentGraph =  nrpFragmentQueue.poll();

        for (Bond targetbond : parentGraph.getAllBond()){

            SimpleGraph<Monomer,Bond> simpleGraph = parentGraph.cloneSimpleGraph();
            simpleGraph.removeEdge(targetbond);
            ConnectivityInspector<Monomer, Bond> connectivityInspector = new ConnectivityInspector<>(simpleGraph);

            List<NRPFragment> childrenList = new ArrayList<>();

            //we check if the removed bond generates 1 or 2 fragments
            if (connectivityInspector.isGraphConnected()){

                NRPFragment childGraph= new NRPFragment(simpleGraph, parentGraph.getComposition(),root.getPrecursorId(),parentGraph.getFragmentationStage()+1);
                childrenList.add(childGraph);

            }else{

                List<Set<Monomer>> connectedSets = connectivityInspector.connectedSets();

                for (Set<Monomer> nodes : connectedSets){
                    SimpleGraph<Monomer,Bond> simpleGraph2 = parentGraph.cloneSimpleGraph();
                    simpleGraph2.removeAllEdges(parentGraph.getBondsSubset(nodes));
                    simpleGraph2.removeAllVertices(nodes);

                    NRPFragment childGraph= new NRPFragment(simpleGraph2, CompositionsJoiner.joinCompositions(simpleGraph2.vertexSet()),root.getPrecursorId(),parentGraph.getFragmentationStage()+1);
                    childrenList.add(childGraph);

                }

            }

            for(NRPFragment childGraph : childrenList){
                SimpleGraph<Monomer,Bond> simpleGraph1 =childGraph.getSimpleGraph();
                List<NRPFragment> nrpFragmentList =graphMap.get(simpleGraph1);
                //if(nrpFragmentList==null || nrpFragmentList.get(0).getFragmentationStage()==childGraph.getFragmentationStage())
                if(nrpFragmentList==null){
                //System.out.println(childGraph.getFragmentationStage());
                //if(childGraph.getFragmentationStage()<=4) {
                    if (nrpFragmentList == null) {
                        List<NRPFragment> nrpFragmentList2 = new ArrayList<>();
                        nrpFragmentList2.add(childGraph);
                        graphMap.put(simpleGraph1, nrpFragmentList2);
                    } else {
                        graphMap.get(simpleGraph1).add(childGraph);
                    }
                    tree.addVertex(childGraph);
                    tree.addEdge(parentGraph, childGraph, new Loss(parentGraph, childGraph, targetbond));
                    //isEndMolecule checks if the graph contains a single monomer
                    if (!childGraph.isEndMolecule()) {
                        nrpFragmentQueue.add(childGraph);
                    }
                //}
                }
            }
        }
        //System.out.println(nrpFragmentQueue.size());
        addChildren(nrpFragmentQueue);
    }


}
