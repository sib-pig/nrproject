package main;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import io.MgfSpectrumReader;
import ms.fragment.Loss;
import ms.fragment.NRPFragmentTree;
import ms.fragment.NRPFragmenter;
import molecules.*;
import molecules.norine.NorineNRPStore;

import ms.fragment.util.BranchIntensityCalculator;
import ms.spectrum.NRPConsensusSpectrum;
import ms.spectrum.NRPLibPeakAnnotation;
import ms.spectrum.NRPPeaksGenerator;
import ms.spectrum.NRPSpectrum;
import org.expasy.mzjava.core.mol.AtomicSymbol;
import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.peakfilter.AbstractMergePeakFilter;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.openscience.cdk.exception.CDKException;


import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Created by ericart on 11.09.2016.
 */
public class MainJSONTreeGenerator {
    public static void main(String[] args) throws IOException, IllegalAccessException, InstantiationException, URISyntaxException, CDKException {
        long startTime = System.currentTimeMillis();

        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();

        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00226"));

        NRPFragmenter NRPFragmenter = new NRPFragmenter();
        NRPFragmentTree simpleDirectedGraph= NRPFragmenter.createFragmentationTree(nrp);

        NRPPeaksGenerator.Builder peaksGeneratorBuilder = new NRPPeaksGenerator.Builder(simpleDirectedGraph);
        peaksGeneratorBuilder.addIon(AtomicSymbol.H,1,nrp.getAllMonomerNode());
        NRPPeaksGenerator peaksGenerator= peaksGeneratorBuilder.build();
        List<NRPSpectrum> peaks =peaksGenerator.generatePeaks();

        URI spectraURI=ClassLoader.getSystemResource("ms/spectrum/lille/Fengycin/F1463.mgf").toURI();
        File spectraFile = new File(spectraURI);
        MgfSpectrumReader mgfSpectrumReader = new MgfSpectrumReader();
        List<MsnSpectrum> listSpectrum = mgfSpectrumReader.readSpectrum(spectraFile);

        NRPConsensusSpectrum nrpConsensusSpectrum = NRPConsensusSpectrum.builder(PeakList.Precision.DOUBLE,spectraURI).
                setConsensusParameters(0.2,0.1, AbstractMergePeakFilter.IntensityMode.MEAN_ALL_INTENSITY).
                setAnnotationParameters(new AbsoluteTolerance(0.1)).
                setFilterParameters(0.5,25).
                buildConsensus(1,nrp,peaks.get(0),listSpectrum);

        //we normalize the intensities for later on calculating the intensity of each branch
        double sumIntensities=0;
        for ( double intensity : nrpConsensusSpectrum.getIntensities(new double[nrpConsensusSpectrum.size()])){
            sumIntensities=sumIntensities+intensity;
        }
        for ( int i : nrpConsensusSpectrum.getAnnotationIndexes()){
            double normalizedIntensity= nrpConsensusSpectrum.getIntensity(i)/sumIntensities;
            nrpConsensusSpectrum.setIntensityAt(normalizedIntensity,i);
        }


        for (int i : nrpConsensusSpectrum.getAnnotationIndexes()){

            for (NRPLibPeakAnnotation nrpLibPeakAnnotation : nrpConsensusSpectrum.getAnnotations(i)){

                if(nrpLibPeakAnnotation.getOptFragmentAnnotation().isPresent()){

                    NRPFragment nrpFragment =nrpLibPeakAnnotation.getOptFragmentAnnotation().get().getFragment();
                    List<NRPFragment> nrpfragmentRepeats=simpleDirectedGraph.getRepeats(nrpFragment);

                    for (NRPFragment nrpFragment1 : nrpfragmentRepeats){
                        nrpFragment1.setState(NRPFragment.State.MATCHED);
                        nrpFragment1.setIntensityMatch(nrpConsensusSpectrum.getIntensity(i));
                        nrpFragment1.setMzMatch(nrpConsensusSpectrum.getMz(i));
                    }
                }
            }
        }

        //we calculate the intensity of each branch
        BranchIntensityCalculator branchIntensityCalculator = new BranchIntensityCalculator(simpleDirectedGraph);
        System.out.println(branchIntensityCalculator.calculateBranchIntensity(simpleDirectedGraph.getRoot()));

        for (Loss loss :simpleDirectedGraph.getTree().outgoingEdgesOf(simpleDirectedGraph.getRoot())){
            System.out.println(branchIntensityCalculator.calculateBranchIntensity(simpleDirectedGraph.getTree().getEdgeTarget(loss)));
        }

        long endTime   = System.currentTimeMillis();
        long totalTime = (endTime - startTime)/ 1000;
        System.out.print("Execution time is " + totalTime + " seconds");

        ObjectMapper mapper = new ObjectMapper();

        File nrpFile = new File("C:\\Users\\ericart\\IdeaProjects\\prova\\web\\tree.json");
        ObjectWriter objectWriter = mapper.writer().withDefaultPrettyPrinter();
        objectWriter.writeValue(nrpFile,simpleDirectedGraph);

    }
}
