package main;



import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;
import io.MgfSpectrumReader;
import molecules.*;

import molecules.norine.NorineNRPStore;
import ms.spectrum.util.MassDifferencesCalculator;
import ms.fragment.NRPFragmentTree;
import ms.fragment.NRPFragmenter;
import ms.spectrum.*;
import org.expasy.mzjava.core.mol.AtomicSymbol;
import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.peakfilter.AbstractMergePeakFilter;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.openscience.cdk.exception.CDKException;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;


/**
 * Created by ericart on 03.11.2015.
 */

    /**
     * This is the modifyLibrary class to create a monomeric graph and fragment in a fragment tree.
     */

    public class MainMonomericFragmentation {

    public static void main(String[] args) throws IOException, IllegalAccessException, InstantiationException, URISyntaxException, CDKException {


        long startTime = System.currentTimeMillis();

        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();

        //we create a monomeric graph of the NRP we are interested in:
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00226"));

        //we fragment the selected NRP in a fragment tree
        NRPFragmenter NRPFragmenter = new NRPFragmenter();
        NRPFragmentTree simpleDirectedGraph= NRPFragmenter.createFragmentationTree(nrp);

        //we generate the peaks selecting the ions, adducts and modifications that we want
        NRPPeaksGenerator.Builder peaksGeneratorBuilder = new NRPPeaksGenerator.Builder(simpleDirectedGraph);
        peaksGeneratorBuilder.addIon(AtomicSymbol.H,1,nrp.getAllMonomerNode());
        NRPPeaksGenerator peaksGenerator= peaksGeneratorBuilder.build();
        List<NRPSpectrum> peaks =peaksGenerator.generatePeaks();

        //we print the theoretical spectra
        printTheoretical(peaks);

        // we read the experimental data:
        MgfSpectrumReader mgfSpectrumReader = new MgfSpectrumReader();
        URI spectraURI=ClassLoader.getSystemResource("ms/spectrum/gnps/Dolastatin_10_MH_1.mgf").toURI();
        File spectraFile = new File(spectraURI);
        List<MsnSpectrum> listSpectrum = mgfSpectrumReader.readSpectrum(spectraFile);
        //MzXmlSpectrumReader mzXmlSpectrumReader = new MzXmlSpectrumReader();
        //List<MsnSpectrum> consensusSpectrum = mzXmlSpectrumReader.readSpectrum();

        //we annotate the spectra
        NRPConsensusSpectrum nrpConsensusSpectrum = NRPConsensusSpectrum.builder(PeakList.Precision.DOUBLE,spectraURI).
                setConsensusParameters(0.2,0.1, AbstractMergePeakFilter.IntensityMode.MEAN_ALL_INTENSITY).
                setAnnotationParameters(new AbsoluteTolerance(0.1)).
                setFilterParameters(0.5,25).
                buildConsensus(1,nrp,peaks.get(0),listSpectrum);


        //we print a summary of the data
        System.out.println("Tree length: "+simpleDirectedGraph.getTree().vertexSet().size());
        System.out.println("Tree without repetitions: "+simpleDirectedGraph.getSetNotRepeatedFragment().size());
        System.out.println("Theoretical spectrum: "+peaks.get(0).size());
        System.out.println("Experimental consensus spectrum: "+nrpConsensusSpectrum.size());


        //steps to do if you want to filter/normalize the spectra
        /*
        RankNormalizer rankNormalizer = new RankNormalizer<>();
        NPeaksFilter topPeaksFilter = new NPeaksFilter(100);
        //NthPeakNormalizer scaleToPeakProcessor = new NthPeakNormalizer(1);
        PeakProcessorChain processorChain = new PeakProcessorChain().add(topPeaksFilter).add(rankNormalizer);
        nrpConsensusSpectrum.apply(processorChain); //To process in place
        //nrpConsensusSpectrum.copy(processorChain); //To create a copy that is processed

        NPeaksFilter topPeaksFilter = new NPeaksFilter(40);
        PeakProcessorChain processorChain = new PeakProcessorChain().add(topPeaksFilter);
        nrpConsensusSpectrum.apply(processorChain); //To process in place

         */

        printConsensusSpectrum(nrpConsensusSpectrum);

        //we print the Xth most frequent mass differences
        printTopMassDifferences(nrpConsensusSpectrum,20);//now it prints only differences smaller than 30Da but can be changed in the print

        //to write the annotated spectra
        /*
        NRPConsensusSpectrumWriter writer = new NRPConsensusSpectrumWriter(com.google.common.base.Optional.of(PeakList.Precision.DOUBLE));
        FileWriter out = new FileWriter("OUTPUT PATH");
        JsonGenerator gen = new JsonFactory().createJsonGenerator(out);
        gen.setPrettyPrinter(new DefaultPrettyPrinter());
        Encoder encoder = EncoderFactory.get().jsonEncoder(writer.createSchema(), gen);
        writer.write(nrpConsensusSpectrum, encoder);
        encoder.flush();
        */

        //to read the annotated spectra
        /*
        InputStream input= new FileInputStream("INPUT PATH");
        NRPConsensusSpectrumReader reader = new NRPConsensusSpectrumReader(
                com.google.common.base.Optional.<PeakList.Precision>absent(),
                Collections.<PeakProcessor<NRPLibPeakAnnotation, NRPLibPeakAnnotation>>emptyList(),new NRPLibPeakAnnotationReader());

        Decoder in = DecoderFactory.get().jsonDecoder(reader.createSchema(),input);
        NRPConsensusSpectrum spectrum = reader.read(in);
        */

    }

    private static void printTheoretical(List<NRPSpectrum> peaks){
        System.out.println("THEORETICAL SPECTRA START");
        for (int i : peaks.get(0).getAnnotationIndexes()){
            System.out.println(peaks.get(0).getMz(i));
            for (NRPFragmentAnnotation i2 : peaks.get(0).getAnnotations(i)){

                System.out.println(i2.getFragment());
            }
            System.out.println("##########");

        }
        System.out.println("THEORETICAL SPECTRA END");
    }

    private static void printConsensusSpectrum(NRPConsensusSpectrum nrpConsensusSpectrum){
        System.out.println("CONSENSUS SPECTRA START");
        int nrpAnnotation=0;
        for (int i : nrpConsensusSpectrum.getAnnotationIndexes()){
            System.out.println(nrpConsensusSpectrum.getMz(i));

            if (nrpConsensusSpectrum.getAnnotations(i).get(0).getOptFragmentAnnotation().isPresent()){
                nrpAnnotation++;
            }

            for (NRPLibPeakAnnotation nrpLibPeakAnnotation : nrpConsensusSpectrum.getAnnotations(i)){

                if(nrpLibPeakAnnotation.getOptFragmentAnnotation().isPresent()){

                    NRPFragment nrpFragment =nrpLibPeakAnnotation.getOptFragmentAnnotation().get().getFragment();
                    System.out.println("MATCHED "+"Theoretical mass: "+nrpFragment.getMolecularMass()+" Fragment: "+nrpFragment);

                }else{
                    System.out.println("UNMATCHED");
                }
            }
            System.out.println("##########");
        }

        System.out.println("Experimental spectrum, peaks that are annotated: "+nrpAnnotation);
        System.out.println("CONSENSUS SPECTRA END");
        System.out.println("##########");
    }



    private static void printTopMassDifferences(NRPConsensusSpectrum nrpConsensusSpectrum, int limitPrint){
        System.out.println("MASS DIFFERENCES IN CONSENSUS SPECTRA");
        MassDifferencesCalculator massDifferencesCalculator = new MassDifferencesCalculator();
        Multiset<Double> massDifferences =massDifferencesCalculator.calculateDifferences(nrpConsensusSpectrum.getMzs(new double[nrpConsensusSpectrum.size()] ));
        //Multsets.copyHighestCountFirst(massDifferences).elementSet()i
        int n = 0;
        for (double d : Multisets.copyHighestCountFirst(massDifferences).elementSet()){
            if (n<limitPrint){
                if (d<30){
                    System.out.println(d +": "+massDifferences.count(d));
                    n++;
                }

            }
        }

    }

}
