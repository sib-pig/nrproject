package main;

import molecules.norine.NorineNRP;
import molecules.norine.NorineNRPStore;
import ms.spectrum.gnps.library.GNPSmodifier;
import ms.spectrum.gnps.library.GnpsLibraryProcessor;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Set;

/**
 * Created by ericart on 01/11/2016.
 */
public class MainGnpsLibraryAnalyzer {

    public static void main(String[] args) throws IOException, URISyntaxException {
        //only necessary the first time that you download the library
        //GNPSmodifier gnpsModifier = new GNPSmodifier();
        //gnpsModifier.modifyLibrary("P:\\nrproject\\src\\main\\resources\\ms\\spectrum\\gnps\\library\\GNPS-LIBRARY.mgf");

        GnpsLibraryProcessor gnpsLibraryProcessor = GnpsLibraryProcessor.getInstance();

        //to create a new mgf from a peptide in the library
        //gnpsLibraryProcessor.createMgfFromLib("dolastatin 10", "P:\\nrproject\\src\\main\\resources\\ms\\spectrum\\gnps\\");


        Set<String > gnpPeptides =gnpsLibraryProcessor.getNRPLibset();

        /*
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();

        for (NorineNRP norineNRP :norineNRPStore.getAllNRPs()){
            norineNRP.structure.getType().toLowerCase();
            if (norineNRP.getSmiles() != null && norineNRP.structure.getType().toLowerCase().equals("linear")){
                String norinePep =norineNRP.getName().toLowerCase();
                for (String gnpPep : gnpPeptides){
                    if (gnpPep.contains(norinePep)){
                        System.out.println(gnpPep+" "+norinePep);
                        System.out.println(norineNRP.getSmiles());
                    }
                }
            }
        }
        */
    }

}
