package io;

import com.fasterxml.jackson.databind.JsonNode;
import org.expasy.mzjava.core.io.ms.spectrum.MgfReader;
import org.expasy.mzjava.core.io.ms.spectrum.mgf.DelegatingRegexTitleParser;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ericart on 01.02.2016.
 */
public class MgfSpectrumReader {

    public MgfSpectrumReader(){

    }

    public List<MsnSpectrum> readSpectrum(File spectraFile) throws IOException, URISyntaxException {

        MgfReader mgfReader= new MgfReader(spectraFile, PeakList.Precision.FLOAT);

        mgfReader.acceptUnsortedSpectra();

        List<MsnSpectrum> experimentalSpectra = new ArrayList<>();

        /*
        LineNumberReader reader =mgfReader.getContext().getCurrentReader();
        for(String line = reader.readLine(); line != null; line = reader.readLine()) {
            System.out.println(line);
        }
        */

        while (mgfReader.hasNext()) {
            MsnSpectrum spectra =mgfReader.next();
            //System.out.println(spectra.getComment()); prints what it is written in title
            experimentalSpectra.add(spectra);
        }


        return experimentalSpectra;

    }
}
