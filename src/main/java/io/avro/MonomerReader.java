package io.avro;

import molecules.Monomer;
import molecules.norine.*;
import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.expasy.mzjava.avro.io.AbstractAvroReader;
import org.expasy.mzjava.avro.io.CompositionReader;
import org.openscience.cdk.exception.CDKException;


import java.io.IOException;
import java.util.List;

/**
 * Created by ericart on 06.07.2016.
 */

public class MonomerReader  extends AbstractAvroReader<Monomer> {

    private final CompositionReader compositionReader = new CompositionReader();

    @Override
    public Class getObjectClass() {
        return Monomer.class;
    }

    public Monomer read(Decoder decoder) throws IOException {

        Monomer monomer = null;
        int index=decoder.readInt();
        String monomerSymbol =decoder.readString();
        String residueId=decoder.readString();
        decoder.skipMap();
        //Composition composition = compositionReader.read(decoder);

        NorineMonomer norineMonomer = NorineMonomerStore.getInstance().getMonomer(monomerSymbol);
        NorineResidue norineResidue = NorineResidueStore.getInstance().getResidue(residueId);

        try {
            monomer =new Monomer(norineMonomer,norineResidue,index);
        } catch (CDKException e) {
            e.printStackTrace();
        }

        return monomer;
    }


    @Override
    protected void createRecordFields(List<Schema.Field> fields) {
        fields.add(createSchemaField("monomerIndex",Schema.create(Schema.Type.INT)));
        fields.add(createSchemaField("originalMonomer",Schema.create(Schema.Type.STRING)));
        fields.add(createSchemaField("residueID",Schema.create(Schema.Type.STRING)));
        fields.add(createSchemaField("residueComposition",compositionReader.createSchema()));

    }


}
