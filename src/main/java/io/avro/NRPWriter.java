package io.avro;

import molecules.Bond;
import molecules.Monomer;
import molecules.NRP;
import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.expasy.mzjava.avro.io.AbstractAvroWriter;


import org.expasy.mzjava.avro.io.CompositionWriter;


import java.io.IOException;
import java.util.List;

/**
 * Created by ericart on 30.06.2016.
 */

public class NRPWriter extends AbstractAvroWriter<NRP> {
    private final MonomerWriter monomerWriter = new MonomerWriter();
    private final CompositionWriter compositionWriter = new CompositionWriter();
    @Override
    public Class getObjectClass() {

        return NRP.class;
    }

    @Override
    public void write(NRP nrp, Encoder out) throws IOException {
        out.writeString(nrp.getNrpId());
        out.writeString(String.valueOf(nrp.getNrpType()));
        compositionWriter.write(nrp.getComposition(),out);
        out.writeArrayStart();

        out.setItemCount((long)nrp.size());
        for (Monomer monomer :nrp.getAllMonomerNode()){
            out.startItem();
            monomerWriter.write(monomer,out);
            //out.writeString(monomer.getMonomerSymbol()+"("+monomer.getIndex()+")");
        }
        out.writeArrayEnd();

        out.writeArrayStart();
        out.setItemCount((long)nrp.getAllBond().size());

        for (Bond bond :nrp.getAllBond()){
            out.startItem();
            out.writeArrayStart();
            out.setItemCount(2);
            out.startItem();
            out.writeInt(bond.getMonomerNodeSource().getIndex());
            out.startItem();
            out.writeInt(bond.getMonomerNodeTarget().getIndex());
            out.writeArrayEnd();
        }

        out.writeArrayEnd();
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {
        fields.add(createSchemaField("precursorId",  Schema.create(Schema.Type.STRING)));
        fields.add(createSchemaField("precursorStructure",  Schema.create(Schema.Type.STRING)));
        fields.add(createSchemaField("precursorComposition",  compositionWriter.createSchema()));
        fields.add(createSchemaField("precursorMonomers",  Schema.createArray(monomerWriter.createSchema())));
        fields.add(createSchemaField("precursorbonds",  Schema.createArray(Schema.createArray(Schema.create(Schema.Type.INT)))));
    }
}
