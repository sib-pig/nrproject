package io.avro;

import com.google.common.base.Optional;
import molecules.NRP;
import ms.spectrum.NRPConsensusSpectrum;
import ms.spectrum.NRPLibPeakAnnotation;
import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;

import org.expasy.mzjava.avro.io.AbstractPeakListReader;
import org.expasy.mzjava.avro.io.PeakReader;
import org.expasy.mzjava.avro.io.UUIDReader;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessor;


import java.io.IOException;
import java.net.URI;
import java.util.*;

/**
 * Created by ericart on 06.07.2016.
 */

public class NRPConsensusSpectrumReader extends AbstractPeakListReader<NRPLibPeakAnnotation, NRPConsensusSpectrum> {
    NRPLibPeakAnnotationReader nrpLibPeakAnnotationReader;
    private final PeakReader precursorReader;
    private final NRPReader nrpReader;
    final UUIDReader uuidReader;
    private NRP nrp;

    public NRPConsensusSpectrumReader(Optional<PeakList.Precision> precisionOverride, List<PeakProcessor<NRPLibPeakAnnotation, NRPLibPeakAnnotation>> peakProcessors, NRPLibPeakAnnotationReader nrpLibPeakAnnotationReader) {
        super(new NRPLibPeakAnnotationReader[]{nrpLibPeakAnnotationReader}, precisionOverride, peakProcessors);
        this.precursorReader = new PeakReader();
        this.nrpReader = new NRPReader();
        this.uuidReader = new UUIDReader();
        this.nrpLibPeakAnnotationReader=nrpLibPeakAnnotationReader;
    }
    @Override
    public Class getObjectClass() {
        return NRPConsensusSpectrum.class;
    }

    @Override
    protected NRPConsensusSpectrum newPeakList(PeakList.Precision precision, double constantIntensity) {
        return new NRPConsensusSpectrum(this.nrp, precision, new HashSet<UUID>());
    }

    @Override
    public NRPConsensusSpectrum read(Decoder decoder) throws IOException {
        Peak precursor = this.precursorReader.read(decoder);
        int msLevel = decoder.readInt();
        String name = decoder.readString();
        double simScoreMean = decoder.readDouble();
        double simScoreStdev = decoder.readDouble();
        double precursorMzMean = decoder.readDouble();
        double precursorMzStdev = decoder.readDouble();

        List<UUID> memberIds = readArray(this.uuidReader, new ArrayList<UUID>(), decoder);

        this.nrp = this.nrpReader.read(decoder);
        this.nrpLibPeakAnnotationReader.setNRP(this.nrp);


        URI source = URI.create(decoder.readString());

        NRPConsensusSpectrum.Status status = NRPConsensusSpectrum.Status.values()[decoder.readEnum()];


        NRPConsensusSpectrum spectrum = super.read(decoder);
        spectrum.setPrecursor(precursor);
        spectrum.setMsLevel(msLevel);
        spectrum.setName(name);
        spectrum.setScoreStats(simScoreMean, simScoreStdev);
        spectrum.setPrecursorStats(precursorMzMean, precursorMzStdev);
        spectrum.addMemberIds(memberIds);
        spectrum.setNRP(this.nrp);
        spectrum.setSpectrumSource(source);
        spectrum.setStatus(status);
        return spectrum;

    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {
        fields.add(createSchemaField("precursor", precursorReader.createSchema()));
        fields.add(createSchemaField("msLevel", Schema.create(Schema.Type.INT)));
        fields.add(createSchemaField("name", Schema.create(Schema.Type.STRING)));
        fields.add(createSchemaField("simScoreMean", Schema.create(Schema.Type.DOUBLE)));
        fields.add(createSchemaField("simScoreStdev", Schema.create(Schema.Type.DOUBLE)));
        fields.add(createSchemaField("precursorMzMean", Schema.create(Schema.Type.DOUBLE)));
        fields.add(createSchemaField("precursorMzStdev", Schema.create(Schema.Type.DOUBLE)));
        fields.add(createSchemaField("memberIds", Schema.createArray(uuidReader.createSchema())));
        fields.add(createSchemaField("nrp", nrpReader.createSchema()));
        fields.add(createSchemaField("source", Schema.create(Schema.Type.STRING)));
        fields.add(createSchemaField("status", createEnumSchema(NRPConsensusSpectrum.Status.class, NRPConsensusSpectrum.Status.values())));
        super.createRecordFields(fields);
    }


}
