package io.avro;

import molecules.Monomer;
import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.expasy.mzjava.avro.io.AbstractAvroWriter;
import org.expasy.mzjava.avro.io.CompositionWriter;

import java.io.IOException;
import java.util.List;

/**
 * Created by ericart on 06.07.2016.
 */
public class MonomerWriter extends AbstractAvroWriter<Monomer> {

    private final CompositionWriter compositionWriter = new CompositionWriter();

    public MonomerWriter() {

    }


    @Override
    public void write(Monomer monomer, Encoder encoder) throws IOException {
        encoder.writeInt(monomer.getIndex());
        encoder.writeString(monomer.getMonomerSymbol());
        encoder.writeString(monomer.getNorineResidue().getId());
        compositionWriter.write(monomer.getComposition(),encoder);
    }

    @Override
    public Class getObjectClass() {
        return Monomer.class;
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {
        fields.add(createSchemaField("monomerIndex",Schema.create(Schema.Type.INT)));
        fields.add(createSchemaField("originalMonomer",Schema.create(Schema.Type.STRING)));
        fields.add(createSchemaField("residueID",Schema.create(Schema.Type.STRING)));
        fields.add(createSchemaField("residueComposition",compositionWriter.createSchema()));


    }

}
