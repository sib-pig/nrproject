package io.avro;

import molecules.Bond;
import molecules.Monomer;
import molecules.NRPFragment;
import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.expasy.mzjava.avro.io.AbstractAvroWriter;
import org.expasy.mzjava.avro.io.CompositionWriter;


import java.io.IOException;
import java.util.List;

/**
 * Created by ericart on 30.06.2016.
 */
public class NRPFragmentWriter extends AbstractAvroWriter<NRPFragment> {

    //private final MonomerWriter monomerWriter = new MonomerWriter();
    private final CompositionWriter compositionWriter = new CompositionWriter();

    public Class getObjectClass() {

        return NRPFragment.class;
    }


    public void write(NRPFragment fragment, Encoder out) throws IOException {

        //out.write(fragment.getAllMonomerNode().)
        out.writeArrayStart();
        out.setItemCount((long)fragment.size());

        for (Monomer monomer :fragment.getAllMonomerNode()){
            out.startItem();
            //monomerWriter.write(monomer,out);
            out.writeInt(monomer.getIndex());
        }

        out.writeArrayEnd();

        out.writeArrayStart();
        out.setItemCount((long)fragment.getAllBond().size());

        for (Bond bond :fragment.getAllBond()){
            out.startItem();
            out.writeArrayStart();
                out.setItemCount(2);
                out.startItem();
                out.writeInt(bond.getMonomerNodeSource().getIndex());
                out.startItem();
                out.writeInt(bond.getMonomerNodeTarget().getIndex());
            out.writeArrayEnd();
        }

        out.writeArrayEnd();
        compositionWriter.write(fragment.getComposition(),out);
        out.writeString(fragment.getPrecursorId());
        out.writeInt(fragment.getFragmentationStage());


    }


    protected void createRecordFields(List<Schema.Field> fields) {

        //fields.add(createSchemaField("monomers",Schema.createArray(monomerWriter.createSchema())));
        fields.add(createSchemaField("monomerIndexes",Schema.createArray(Schema.create(Schema.Type.INT))));
        fields.add(createSchemaField("bonds",  Schema.createArray(Schema.createArray(Schema.create(Schema.Type.INT)))));
        fields.add(createSchemaField("fragmentComposition", compositionWriter.createSchema()));
        fields.add(createSchemaField("precursorId", Schema.create(Schema.Type.STRING)));
        fields.add(createSchemaField("msStage", Schema.create(Schema.Type.INT)));
    }
}
