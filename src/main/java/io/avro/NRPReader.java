package io.avro;

import molecules.Bond;
import molecules.Monomer;
import molecules.NRP;
import molecules.NRPType;

import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;

import org.expasy.mzjava.avro.io.AbstractAvroReader;
import org.expasy.mzjava.avro.io.CompositionReader;
import org.expasy.mzjava.core.mol.Composition;

import org.jgrapht.graph.SimpleGraph;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ericart on 06.07.2016.
 */
public class NRPReader extends AbstractAvroReader<NRP> {

    MonomerReader monomerReader = new MonomerReader();
    CompositionReader compositionReader = new CompositionReader();

    @Override
    public Class getObjectClass() {
        return NRP.class;
    }

    @Override
    public NRP read(Decoder decoder) throws IOException {

        String nrpId =decoder.readString();
        String nrpType = decoder.readString();
        Composition composition =compositionReader.read(decoder);
        SimpleGraph<Monomer, Bond> simpleGraph = new SimpleGraph<>(Bond.class);
        //List monomers =this.readArray(this.monomerReader, new ArrayList(), decoder);

        Map<Integer,Monomer> monomersMap = new HashMap<>();

        for(long i = decoder.readArrayStart(); i != 0L; i = decoder.arrayNext()) {

            for (long j = 0L; j < i; j++) {
                Monomer monomer = monomerReader.read(decoder);
                simpleGraph.addVertex(monomer);
                monomersMap.put(monomer.getIndex(),monomer);
            }
        }

        for(long i = decoder.readArrayStart(); i != 0L; i = decoder.arrayNext()) {

            for (long j = 0L; j < i; j++) {
                int[] indexes = new int[2];
                int c=0;

                for(long i2 = decoder.readArrayStart(); i2 != 0L; i2 = decoder.arrayNext()) {
                    for (long j2 = 0L; j2 < i2; j2++){
                        indexes[c] = decoder.readInt();
                        c++;
                    }
                }

                simpleGraph.addEdge(monomersMap.get(indexes[0]), monomersMap.get(indexes[1]), new Bond(monomersMap.get(indexes[0]), monomersMap.get(indexes[1]), Bond.BondType.AMINO));
            }

        }

        return new NRP(simpleGraph,composition,nrpId, NRPType.valueOf(nrpType));

    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {
        fields.add(createSchemaField("precursorId",  Schema.create(Schema.Type.STRING)));
        fields.add(createSchemaField("precursorStructure",  Schema.create(Schema.Type.STRING)));
        fields.add(createSchemaField("precursorComposition",  compositionReader.createSchema()));
        fields.add(createSchemaField("precursorMonomers",  Schema.createArray(monomerReader.createSchema())));
        fields.add(createSchemaField("precursorbonds",  Schema.createArray(Schema.createArray(Schema.create(Schema.Type.INT)))));
    }

}
