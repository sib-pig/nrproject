package io.avro;

import com.google.common.base.Optional;
import ms.spectrum.NRPConsensusSpectrum;
import org.apache.avro.io.Encoder;
import org.expasy.mzjava.avro.io.*;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.apache.avro.Schema;


import java.io.IOException;
import java.util.List;

/**
 * Created by ericart on 30.06.2016.
 */


public class NRPConsensusSpectrumWriter extends AbstractPeakListWriter<NRPConsensusSpectrum> {

    private PeakWriter peakWriter = new PeakWriter();
    private NRPWriter nrpWriter = new NRPWriter();
    private static final UUIDWriter uuidWriter = new UUIDWriter();

    public NRPConsensusSpectrumWriter() {

        this(Optional.<PeakList.Precision>absent());
    }

    public NRPConsensusSpectrumWriter(Optional<PeakList.Precision> precisionOverride) {
        super(precisionOverride, new AbstractAvroWriter[]{new NRPLibPeakAnnotationWriter()});
        this.peakWriter = new PeakWriter();
        this.nrpWriter = new NRPWriter();
    }

    @Override
    public Class getObjectClass() {

        return NRPConsensusSpectrum.class;
    }

    @Override
    public void write(NRPConsensusSpectrum spectrum, Encoder out) throws IOException {

        peakWriter.write(spectrum.getPrecursor(), out);
        out.writeInt(spectrum.getMsLevel());
        out.writeString(spectrum.getName());
        out.writeDouble(spectrum.getSimScoreMean());
        out.writeDouble(spectrum.getSimScoreStdev());
        out.writeDouble(spectrum.getPrecursorMzMean());
        out.writeDouble(spectrum.getPrecursorMzStdev());
        writeArray(uuidWriter, spectrum.getMemberIds(), out);
        nrpWriter.write(spectrum.getNRP(), out);
        out.writeString(spectrum.getSpectrumSource().toString());
        out.writeEnum(spectrum.getStatus().ordinal());

        super.writePeakList(spectrum, out);
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        fields.add(createSchemaField("precursor", peakWriter.createSchema()));
        fields.add(createSchemaField("msLevel", Schema.create(Schema.Type.INT)));
        fields.add(createSchemaField("name", Schema.create(Schema.Type.STRING)));
        fields.add(createSchemaField("simScoreMean", Schema.create(Schema.Type.DOUBLE)));
        fields.add(createSchemaField("simScoreStdev", Schema.create(Schema.Type.DOUBLE)));
        fields.add(createSchemaField("precursorMzMean", Schema.create(Schema.Type.DOUBLE)));
        fields.add(createSchemaField("precursorMzStdev", Schema.create(Schema.Type.DOUBLE)));
        fields.add(createSchemaField("memberIds", Schema.createArray(uuidWriter.createSchema())));
        fields.add(createSchemaField("nrp", nrpWriter.createSchema()));
        fields.add(createSchemaField("source", Schema.create(Schema.Type.STRING)));
        fields.add(createSchemaField("status", createEnumSchema(NRPConsensusSpectrum.Status.class, NRPConsensusSpectrum.Status.values())));

        super.createRecordFields(fields);
    }




}
