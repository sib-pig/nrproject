package io.avro;

import molecules.Monomer;
import molecules.modification.Modification;
import ms.spectrum.NRPFragmentAnnotation;
import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.expasy.mzjava.avro.io.AbstractAvroWriter;
import org.expasy.mzjava.avro.io.CompositionWriter;


import java.io.IOException;
import java.util.List;


/**
 * Created by ericart on 30.06.2016.
 */

public class NRPFragmentAnnotationWriter extends AbstractAvroWriter<NRPFragmentAnnotation> {

    private final NRPFragmentWriter nrpWriter = new NRPFragmentWriter();
    private final CompositionWriter compositionWriter = new CompositionWriter();
    private final ModificationWriter modificationWriter = new ModificationWriter();

    public NRPFragmentAnnotationWriter() {

    }


    public void write(NRPFragmentAnnotation annotation, Encoder out) throws IOException {

        nrpWriter.write(annotation.getFragment(), out);
        //compositionWriter.write(annotation.getFragment().getComposition(), out);
        out.writeInt(annotation.getCharge());
        compositionWriter.write(annotation.getChargedGroup(),out);
        compositionWriter.write(annotation.getNeutralLoss(), out);
        compositionWriter.write(annotation.getModifications(), out);

        out.writeMapStart();
        out.setItemCount((long)annotation.getAllModifiedMonomers().size());
        for (Monomer monomer : annotation.getAllModifiedMonomers()){
            out.startItem();
            out.writeString(String.valueOf(monomer.getIndex()));

            out.writeArrayStart();
            out.setItemCount((long)annotation.getModifications(monomer).size());

            for(Modification modification : annotation.getModifications(monomer)){
                    out.startItem();
                    modificationWriter.write(modification,out);
                }
            out.writeArrayEnd();

        }

        out.writeMapEnd();

    }


    public Class getObjectClass() {

        return NRPFragmentAnnotation.class;
    }


    protected void createRecordFields(List<Schema.Field> fields) {

        fields.add(createSchemaField("fragment", nrpWriter.createSchema()));
        //fields.add(createSchemaField("fragmentComposition", compositionWriter.createSchema()));
        fields.add(createSchemaField("charge", Schema.create(Schema.Type.INT)));
        fields.add(createSchemaField("chargeComposition", compositionWriter.createSchema()));
        fields.add(createSchemaField("neutralLoss", compositionWriter.createSchema()));
        fields.add(createSchemaField("modifications", compositionWriter.createSchema()));
        //fields.add(createSchemaField("modificationPositions",  Schema.createMap(Schema.createArray(Schema.create(Schema.Type.STRING)))));
        fields.add(createSchemaField("modificationPositions",  Schema.createMap(Schema.createArray(modificationWriter.createSchema()))));

    }



}

