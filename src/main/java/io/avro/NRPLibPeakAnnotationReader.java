package io.avro;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import molecules.NRP;
import ms.spectrum.NRPFragmentAnnotation;
import ms.spectrum.NRPLibPeakAnnotation;
import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.expasy.mzjava.avro.io.AbstractAvroReader;


import java.io.IOException;
import java.util.List;

/**
 * Created by ericart on 06.07.2016.
 */
public class NRPLibPeakAnnotationReader extends AbstractAvroReader<NRPLibPeakAnnotation> {

    private NRPFragmentAnnotationReader nrpFragmentAnnotationReader=new NRPFragmentAnnotationReader();
    private NRP nrp=null;

    @Override
    public Class getObjectClass() {
        return NRPLibPeakAnnotation.class;
    }

    @Override
    public NRPLibPeakAnnotation read(Decoder decoder) throws IOException {

        Preconditions.checkNotNull(nrp,"NRP must be set before reading");
        nrpFragmentAnnotationReader.setNRP(nrp);
        int count = decoder.readInt();
        double mzStd = decoder.readDouble();
        double intensityStd = decoder.readDouble();
        Optional<NRPFragmentAnnotation> optAnnotation = readOptional(nrpFragmentAnnotationReader, decoder);

/*
        if(optAnnotation.equals(Optional.absent())){
            optAnnotation=null;
        }

*/
        return new NRPLibPeakAnnotation(count, mzStd, intensityStd, optAnnotation);
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {
        fields.add(createSchemaField("mergedPeakCount", Schema.create(Schema.Type.INT)));
        fields.add(createSchemaField("mzStd", Schema.create(Schema.Type.DOUBLE)));
        fields.add(createSchemaField("intensityStd", Schema.create(Schema.Type.DOUBLE)));
        fields.add(createSchemaField("fragAnnotation", Schema.createUnion(Lists.newArrayList(nrpFragmentAnnotationReader.createSchema(), Schema.create(Schema.Type.NULL)))));

    }

    public void setNRP(NRP nrp){
        if (this.nrp==null){
            this.nrp=nrp;
        }else{
            throw new IllegalStateException("NRP can only be set once");
        }

    }


}
