package io.avro;

import com.google.common.collect.Lists;
import ms.spectrum.NRPLibPeakAnnotation;
import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.expasy.mzjava.avro.io.AbstractLibPeakAnnotationWriter;
import org.expasy.mzjava.avro.io.AvroWriter;
import org.openide.util.lookup.ServiceProvider;

import java.io.IOException;
import java.util.List;

/**
 * Created by ericart on 30.06.2016.
 */

public class NRPLibPeakAnnotationWriter extends AbstractLibPeakAnnotationWriter<NRPLibPeakAnnotation> {
    private final NRPFragmentAnnotationWriter nrpFragAnnotationWriter = new NRPFragmentAnnotationWriter();

    @Override
    public Class getObjectClass() {

        return NRPLibPeakAnnotation.class;
    }

    @Override
    public void write(NRPLibPeakAnnotation annotation, Encoder out) throws IOException {

        super.writeLibAnnotation(annotation, out);
        writeOptional(nrpFragAnnotationWriter, annotation.getOptFragmentAnnotation(), out);
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        super.createRecordFields(fields);
        fields.add(createSchemaField("fragAnnotation", Schema.createUnion(Lists.newArrayList(nrpFragAnnotationWriter.createSchema(), Schema.create(Schema.Type.NULL)))));
    }

}
