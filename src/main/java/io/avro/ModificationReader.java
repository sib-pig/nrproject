package io.avro;

import molecules.modification.Modification;
import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.expasy.mzjava.avro.io.AbstractAvroReader;
import org.expasy.mzjava.avro.io.CompositionReader;
import org.expasy.mzjava.core.mol.Composition;

import java.io.IOException;
import java.util.List;

/**
 * Created by ericart on 06.07.2016.
 */
public class ModificationReader extends AbstractAvroReader<Modification> {

    private final CompositionReader compositionReader = new CompositionReader();

    public ModificationReader() {

    }

    @Override
    public Class getObjectClass() {

        return Modification.class;
    }

    @Override
    public Modification read(Decoder decoder) throws IOException {

        Composition composition =compositionReader.read(decoder);
        String modificationType=decoder.readString();
        return new Modification(composition, Modification.ModificationType.valueOf(modificationType));
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {
        fields.add(createSchemaField("modificationComposition",compositionReader.createSchema()));
        fields.add(createSchemaField("modificationType",Schema.create(Schema.Type.STRING)));

    }


}

