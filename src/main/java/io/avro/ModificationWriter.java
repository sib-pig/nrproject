package io.avro;

import molecules.modification.Modification;
import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.expasy.mzjava.avro.io.AbstractAvroWriter;
import org.expasy.mzjava.avro.io.CompositionWriter;

import java.io.IOException;
import java.util.List;

/**
 * Created by ericart on 06.07.2016.
 */
public class ModificationWriter  extends AbstractAvroWriter<Modification> {
    private final CompositionWriter compositionWriter = new CompositionWriter();

    public ModificationWriter() {

    }

    @Override
    public Class getObjectClass() {
        return Modification.class;
    }

    @Override
    public void write(Modification modification, Encoder encoder) throws IOException {
        compositionWriter.write(modification.getComposition(),encoder);
        encoder.writeString(String.valueOf(modification.getModificationType()));

    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {
        fields.add(createSchemaField("modificationComposition",compositionWriter.createSchema()));
        fields.add(createSchemaField("modificationType",Schema.create(Schema.Type.STRING)));

    }

}
