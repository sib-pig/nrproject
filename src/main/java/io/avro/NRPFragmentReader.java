package io.avro;

import com.google.common.base.Preconditions;
import molecules.Bond;
import molecules.Monomer;
import molecules.NRP;
import molecules.NRPFragment;
import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.expasy.mzjava.avro.io.AbstractAvroReader;
import org.expasy.mzjava.avro.io.CompositionReader;
import org.expasy.mzjava.core.mol.Composition;
import org.jgrapht.graph.SimpleGraph;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by ericart on 04.07.2016.
 */
public class NRPFragmentReader extends AbstractAvroReader<NRPFragment> {

    private NRP nrp=null;
    private final CompositionReader compositionReader = new CompositionReader();

    @Override
    public Class getObjectClass() {

        return NRPFragment.class;
    }

    @Override
    public NRPFragment read(Decoder in) throws IOException {

        Preconditions.checkNotNull(nrp,"NRP must be set before reading");

        SimpleGraph<Monomer,Bond> simpleGraph = nrp.cloneSimpleGraph();

        Set<Integer> indexes = new HashSet<>();
        for(long i = in.readArrayStart(); i != 0L; i = in.arrayNext()) {
            for (long j = 0L; j < i; j++) {
                indexes.add(in.readInt());
            }
        }

        for (Monomer monomer :  nrp.getAllMonomerNode()){
            if (!indexes.contains(monomer.getIndex())) {
                simpleGraph.removeVertex(monomer);
            }
        }

        Set<Set<Integer>> bonds = new HashSet<>();


        for(long i = in.readArrayStart(); i != 0L; i = in.arrayNext()) {
            for (long j = 0L; j < i; j++) {
                Set<Integer> bondIndexes = new HashSet<>();

                for(long i2 = in.readArrayStart(); i2 != 0L; i2 = in.arrayNext()) {
                    for (long j2 = 0L; j2 < i2; j2++) {
                        bondIndexes.add(in.readInt());
                    }
                }
                bonds.add(bondIndexes);

            }
        }


        for (Bond  bond : nrp.getAllBond()){
            Set<Integer> graphBond = new HashSet<>();
            graphBond.add(bond.getMonomerNodeSource().getIndex());
            graphBond.add(bond.getMonomerNodeTarget().getIndex());
            if (!bonds.contains(graphBond)){
                simpleGraph.removeEdge(bond);
            }

        }

        Composition fragmentComposition =compositionReader.read(in);
        String precursorId= in.readString();
        int msStage = in.readInt();

        NRPFragment nrpFragment = new NRPFragment(simpleGraph,fragmentComposition,precursorId,msStage);

        return nrpFragment;

    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        fields.add(createSchemaField("monomerIndexes",Schema.createArray(Schema.create(Schema.Type.INT))));
        fields.add(createSchemaField("bonds",  Schema.createArray(Schema.createArray(Schema.create(Schema.Type.INT)))));
        fields.add(createSchemaField("fragmentComposition", compositionReader.createSchema()));
        fields.add(createSchemaField("precursorId", Schema.create(Schema.Type.STRING)));
        fields.add(createSchemaField("msStage", Schema.create(Schema.Type.INT)));
    }

    public void setNRP(NRP nrp){
        if (this.nrp==null){
            this.nrp=nrp;
        }else{
            throw new IllegalStateException("NRP can only be set once");
        }

    }


}
