package io.avro;

import com.google.common.base.Preconditions;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import molecules.Monomer;
import molecules.NRP;
import molecules.NRPFragment;
import molecules.modification.Modification;
import ms.spectrum.NRPFragmentAnnotation;
import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.expasy.mzjava.avro.io.AbstractAvroReader;
import org.expasy.mzjava.avro.io.CompositionReader;

import org.expasy.mzjava.core.mol.Composition;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by ericart on 04.07.2016.
 */

public class NRPFragmentAnnotationReader extends AbstractAvroReader<NRPFragmentAnnotation> {

    private final NRPFragmentReader nrpFragmentReader=new NRPFragmentReader();
    private final CompositionReader compositionReader = new CompositionReader();
    private final ModificationReader modificationReader = new ModificationReader();
    private NRP nrp=null;


    @Override
    public Class getObjectClass() {

        return NRPFragmentAnnotation.class;
    }

    @Override
    public NRPFragmentAnnotation read(Decoder in) throws IOException {

        Preconditions.checkNotNull(nrp,"NRP must be set before reading");
        nrpFragmentReader.setNRP(nrp);
        NRPFragment nrpFragment = nrpFragmentReader.read(in);
        //in.skipMap();
        int charge = in.readInt();
        Composition chargeComposition1= compositionReader.read(in);
        Composition chargeComposition = new Composition.Builder().addAll(chargeComposition1).charge(charge).build();
        Composition neutralLossComposition= compositionReader.read(in);
        in.skipMap();

        ListMultimap<Monomer,Modification> listModifications = ArrayListMultimap.create();

        for(long i = in.readMapStart(); i != 0L; i = in.mapNext()) {
            for(long j = 0L; j < i; j++) {
                String monomerIndex = in.readString();
                List<Modification> modifications = new ArrayList<>();
                for(long i2 = in.readArrayStart(); i2 != 0; i2 = in.arrayNext()) {
                    for (long j2 = 0; j2 < i2; j2++) {
                        modifications.add(modificationReader.read(in));
                    }
                }

                listModifications.putAll(nrpFragment.getMonomer(Integer.valueOf(monomerIndex).intValue()),modifications);
            }
        }

        return new NRPFragmentAnnotation(charge,nrpFragment,listModifications,chargeComposition,neutralLossComposition);

    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        fields.add(createSchemaField("fragment", nrpFragmentReader.createSchema()));
        //fields.add(createSchemaField("fragmentComposition", compositionReader.createSchema()));
        fields.add(createSchemaField("charge", Schema.create(Schema.Type.INT)));
        fields.add(createSchemaField("chargeComposition", compositionReader.createSchema()));
        fields.add(createSchemaField("neutralLoss", compositionReader.createSchema()));
        fields.add(createSchemaField("modifications", compositionReader.createSchema()));
        fields.add(createSchemaField("modificationPositions",  Schema.createMap(Schema.createArray(modificationReader.createSchema()))));

    }

    public void setNRP(NRP nrp){
        if (this.nrp==null){
            this.nrp=nrp;
        }

        //be careful, it doesnt throw an exception
    }


}

