package io;

import org.expasy.mzjava.core.io.ms.spectrum.MzxmlReader;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;


import java.io.*;
import java.util.*;

/**
 * Created by ericart on 25.01.2016.
 */

public class MzXmlSpectrumReader {

    public List<MsnSpectrum> readSpectrum() throws IOException {

        File fileSpectrum = new File ("P:\\NRProject\\src\\modifyLibrary\\resources\\ms\\iSNAP_PNAS_data\\Tyrocidine_LCMS\\AI_Tyro_Nutrient_Pellet_c01_R1.mzXML");
        MzxmlReader mzxmlReader = MzxmlReader.newStrictReader(fileSpectrum , PeakList.Precision.DOUBLE);


        List<MsnSpectrum> experimentalSpectra = new ArrayList<>();
        Set<Integer> targetScanNumbers = new HashSet<>(Arrays.asList(1246,1242,1239,1251,1253,1257));

        while (mzxmlReader.hasNext()){
            MsnSpectrum msnSpectrum =mzxmlReader.next();
            if (targetScanNumbers.contains(msnSpectrum.getSpectrumIndex())){
                experimentalSpectra.add(msnSpectrum);
            }

        }
        return experimentalSpectra;

    }

}
