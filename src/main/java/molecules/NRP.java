package molecules;

import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.jgrapht.graph.SimpleGraph;
import scala.util.parsing.combinator.testing.Str;


/**
 * Created by ericart on 24.11.2015.
 */

public class NRP extends MonomericGraph {

    private String nrpId;
    private NRPType nrpType;

    public NRP(SimpleGraph<Monomer, Bond> monomersGraph, Composition composition, String nrpId, NRPType nrpType){
        super(monomersGraph,composition);
        this.nrpId=nrpId;
        this.nrpType = nrpType;

    }

    public String getNrpId() {

        return nrpId;

    }

    public NRPType getNrpType() {

        return nrpType;

    }
}
