package molecules;

import org.expasy.mzjava.core.mol.Composition;
import org.jgrapht.graph.SimpleGraph;


/**
 * Created by ericart on 19.11.2015.
 */
public class NRPFragment extends MonomericGraph {

    private String precursorId;
    private int fragmentationStage;
    private State state = State.UNKNOWN;
    private String graphID=null;
    private double intensity=0.0D;
    private double mz = 0.0D;

    public NRPFragment(SimpleGraph<Monomer, Bond> fragmentSubgraph, Composition composition, String precursorId, int fragmentationStage) {
        super(fragmentSubgraph, composition);
        this.precursorId=precursorId;
        this.fragmentationStage=fragmentationStage;
    }

    public String getPrecursorId() {

        return precursorId;
    }

    public int getFragmentationStage() {

        return fragmentationStage;
    }

    public State getState() {

        return state;
    }

    public String getGraphID() {

        return graphID;
    }

    public double getExperimentalMz() {
        return mz;
    }

    public double getExperimentalIntensity() {

        return intensity;

    }

    public void setState(State state) {

        this.state = state;
    }


    public void setGraphID(String graphID){

        if (this.graphID==null){
            this.graphID= graphID;
        }else{
            throw new IllegalStateException("GraphID has already been set");
        }

    }
    public void setIntensityMatch(double intensity){

        if (this.state == State.MATCHED && this.intensity==0.0D){
            this.intensity=intensity;
        }else{
            throw new IllegalStateException("Intensity can't be set if it has already been introduced or the fragment has not been matched");
        }

    }

    public void setMzMatch(double mz){

        if (this.state == State.MATCHED && this.mz==0.0D){
            this.mz=mz;
        }else{
            throw new IllegalStateException("Mz can't be set if it has already been introduced or the fragment has not been matched");
        }

    }
    public enum State{
        MATCHED, UNMATCHED, UNKNOWN;

    }
/*
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        NRPFragment that = (NRPFragment) o;

        if (fragmentationStage != that.fragmentationStage) return false;
        return precursorId != null ? precursorId.equals(that.precursorId) : that.precursorId == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (precursorId != null ? precursorId.hashCode() : 0);
        return result;
    }
*/
}
