package molecules;

import molecules.norine.*;
import org.expasy.mzjava.core.mol.Composition;
import org.jgrapht.graph.SimpleGraph;
import org.openscience.cdk.exception.CDKException;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by ericart on 20.02.2016.
 */

/**
 * This class creates a NRP (a graph having MonomerNodes as vertexes and Bonds as edges). Note that this graph
 * is created using the information given by the results of Smiles2Monomers, so it could be possible that it does not
 * have the same monomers specified by Norine database.
 * CAN ONLY BE DONE IF RESIDUES EXIST!
 */

public class NRPBuilder {

    private NorineMonomerStore monomersNorine;
    private NorineResidueStore residuesS2M;
    private SimpleGraph<Monomer, Bond> simpleGraph;

    public NRPBuilder() {

        this.monomersNorine= NorineMonomerStore.getInstance();
        this.residuesS2M= NorineResidueStore.getInstance();

    }

    public NRP createNRP (NorineNRP norineNRP) throws CDKException {

        NorineNRPChecker norineNRPChecker = new NorineNRPChecker(norineNRP);
        norineNRPChecker.nonNullResidues();
        norineNRPChecker.checkResiduesCorrectness();
        norineNRPChecker.checkResiduesCoverage();
        norineNRPChecker.checkSmilesVerified();

        this.simpleGraph = new SimpleGraph<>(Bond.class);

        List<String> listNamesResidues = norineNRP.structure.getResidues();
        List<NorineNRP.Idxs> listResiduesEdges = norineNRP.structure.getResiduesEdges();

        // we set the nodes
        List<Monomer> listNodes = setNodes(listNamesResidues);

        //we set the bonds
        setBonds(listNodes, listResiduesEdges);

        NRPCompositionBuilder nrpCompositionBuilder = new NRPCompositionBuilder();
        Composition composition = nrpCompositionBuilder.createNRPComposition(norineNRP, simpleGraph);

        NRPType nrpType = NRPType.valueOf(norineNRP.structure.getType().replaceAll(" ","_").toUpperCase());

        return new NRP(simpleGraph, composition,norineNRP.getId(),nrpType);

    }


    private  List<Monomer> setNodes(List<String> listNamesResidues) throws CDKException {

        List<Monomer> listNodes = new ArrayList<>();
        Monomer monomer;

        int i=0;
        for (String nameResidue : listNamesResidues ){

            NorineResidue norineResidue = residuesS2M.getResidue(nameResidue);
            NorineMonomer norineMonomer = monomersNorine.getMonomer(norineResidue.getMono());
            monomer = new Monomer(norineMonomer, norineResidue,i);
            simpleGraph.addVertex(monomer);
            listNodes.add(monomer);
            i++;
        }

        return listNodes;

    }

    private void setBonds(List<Monomer> listNodes, List<NorineNRP.Idxs> listBonds){

        for (NorineNRP.Idxs edge : listBonds){
            int source=edge.getIdxs()[0];
            int target=edge.getIdxs()[1];
            simpleGraph.addEdge(listNodes.get(source), listNodes.get(target), new Bond(listNodes.get(source), listNodes.get(target), Bond.BondType.AMINO));
        }
    }

}
