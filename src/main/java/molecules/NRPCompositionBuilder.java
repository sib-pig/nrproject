package molecules;

import molecules.norine.NorineNRP;
import org.jgrapht.graph.SimpleGraph;
import org.expasy.mzjava.core.mol.Composition;
import org.openscience.cdk.exception.CDKException;
import utils.CompositionsJoiner;
import utils.SmilesUtils;


/**
 * Created by ericart on 22.02.2016.
 */

/**
 * This class returns a Composition of a given NRP after checking several parameters of it:
 * -CheckSmiles: checks that the Smiles and Formula of an NRP are equivalent
 * -CheckResiduesComposition: checks if the composition of a whole peptide (computed using its formula) is equivalent
 * to the sum of the compositions from its residues (computed using their smarts).
 */

public class NRPCompositionBuilder {

    String nrpID="";
    Composition norineComposition;

    public NRPCompositionBuilder() {

    }

    public Composition createNRPComposition (NorineNRP norineNRP, SimpleGraph<Monomer, Bond> simpleGraph ) throws CDKException {

        this.nrpID=norineNRP.getId();
        String nrpFormula= norineNRP.general.getFormula();
        String smiles = norineNRP.getSmiles();
        return createNRPComposition(nrpFormula,smiles,simpleGraph);

    }

    public Composition createNRPComposition (String nrpFormula, String smiles,SimpleGraph<Monomer, Bond> simpleGraph) throws CDKException {

        if (nrpFormula != null){
            this.norineComposition=Composition.parseComposition(nrpFormula);
            checkSmiles(smiles, "formula");
            checkResidues(simpleGraph);


        }else{
            this.norineComposition = CompositionsJoiner.joinCompositions(simpleGraph.vertexSet());
            checkSmiles(smiles, "residues");
        }

        return this.norineComposition;

    }

    private boolean checkSmiles(String smiles, String compositionSource) throws CDKException {
        String formulaFromSmiles=SmilesUtils.smilesToFormula(smiles,true);
        if (!this.norineComposition.isEmpty() ){

            if(!this.norineComposition.equals(Composition.parseComposition(formulaFromSmiles))) {

                throw new IllegalStateException("NRP "+ nrpID +" smiles does not match NRP "+ compositionSource+"!!");
            }
        }


        return true;

    }

    private boolean checkResidues(SimpleGraph<Monomer, Bond> simpleGraph){
        Composition compositionResidues=CompositionsJoiner.joinCompositions(simpleGraph.vertexSet());
        if (!this.norineComposition.isEmpty() ){
            if (!this.norineComposition.equals(compositionResidues)){

                throw new IllegalStateException("NRP " + nrpID +" residues do not match NRP formula!!");
            }
        }


        return true;
    }


}
