package molecules.modification;

import molecules.Compound;
import org.expasy.mzjava.core.mol.Composition;

/**
 * Created by ericart on 04.04.2016.
 */
public class Modification implements Compound {
    Composition composition;
    ModificationType modificationType;

    public Modification(Composition composition,ModificationType modificationType) {

        this.composition= composition;
        this.modificationType = modificationType;

    }

    public Composition getComposition() {

        return composition;
    }

    public ModificationType getModificationType() {

        return modificationType;

    }


    public String toString() {

        return composition.toString();

    }
    public static enum ModificationType {
        FIX,
        VARIABLE,
        ION,
        NEUTRAL_LOSS;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Modification that = (Modification) o;

        if (composition != null ? !composition.equals(that.composition) : that.composition != null) return false;
        return modificationType == that.modificationType;

    }

    @Override
    public int hashCode() {
        int result = composition != null ? composition.hashCode() : 0;
        result = 31 * result + (modificationType != null ? modificationType.hashCode() : 0);
        return result;
    }

}
