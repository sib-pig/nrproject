package molecules;

import org.jgrapht.graph.DefaultEdge;

/**
 * Created by ericart on 06.11.2015.
 */

/**
 * This class extends de DefaultEdge of JGrapht so it is used to label the
 * graph edges in order to indicate the bond between two monomers
 */

public class Bond extends DefaultEdge {

    private Monomer source;
    private Monomer target;
    BondType bondType;

    public enum BondType {
        AMINO,
        DISULFUR,
        PHENOLIC,
        GLYCOSIDE
    }

    public Bond(Monomer m1, Monomer m2, BondType label) {
        this.source=m1;
        this.target=m2;
        this.bondType= label;

    }

    public Monomer getMonomerNodeSource() {
        return source;
    }

    public Monomer getMonomerNodeTarget() {
        return target;
    }

    public BondType getBondType() {
        return bondType;
    }

    //public String toIndexes(){
    //   return "["+source.getIndex()+","+target.getIndex()+"]";
    //}

    @Override
    public String toString() {
        return "(" + this.source + " : " + this.target + " , " + this.bondType +")";
    }


}




