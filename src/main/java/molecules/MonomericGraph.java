package molecules;

import org.expasy.mzjava.core.mol.Composition;
import org.jgrapht.graph.SimpleGraph;

import java.util.*;

/**
 * Created by ericart on 24.11.2015.
 */

public abstract class MonomericGraph {

    private SimpleGraph<Monomer, Bond> graph;
    private Map<Integer,Monomer> indexesMap;
    private Composition composition;
    private double molecularMass;
    private int size;


    public MonomericGraph(SimpleGraph<Monomer, Bond> graph, Composition composition) {
        this.graph = graph;
        this.composition = composition;
        this.molecularMass=composition.getMolecularMass();
        this.size=graph.vertexSet().size();
        createIndexesMap();
    }

    public void createIndexesMap(){
        indexesMap =new HashMap<>();
        for (Monomer monomer : graph.vertexSet()){
            indexesMap.put(monomer.getIndex(),monomer);
        }
    }

    public SimpleGraph<Monomer, Bond> getSimpleGraph(){

        return this.graph;

    }
    public SimpleGraph<Monomer, Bond> cloneSimpleGraph(){

        return (SimpleGraph)this.graph.clone();

    }

    public Monomer getMonomer(int monomerIdx){

        return indexesMap.get(monomerIdx);

    }

    public Composition getComposition() {

        return composition;
    }


    public double getMolecularMass() {

        return molecularMass;

    }
    public Set<Monomer> getAllMonomerNode(){

        return this.graph.vertexSet();

    }

    public Set<Bond> getAllBond(){

        return this.graph.edgeSet();

    }


    public Set<Bond> getBondsSubset(Set<Monomer> monomerSubset){
        Set<Bond> bondsSubset = new HashSet<>();
        for (Monomer node : this.graph.vertexSet()){
            if (monomerSubset.contains(node)){

                for (Bond edge : this.graph.edgesOf(node)){
                    Monomer monomer1 = edge.getMonomerNodeSource();//why returns an object and needs to be cast?
                    Monomer monomer2 = edge.getMonomerNodeTarget();
                    if (monomerSubset.contains(monomer1) && monomerSubset.contains(monomer2)){
                        bondsSubset.add(edge);

                    }
                }
            }
        }
        return bondsSubset;
    }

    public int size() {
        return size;
    }


    public boolean containsMonomer(Monomer monomer){

        return this.graph.containsVertex(monomer);

    }

    public boolean isEndMolecule(){

        return this.graph.vertexSet().size() == 1;
    }
/*
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MonomericGraph that = (MonomericGraph) o;

        return graph.equals(that.graph) && composition.equals(that.composition);

    }

    @Override
    public int hashCode() {
        int result = graph.hashCode();
        result = 31 * result + composition.hashCode();
        return result;
    }
*/
    @Override
    public String toString() {

        return this.graph.vertexSet().toString();

    }


}
