package molecules.norine;

import org.expasy.mzjava.core.mol.Composition;
import org.openscience.cdk.exception.CDKException;
import utils.SmilesUtils;


/**
 * Created by ericart on 20.02.2016.
 */

/**
 * The information of the NorineResidue class is retrieved from the residues JSON output from Smiles2Monomers.
 *
 * A residue is the chemical composition of a monomer inside an NRP, after being linked with the other monomers and
 * suffering some loses.
 * So each residue comes from a "precursor" NorineMonomer.
 */

public class NorineResidue {
    private String mono;
    private String smarts;
    private String id;
    private Composition composition = null;

    public String getMono() {

        return mono;
    }

    public String getSmarts() {

        return smarts;
    }

    public String getId() {

        return id;
    }

    public Composition getComposition() throws CDKException {
        if (this.composition==null){
            String formula = SmilesUtils.smilesToFormula(this.getSmarts(),false);
            this.composition=Composition.parseComposition(formula);
        }
        return this.composition;
    }

}
