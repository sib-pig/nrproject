package molecules.norine;

import com.fasterxml.jackson.databind.JsonNode;
import io.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Created by ericart on 04.12.2015.
 */

/**
 * The monomers from Norine are read from a JSON file (either locally or using a link to the REST JSON file) and stored
 * in this class.
 */

public class NorineMonomerStore {

    private static NorineMonomerStore ourInstance = new NorineMonomerStore();
    private final Map<String, NorineMonomer> monomerMap = new HashMap<>();
    private String monomersResource=System.getProperty("monomer.store.resource", "norineMonomers.json");
    private JsonReader jsonReader = new JsonReader();

    public static NorineMonomerStore getInstance() {

        return ourInstance;
    }

    private NorineMonomerStore() {
        try {
            if (monomersResource.startsWith("http")){
                mapMonomersREST();
            }else{
                mapMonomersResources();
            }
        }catch (IOException e){

            throw new IllegalStateException("The monomers resource file needs to be the path to the norine JSON file or its REST link", e);
        }
    }

    //used when we read locally
    private void mapMonomersResources () throws IOException {

        InputStream fileMonomers = this.getClass().getResourceAsStream(monomersResource);
        JsonNode monomersNode = jsonReader.readFile(fileMonomers);
        addMonomers(monomersNode);

    }

    //used when we read from the REST resource
    private void mapMonomersREST () throws IOException {

        JsonNode monomersNode = jsonReader.readREST(monomersResource);
        addMonomers(monomersNode);

    }

    //helper method to add the monomers to the map (monomerMap)
    private void addMonomers(JsonNode monomersNode) throws IOException {

        List<NorineMonomer> listNorineMonomers = jsonReader.jsonNode2list(monomersNode, NorineMonomer.class);
        for (NorineMonomer norineObject : listNorineMonomers) {
            String monomerKey = norineObject.getCode();
            this.monomerMap.put(monomerKey, norineObject);

        }
    }

    //public method to extract a single monomer from the class. It requires to give the Symbol of this monomer.
    public NorineMonomer getMonomer(String monomerSymbol){

        Objects.nonNull(monomerSymbol);
        NorineMonomer norineMonomer = this.monomerMap.get(monomerSymbol);

        if(norineMonomer != null) {
           return norineMonomer;
        } else {
            throw new NullPointerException("NorineMonomer "+ monomerSymbol + " not found in norine monomers database.");
        }

    }

    //public method to get all the Monomers stored in monomerMap
    public List<NorineMonomer> getAllMonomers(){

        List<NorineMonomer> norineMonomerList =  new ArrayList<>();
        norineMonomerList.addAll(monomerMap.values());
        if (norineMonomerList.isEmpty()){
            throw new IllegalStateException("NorineMonomer Store is empty");
        }else{
            return norineMonomerList;
        }
    }



}
