package molecules.norine;

import com.fasterxml.jackson.databind.JsonNode;
import io.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Created by ericart on 20.02.2016.
 */
/**
 * The residues are read from the residues json output from Smiles2Monomers and stored in this class.
 */

public class NorineResidueStore {
    private static NorineResidueStore ourInstance = new NorineResidueStore();
    private final Map<String, NorineResidue> residuesMap = new HashMap<>();
    private String residuesResource =System.getProperty("residue.store.resource", "norineResidues.json");
    private JsonReader jsonReader = new JsonReader();

    public static NorineResidueStore getInstance() {

        return ourInstance;
    }

    private NorineResidueStore() {
        try {
            mapResiduesResources();
        }catch (IOException e){
            throw new IllegalStateException("The residues resource file needs to be the residues output from Smiles2Monomers", e);
        }
    }

    //used when we read locally
    private void mapResiduesResources() throws IOException {

        InputStream fileResidues = this.getClass().getResourceAsStream(residuesResource);
        JsonNode residuesNode = jsonReader.readFile(fileResidues);
        addResidues(residuesNode);

    }


    //helper method to add the residues to the map (residuesMap)
    private void addResidues(JsonNode residuesNode) throws IOException {

        List<NorineResidue> listNorineResidues = jsonReader.jsonNode2list(residuesNode, NorineResidue.class);
        for (NorineResidue norineObject : listNorineResidues) {
            String residueKey = norineObject.getId();
            this.residuesMap.put(residueKey, norineObject);
        }
    }

    //public method to extract a single residue from the class. It requires to give the Symbol of this residue.
    public NorineResidue getResidue(String residueSymbol){

        Objects.nonNull(residueSymbol);
        NorineResidue norineResidue = this.residuesMap.get(residueSymbol);

        if(norineResidue != null) {
            return norineResidue;
        } else {
            throw new NullPointerException("NorineResidue "+ residueSymbol + " not found in residues json file.");
        }

    }

    //public method to get all the Residues stored in residuesMap
    public List<NorineResidue> getAllResidues(){

        List<NorineResidue> norineResidueList =  new ArrayList<>();
        norineResidueList.addAll(residuesMap.values());
        if (norineResidueList.isEmpty()){
            throw new IllegalStateException("NorineResidue Store is empty");
        }else{
            return norineResidueList;
        }
    }


}
