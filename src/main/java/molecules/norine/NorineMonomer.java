package molecules.norine;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.expasy.mzjava.core.mol.Composition;
import serializers.MonomerSerializer;


/**
 * Created by ericart on 04.11.2015.
 */


/**
 * The information of the NorineMonomer class is retrieved from a Norine JSON file.
 */

@JsonSerialize(using = MonomerSerializer.class)
public class    NorineMonomer {

    private String code;
    private String nameAA;
    private String molecularFormula;
    private String pubchem;
    private double molecularWeight;
    private String smiles;
    private String isomeric;
    private Composition composition = null; //check optional class

    public String getCode(){

        return code;
    }

    public String getNameAA(){
        //String name = nameAA.replaceAll("’", "'");
        return nameAA;
    }

    public String getMolecularFormula(){

        return molecularFormula;
    }

    public String getPubchem() {

        return pubchem;
    }

    public double getMolecularWeight() {

        return molecularWeight;
    }

    public String getSmiles() {

        return smiles;
    }

    public String getIsomeric() {

        return isomeric;
    }

    public String toString() {

        return code;
    }

    public Composition getComposition() {
        if(composition == null) {
            this.composition =Composition.parseComposition(this.molecularFormula);
        }
        return composition;

    }


}
