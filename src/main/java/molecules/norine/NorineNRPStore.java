package molecules.norine;

import com.fasterxml.jackson.databind.JsonNode;
import io.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Created by ericart on 04.12.2015.
 */

/**
 * The NRPs from Norine are read from a JSON file (either locally or using a link to the REST JSON file) and stored
 * in this class.
 */


public class NorineNRPStore {

    private static NorineNRPStore ourInstance = new NorineNRPStore();
    private final Map<String, NorineNRP> NRPMap = new HashMap<>();
    private String NRPResource=System.getProperty("NRP.store.resource", "norineNRPs.json");
    private JsonReader jsonReader = new JsonReader();

    public static NorineNRPStore getInstance() {
        return ourInstance;
    }

    private NorineNRPStore()  {
        try {
            if (NRPResource.startsWith("http")){
                mapNRPREST();
            }else{
                mapNRPResources();
            }
        }catch (IOException e){
            throw new IllegalStateException("The NRP resource file needs to be the path to the norine JSON file or its REST link", e);
        }

    }

    //used when we read locally
    private void mapNRPResources () throws IOException {

        //InputStream filePeptides = dynamicClassLoader.getResourceAsStream(NRPResource);
        InputStream filePeptides  = this.getClass().getResourceAsStream(NRPResource);
        JsonNode rootnode = jsonReader.readFile(filePeptides);
        addNRPs(rootnode);

    }

    //used when we read from the REST resource
    private void mapNRPREST () throws IOException {

        JsonNode rootnode = jsonReader.readREST(NRPResource);
        addNRPs(rootnode);
    }

    //helper method to add the NRPs to the map (NRPMap)
    private void addNRPs(JsonNode rootnode) throws IOException {

        JsonNode peptidesNode = rootnode.path("peptides");
        List<NorineNRP> listpeptides = jsonReader.jsonNode2list(peptidesNode, NorineNRP.class);
        for (NorineNRP norineObject : listpeptides) {
            String monomerkey = norineObject.getId();
            this.NRPMap.put(monomerkey, norineObject);
        }

    }

    //public method to extract a single NRP from the class. It requires to give the Symbol of this NRP.
    public NorineNRP getNRP(String nrpSymbol){

        Objects.nonNull(nrpSymbol);
        NorineNRP norineNRP = this.NRPMap.get(nrpSymbol);

        if(norineNRP != null) {
            return norineNRP;
        } else {
            throw new NullPointerException("NRP " + nrpSymbol +" not found in norine NRPs database. Check if it has residues information.");
        }

    }

    //public method to get all the NRPs stored in NRPMap
    public List<NorineNRP> getAllNRPs(){

        List<NorineNRP> nrpList= new ArrayList<>();
        nrpList.addAll(NRPMap.values());
        if (nrpList.isEmpty()){
            throw new IllegalStateException("NRP Store is empty");
        }else{
            return nrpList;
        }
    }



}
