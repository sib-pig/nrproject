package molecules.norine;

import molecules.norine.NorineNRP;

import java.io.ByteArrayOutputStream;

/**
 * Created by ericart on 10.03.2016.
 */
public class NorineNRPChecker {

    NorineNRP norineNRP;
    public NorineNRPChecker(NorineNRP norineNRP) {
        this.norineNRP=norineNRP;
    }
    public void nonNullResidues(){
        if (this.norineNRP.structure.getResidues() == null){
            throw new NullPointerException(norineNRP.getId()+" can't be constructed because it doesn't have residues information, possibly missing smiles.");
        }
    }
    public ByteArrayOutputStream checkResiduesCorrectness(){
        if (this.norineNRP.structure.getCorrectness()<1){
            System.err.println("WARNING: "+norineNRP.getId()+" residues aren't completely correct.");
        }
        return null;
    }
    public void checkResiduesCoverage(){
        if (this.norineNRP.structure.getCoverage()<1){
            System.err.println("WARNING: "+norineNRP.getId()+" residues don't have a complete coverage.");
        }
    }
    public void checkSmilesVerified(){
        if (!this.norineNRP.structure.getSmilesVerified()){
            System.err.println("WARNING: "+norineNRP.getId()+" doesn't have a verified smiles.");
        }
    }

}
