package molecules.norine;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import serializers.NorineNRPSerializer;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by ericart on 09.11.2015.
 */


/**
 * The information of the NorineNRP class is retrieved from a Norine JSON file. It is possible
 * that some monomers lack of certain parameters (SMILES for example), because not all of them have
 * complete information.
 * Note that it takes some parameters that are not present in the original database ("residues" for instance). So it is
 * necessary to update the original file by precomputation.
 */

@JsonSerialize(using = NorineNRPSerializer.class)

public class NorineNRP {

    public General general;
    public Structure structure;
    public ArrayList<Link> link;


    public String getId(){

        return general.getId();
    }

    public String getName(){

        return general.getName();
    }

    public String getSmiles(){

        return structure.getSmiles();
    }

    public ArrayList<Link> getLinks (){

        return this.link;
    }


    public static class General {

        private String id;
        private String name;
        private String family;
        private String syno;
        private String category;
        private String formula;
        private double mw;
        private String status;


        public String getId(){

            return id;
        }

        public String getName(){

            return name;
        }

        public String getFamily(){

            return family;
        }

        public String getSyno() {

            return syno;
        }

        public String getCategory() {

            return category;
        }

        public String getFormula() {

            return formula;
        }

        public double getMW() {

            return mw;
        }

        public String getStatus() {

            return status;
        }

    }

    public static class Structure {

        private String type;
        private int size;
        private String graph;
        private String smiles;
        private boolean smilesVerified;
        private List<String> residues;
        private List<Idxs> residuesEdges;
        private int date;

        private double correctness;
        private double coverage;

        public Structure(@JsonProperty("graph") String graph){
            this.graph=graph;
        }

        public String getType(){

            return type;
        }

        public int getSize(){

            return size;
        }

        public String getGraph() {

            return graph;
        }

        public String getSmiles() {

            return smiles;
        }

        public boolean getSmilesVerified() {

            return smilesVerified;
        }

        public List<String> getResidues() {

            return residues;
        }

        public List<Idxs> getResiduesEdges() {

            return residuesEdges;
        }

        public double getCorrectness() {

            return correctness;
        }

        public double getCoverage() {

            return coverage;
        }


        public void setSmiles(String smiles){

            this.smiles=smiles;
        }

        public void setSmilesVerified(boolean smilesVerified){

            this.smilesVerified=smilesVerified;

        }

    }

    public static class Link{
        private String name_db;
        private String acc;

        public String getName_db() {
            return name_db;
        }

        public String getAcc() {
            return acc;
        }
    }

    public static class Idxs{
        private int[] idxs;

        public int[] getIdxs() {

            return idxs;

        }

    }


}
