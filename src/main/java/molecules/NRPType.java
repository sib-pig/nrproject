package molecules;

/**
 * Created by ericart on 04.04.2016.
 */
public enum NRPType {

    CYCLIC,
    LINEAR,
    DOUBLE_CYCLIC,
    BRANCHED,
    PARTIAL_CYCLIC,
    OTHER;

}

