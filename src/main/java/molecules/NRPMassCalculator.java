package molecules;

import com.google.common.base.Preconditions;
import com.google.common.collect.Collections2;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.mol.PeriodicTable;
//import tachyon.client.InStream;

import java.util.Collections;
import java.util.stream.DoubleStream;


/**
 * Created by ericart on 09.06.2016.
 */
public class NRPMassCalculator{

    public static double calculateMz(double nrpMass, Composition charge) {
        Preconditions.checkArgument(charge.getCharge() > 0, "Charge must be greater than 0");
        return (nrpMass + charge.getMolecularMass())/ charge.getCharge();
    }
    public static double calculateMz(double nrpMass, Composition charge, double neutralLosses, double modifications) {
        double totalMass =nrpMass+modifications-neutralLosses;
        return calculateMz(totalMass,charge);
    }


}
