package molecules;

import org.expasy.mzjava.core.mol.Composition;

/**
 * Created by ericart on 06.04.2016.
 */
public interface Compound {

    Composition getComposition();
}
