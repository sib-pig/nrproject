package molecules;

/**
 * Created by ericart on 09.11.2015.
 */


import molecules.norine.NorineMonomer;
import molecules.norine.NorineResidue;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.openscience.cdk.exception.CDKException;


/**
 * This class is used as a vertex in the MonomericGraph. It stores a NorineMonomer object and its symbol.
 */

public class Monomer implements Compound{

    private String monomerSymbol;
    private NorineMonomer norineMonomer;
    private int index;
    private NorineResidue norineResidue;
    private Composition composition;


    public Monomer(NorineMonomer norineMonomer, NorineResidue norineResidue, int index) throws CDKException {
        this.monomerSymbol= norineMonomer.getCode();
        this.norineMonomer = norineMonomer;
        this.index= index;
        this.norineResidue = norineResidue;
        this.composition= norineResidue.getComposition();
    }

    public String getMonomerSymbol (){

        return this.monomerSymbol;
    }
    public NorineMonomer getNorineMonomer(){

        return this.norineMonomer;
    }

    public NorineResidue getNorineResidue() {

        return this.norineResidue;
    }


    public Composition getComposition() {

        return composition;
    }



    public int getIndex(){

        return this.index;
    }




    @Override
    public String toString() {
        return this.monomerSymbol;
    }
}
