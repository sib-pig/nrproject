package deprecated;

import org.junit.Ignore;


/**
 * Created by ericart on 15.01.2016.
 */
@Ignore
public class MainAtomicFragmentation {
/*
    public static void modifyLibrary(String[] args) throws IOException, CDKException {

        long startTime = System.currentTimeMillis();
        ObjectMapper mapper = new ObjectMapper();
        List<molecules.NorineNRP> nrps = molecules.NorineNRPStore.getInstance().getAllNRPs();

        String serialFolder = "P:/NRProject/src/modifyLibrary/resources/atomicfragmentation/serials/";
        String monoDBname = "P:/NRProject/src/modifyLibrary/resources/atomicfragmentation/monomers.json";
        String pepDBname = "P:/NRProject/src/modifyLibrary/resources/atomicfragmentation/polymers.json";
        String rulesDBname = "P:/NRProject/src/modifyLibrary/resources/atomicfragmentation/rules.json";
        String residuesDBname = "P:/NRProject/src/modifyLibrary/resources/atomicfragmentation/residues.json";
        String chainsDBFile = "P:/NRProject/src/modifyLibrary/resources/atomicfragmentation/chains.json";

        String finalmonomers= "P:/NRProject/src/modifyLibrary/resources/atomicfragmentation/monomers_final.json";
        String finalpeptides = "P:/NRProject/src/modifyLibrary/resources/atomicfragmentation/polymers_final.json";


        String outfile = "P:/NRProject/src/modifyLibrary/resources/atomicfragmentation/results/coverages.json";
        String outfolderName = "P:/NRProject/src/modifyLibrary/resources/atomicfragmentation/results/";
        String imgsFoldername = "P:/NRProject/src/modifyLibrary/resources/atomicfragmentation/images/";



        if (! new File(outfolderName).exists())
            new File(outfolderName).mkdir();

        if (! new File(imgsFoldername).exists())
            new File(imgsFoldername).mkdir();

        Set<molecules.NorineMonomer> monomers4 = new HashSet<>();
        Set<molecules.NorineNRP> nrp4 = new HashSet<>();
        int counter=0;
        double totalnumberatoms=0;
        double totalcorrectness=0;
        double totalcoverage =0;
        for (NorineNRP nrp : nrps) {
            if (nrp.getSmiles() != null & nrp.general.getFormula() != null & !nrp.getId().equals("NOR00250")){

                Set<molecules.NorineMonomer> monomers3 = new HashSet<>();
                Set<molecules.NorineNRP> nrp3 = new HashSet<>();
                NRPBuilder monomericNRPBuilder = new NRPBuilder();
                System.out.println(nrp.getId());
                NRP monomericNRP = monomericNRPBuilder.createNRP(nrp);
                Set<Monomer> monomerNodeSet = monomericNRP.getAllMonomerNode();

                for (Monomer monomerNode : monomerNodeSet) {
                    monomers3.add(monomerNode.getNorineMonomer());
                    monomers4.add(monomerNode.getNorineMonomer());
                }

                nrp3.add(nrp);
                nrp4.add(nrp);

                mapper.writeValue(new File(monoDBname), monomers3);
                mapper.writeValue(new File(pepDBname), nrp3);

                System.out.println(nrp.getName());


                String subimgsFoldername=imgsFoldername+"/"+nrp.getName();
                String suboutfolderName=outfolderName+"/"+nrp.getName();
                double[] array=findMatches(serialFolder, monoDBname, pepDBname, rulesDBname, residuesDBname, chainsDBFile);
                String[] arguments ={"-rul", rulesDBname,"-mono", monoDBname, "-poly", pepDBname, "-res", residuesDBname, "-cha", chainsDBFile, "-serial", serialFolder, "-outfile",outfile,"-outfolder", suboutfolderName, "-imgs", subimgsFoldername, "-zip"};
                ProcessPolymers.modifyLibrary(arguments);

                FileUtils.deleteDirectory(new File(serialFolder));
                //File f = new File(monoDBname);
                //BufferedReader br = new BufferedReader (new FileReader(f));
                //br.close();
                //f.setWritable(true);
                //System.out.println(f.canExecute());
                //Files.de(Paths.get(monoDBname));
                //FileUtils.forceDeleteOnExit(new File(monoDBname));
                //FileUtils.forceDelete(new File(monoDBname));
                //FileUtils.forceDelete( new File(pepDBname));
                //FileUtils.forceDelete(new File(residuesDBname));
                //FileUtils.forceDelete(new File(chainsDBFile));
                totalnumberatoms=totalnumberatoms+array[2];
                totalcorrectness=totalcorrectness+array[1];
                totalcoverage=totalcoverage+array[0];

                counter++;
                //generate a json file with all the polymers analysed and another with all the monomers
            }

        }
        long endTime   = System.currentTimeMillis();
        long totalTime = endTime - startTime;

        mapper.writeValue(new File(finalmonomers), monomers4);
        mapper.writeValue(new File(finalpeptides), nrp4);

        System.out.println("Peptides analized: " + counter);

        System.out.println("Coverage : "+ totalcoverage/counter);
        System.out.println("Correctness : "+ totalcorrectness/counter);
        System.out.println("Seconds: "+ TimeUnit.MILLISECONDS.toSeconds(totalTime));
        System.out.println("Minutes: "+ TimeUnit.MILLISECONDS.toMinutes(totalTime));
    }


    private static double[] findMatches (String serialFolder, String monoDBname, String pepDBname, String rulesDBname, String residuesDBname, String chainsDBFile){
        double[] array = new double[3];
        String [] arg = {"-rul", rulesDBname, "-mono", monoDBname, "-poly", pepDBname, "-serial", serialFolder, "-res", residuesDBname, "-cha", chainsDBFile};
        PreComputation.modifyLibrary(arg);

        //------------------- Loadings ------------------------
        // Maybe loading can be faster for the learning base, using serialized molecules instead of CDK SMILES parsing method.

        MonomersDB monoDB = new MonomersJsonLoader(false).loadFile(monoDBname);
        //deserialize? seems to work without this step:
        //MonomersSerialization ms = new MonomersSerialization();
        //ms.deserialize(monoDB, serialFolder + "monos.serial");

        PolymersDB polDB = new PolymersJsonLoader(monoDB).loadFile(pepDBname);
        RulesDB rulesDB = RulesJsonLoader.loader.loadFile(rulesDBname);
        FamilyDB families = new ResidueJsonLoader(rulesDB, monoDB).loadFile(residuesDBname); // Need optimizations
        ChainsDB chains = new FamilyChainIO(families).loadFile(chainsDBFile);


        //------------------- Spliting ------------------------
        int removeDistance = 2;
        int retryCount = 2;
        int modulationDepth = 2;

        MonomericSpliting split = new MonomericSpliting(families, chains, removeDistance, retryCount, modulationDepth);
        split.setAllowLightMatchs(true);
        Coverage[] covs = split.computeCoverages(polDB);

        //emma edit start
        MonomerGraph monomerGraph = split.getCoverage().getMonomericGraph(families);
        NorineMonomer[] monomers = monomerGraph.nodes;
        //System.out.println("Coverages1: " + covs[0].getChemicalObject().getName());
        Coverage coverage = covs[0];
        IMolecule molecule = covs[0].getMolecule(false);

        for (IAtom a:molecule.atoms()){
            //System.out.println(a.getSymbol());
        }

        for (IBond b: molecule.bonds()){
            //System.out.println(b.getAtomCount());
        }

        //System.out.println("Coverages2: " + monomers[0].getId());

        IMolecule mol = coverage.getMolecule(false);


        for (Match match : coverage.getUsedMatches())
        {

            NorineResidue res = match.getNorineResidue();
            System.out.println(res.getMonoName());

            for (int idx : match.getAtoms())
            {
                //System.out.println(coverage.getMolecule(true).getAtom(idx).getSymbol());
                List<IBond> bondslist =mol.getConnectedBondsList(coverage.getMolecule(true).getAtom(idx));
                for (IBond bond : bondslist){
                    //System.out.println(bond);
                    for ( IAtom atom : bond.atoms()){
                        //System.out.print(atom.getSymbol() + ":");
                    }
                    //System.out.println("");
                }

            }


        }

        System.out.println("Correct monomers : "+coverage.getCorrectMonomers(families));
        System.out.println("Incorrect monomers : "+coverage.getIncorrectMonomers(families));
        System.out.println("Coverage : "+coverage.getCoverageRatio());
        System.out.println("Correctness : " +coverage.getCorrectness(families));
        array[0]=coverage.getCoverageRatio();
        array[1]=coverage.getCorrectness(families);
        array[2]=molecule.getAtomCount();
        return array;
    }
*/
}













