package deprecated.xml;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Created by ericart on 05.10.2015.
 */
//http://www.mkyong.com/java/how-to-read-xml-file-in-java-sax-parser/
public class ReadXMLFile {
    public static void main(String argv[]) {

        try {

            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();

            DefaultHandler handler = new DefaultHandler() {

                boolean bfname = false;

                public void startElement(String uri, String localName,String qName, Attributes attributes) throws SAXException {
                    if (qName.equalsIgnoreCase("FORMULA")) {
                        bfname = true;
                    }
                }

                public void characters(char ch[], int start, int length) throws SAXException {

                    if (bfname) {
                        //System.out.println("Smiles : " + new String(ch, start, length));
                        bfname = false;
                    }
                }

            };

            saxParser.parse("c:\\Users\\ericart\\Downloads\\NorineXML.xml", handler);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
