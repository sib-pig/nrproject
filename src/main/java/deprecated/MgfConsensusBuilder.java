package deprecated;

import com.google.common.base.Preconditions;
import org.apache.commons.io.FilenameUtils;
import org.expasy.mzjava.core.io.ms.spectrum.MgfReader;
import org.expasy.mzjava.core.io.ms.spectrum.MgfWriter;
import org.expasy.mzjava.core.ms.consensus.ConsensusSpectrum;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.core.ms.peaklist.peakfilter.AbstractMergePeakFilter;
import org.expasy.mzjava.core.ms.peaklist.peakfilter.NPeaksPerSlidingWindowFilter;
import org.expasy.mzjava.core.ms.spectrum.LibPeakAnnotation;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;

import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class MgfConsensusBuilder {

    private Pattern commentPattern;

    public MgfConsensusBuilder(){
        commentPattern = Pattern.compile("Spot_Id: (\\d+), Peak_List_Id: (\\d+), ");
    }

    private void run(File mgfDir, File destDir) throws IOException {

        double peakFraction = 0.2;
        int minPeakCount = 2;

        NPeaksPerSlidingWindowFilter<PeakAnnotation> perSlidingWindowFilter =
                new NPeaksPerSlidingWindowFilter<>(2, 10.0, 0);

        PeakProcessorChain<PeakAnnotation> peakProcessorChain = new PeakProcessorChain<>();
        peakProcessorChain.add(perSlidingWindowFilter);

        Preconditions.checkArgument(mgfDir.isDirectory());
        Preconditions.checkArgument(destDir.isDirectory());

        FilenameFilter mgfFilter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {

                return name.toLowerCase().endsWith(".mgf") && !name.contains("consensus");
            }
        };

        PeakList.Precision precision = PeakList.Precision.DOUBLE;

        for(File mgfFile : mgfDir.listFiles(mgfFilter)) {

            System.out.println("Reading " + mgfFile.getAbsolutePath());                      //sout

            MgfReader reader = new MgfReader(mgfFile, precision);
            String name = FilenameUtils.removeExtension(mgfFile.getName());
            File mgfFileCons = new File(destDir, name+"_consensus.mgf");
            File txtFileCons = new File(destDir, name+"_consensus.txt");

            List<PeakList<PeakAnnotation>> spectra = new ArrayList<>();
            MsnSpectrum spectrum;

            while (reader.hasNext()) {
                spectrum = reader.next();
                spectrum.apply(peakProcessorChain);
                spectra.add(spectrum);
            }

            if (spectra.size()==0) return;

            ConsensusSpectrum<LibPeakAnnotation> consensus =
                    ConsensusSpectrum.Builder.getBuilder().
                            spectra(spectra).setPeakFilterParams(peakFraction,minPeakCount).
                            fragMzTolerance(0.3).
                            intensityCombMethod(AbstractMergePeakFilter.IntensityMode.SUM_INTENSITY).
                            build();

            System.out.println("Writing mgf file " + mgfFileCons.getAbsolutePath());
            MgfWriter mgfWriter = new MgfWriter(mgfFileCons, precision);
            mgfWriter.write(copy2MSnSpectrum(consensus));
            if(mgfWriter != null) mgfWriter.close();

            System.out.println("Writing text file " + mgfFileCons.getAbsolutePath());
            FileWriter txtWriter = new FileWriter(txtFileCons);

            txtWriter.write("PrecursorMz: "+consensus.getPrecursor().getMz()+", PrecursorCharge: "+consensus.getPrecursor().getCharge()+", Nr. of spectra: "+spectra.size()+"\n");
            for (int i=0;i<consensus.size();i++) {
                txtWriter.write(consensus.getMz(i)+"\t");
                txtWriter.write(consensus.getIntensity(i)+"\t");
                List<LibPeakAnnotation> annots = consensus.getAnnotations(i);
                if (annots!=null && annots.size()>0)
                    txtWriter.write(annots.get(0).getMergedPeakCount()+"\t");
                else
                    txtWriter.write("\t");
                txtWriter.write("\n");
                txtWriter.flush();
            }
            txtWriter.close();

        }
    }



    public static void main(String[] args) throws IOException {

        File mgfDir = new File("P:\\NRProject\\src\\main\\resources\\spectrum\\Fengycin");
        File destDir = new File("P:\\NRProject\\src\\main\\resources\\spectrum\\Fengycin");

        new MgfConsensusBuilder().run(mgfDir, destDir);
    }

    private MsnSpectrum copy2MSnSpectrum(ConsensusSpectrum<LibPeakAnnotation> consensus) {

        MsnSpectrum msnSpectrum = new MsnSpectrum(consensus.size(),PeakList.Precision.DOUBLE);

        msnSpectrum.setPrecursor(consensus.getPrecursor());
        for (int i=0;i<consensus.size();i++) {
            msnSpectrum.add(consensus.getMz(i),consensus.getIntensity(i));
        }

        String comment = "";
        for (UUID id : consensus.getMemberIds()) {
            if (!comment.isEmpty()) comment += "";
            comment += id.toString();
        }
        msnSpectrum.setComment(comment);

        return msnSpectrum;
    }


}
