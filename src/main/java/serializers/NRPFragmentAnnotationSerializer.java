package serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import molecules.Bond;
import molecules.Monomer;
import ms.spectrum.NRPFragmentAnnotation;

import java.io.IOException;
import java.util.Set;

/**
 * Created by ericart on 10.05.2016.
 */
public class NRPFragmentAnnotationSerializer extends JsonSerializer<NRPFragmentAnnotation> {

    @Override
    public void serialize(NRPFragmentAnnotation nrpFragmentAnnotation, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {

        jsonGenerator.writeStartObject();
            jsonGenerator.writeStringField("precursorId", nrpFragmentAnnotation.getFragment().getPrecursorId());
            //jsonGenerator.writeStringField("monomersOriginal",  nrpFragmentAnnotation.getFragment().ge);
        //we print the graph object in the appropiate format for Smiles2Monomers
            jsonGenerator.writeObjectFieldStart("fragment");

                jsonGenerator.writeArrayFieldStart("bonds");
                    for (Bond edge : nrpFragmentAnnotation.getFragment().getAllBond()){
                        jsonGenerator.writeStartArray();
                            jsonGenerator.writeNumber(edge.getMonomerNodeSource().getIndex());
                            jsonGenerator.writeNumber(edge.getMonomerNodeTarget().getIndex());
                        jsonGenerator.writeEndArray();
                    }
                jsonGenerator.writeEndArray();

                jsonGenerator.writeArrayFieldStart("monomers");
                ObjectMapper mapper = new ObjectMapper();
                    for (Monomer monomerCode :nrpFragmentAnnotation.getFragment().getAllMonomerNode()){
                        String monomerString=mapper.writeValueAsString(monomerCode.getMonomerSymbol()+"("+monomerCode.getIndex()+")").replace("\"","");

                        jsonGenerator.writeString(monomerString);
                    }


                jsonGenerator.writeEndArray();
            jsonGenerator.writeEndObject();
            jsonGenerator.writeStringField("modifiedMonomers",  nrpFragmentAnnotation.getAllModifiedMonomers().toString());
            jsonGenerator.writeStringField("modifications",   nrpFragmentAnnotation.getModifications().toString());
            jsonGenerator.writeStringField("chargedGroup",   nrpFragmentAnnotation.getChargedGroup().toString());
            jsonGenerator.writeNumberField("fragmentationStage",   nrpFragmentAnnotation.getFragment().getFragmentationStage());
        jsonGenerator.writeEndObject();

    }
}
