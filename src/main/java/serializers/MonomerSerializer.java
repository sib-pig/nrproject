package serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import molecules.norine.NorineMonomer;

import java.io.IOException;

/**
 * Created by ericart on 18.01.2016.
 */
public class MonomerSerializer extends JsonSerializer<NorineMonomer> {
    @Override
    public void serialize (NorineMonomer norineMonomer, JsonGenerator jsonGenerator, SerializerProvider serializerProvider ) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("code", norineMonomer.getCode());
        jsonGenerator.writeStringField("nameAA", norineMonomer.getNameAA());
        jsonGenerator.writeStringField("smiles", norineMonomer.getSmiles());
        jsonGenerator.writeEndObject();
    }
}

