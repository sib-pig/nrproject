package serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import molecules.norine.NorineNRP;

import java.io.IOException;
import java.util.*;

/**
 * Created by ericart on 18.01.2016.
 */

/**
 * NorineNRPSerializer is used to serialize the class NorineNRP in a valid json format for Smiles2Monomers input (peptides).
 * Note that the graph has to be modified because Norine database has a different graph annotation than Smiles2Monomers.
 */

public class NorineNRPSerializer extends JsonSerializer<NorineNRP> {

    @Override
    public void serialize (NorineNRP norineNRP,JsonGenerator jsonGenerator, SerializerProvider serializerProvider ) throws IOException {
        if (norineNRP.structure.getSmiles() != null){

            String NRPgraphString= norineNRP.structure.getGraph();

            //we split the string to separate the NorineMonomer names and the connections between them in two different lists
            List<String> stringEdges = new ArrayList<>(Arrays.asList(NRPgraphString.split("@")));
            String namesMonomers =stringEdges.remove(0);
            Set<Set<Integer>> setEdges= getSetEdges(stringEdges);

            List<String> listMonomers = new ArrayList<>(Arrays.asList(namesMonomers.split(",")));

            jsonGenerator.writeStartObject();
                jsonGenerator.writeStringField("id", norineNRP.getId());

                //we print the graph object in the appropiate format for Smiles2Monomers
                jsonGenerator.writeObjectFieldStart("graph");
                    jsonGenerator.writeArrayFieldStart("E");
                        for (Set<Integer> edge : setEdges){
                            jsonGenerator.writeStartArray();
                                for (Integer edgePoint : edge){
                                    jsonGenerator.writeNumber(edgePoint);
                                }
                            jsonGenerator.writeEndArray();
                        }
                    jsonGenerator.writeEndArray();

                    jsonGenerator.writeArrayFieldStart("V");
                        for (String monomerCode :listMonomers)
                            jsonGenerator.writeString(monomerCode);
                    jsonGenerator.writeEndArray();
                jsonGenerator.writeEndObject();

                jsonGenerator.writeStringField("name", norineNRP.general.getName());

                jsonGenerator.writeStringField("smiles", norineNRP.structure.getSmiles());
            jsonGenerator.writeEndObject();

        }
    }


    private  Set<Set<Integer>> getSetEdges(List<String> stringEdges){
        Set<Set<Integer>> setEdges = new HashSet<>();

        for (int indexVertex=0; indexVertex<stringEdges.size(); indexVertex++) {
            String neighbors = stringEdges.get(indexVertex);
            List<String> listNeighbors = new ArrayList<>(Arrays.asList(neighbors.split(",")));

            for (String neighbor : listNeighbors){

                Set<Integer> edge = new HashSet<>();
                int indexNeighbor = Integer.parseInt(neighbor);
                edge.add(indexVertex);
                edge.add(indexNeighbor);
                setEdges.add(edge);

            }

        }
        return setEdges;
    }



}
