package serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import molecules.NRPFragment;
import ms.fragment.Loss;
import ms.fragment.NRPFragmentTree;

import org.jgrapht.graph.SimpleDirectedGraph;
import org.jgrapht.traverse.DepthFirstIterator;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ericart on 13.09.2016.
 */
public class TreeSerializer extends JsonSerializer<NRPFragmentTree> {

    @Override
    public void serialize(NRPFragmentTree nrpFragmentTree,JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {

        SimpleDirectedGraph simpleGraph =nrpFragmentTree.getTree();
        Map<NRPFragment,Integer> map = new HashMap<>();

        DepthFirstIterator depthFirstIterator = new DepthFirstIterator(simpleGraph,nrpFragmentTree.getRoot());
        NRPFragment actualFragment = (NRPFragment) depthFirstIterator.next();

        int actualLevel=1;
        int levelDiff;

        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("name", Double.toString(actualFragment.getExperimentalMz()));
            jsonGenerator.writeStringField("state",actualFragment.getState().toString());
            jsonGenerator.writeNumberField("intensity",actualFragment.getExperimentalIntensity());
            jsonGenerator.writeBooleanField("unique",nrpFragmentTree.isUnique(actualFragment));
            jsonGenerator.writeStringField("graphID",actualFragment.getGraphID());
            jsonGenerator.writeArrayFieldStart("children");
            map.put(actualFragment,actualLevel);

        int previousLevel=actualLevel;

        while (depthFirstIterator.hasNext()){

            actualFragment = (NRPFragment) depthFirstIterator.next();
            Loss incomingEdge = (Loss) simpleGraph.incomingEdgesOf(actualFragment).iterator().next();
            actualLevel =map.get(simpleGraph.getEdgeSource(incomingEdge))+1;
            levelDiff =actualLevel-previousLevel;

            if (levelDiff>=0){

                jsonGenerator.writeStartObject();
                jsonGenerator.writeStringField("name", Double.toString(actualFragment.getExperimentalMz()));
                jsonGenerator.writeStringField("state",actualFragment.getState().toString());
                jsonGenerator.writeNumberField("intensity",actualFragment.getExperimentalIntensity());
                jsonGenerator.writeBooleanField("unique",nrpFragmentTree.isUnique(actualFragment));
                jsonGenerator.writeStringField("graphID",actualFragment.getGraphID());
                if( simpleGraph.outgoingEdgesOf(actualFragment).size()!=0){
                    jsonGenerator.writeArrayFieldStart("children");
                }else{
                    jsonGenerator.writeEndObject();
                }
            }
            else if (levelDiff<0){

                for (int i=1;i<=Math.abs(levelDiff);i++){
                        jsonGenerator.writeEndArray();
                        jsonGenerator.writeEndObject();
                }

                jsonGenerator.writeStartObject();
                jsonGenerator.writeStringField("name", Double.toString(actualFragment.getExperimentalMz()));
                jsonGenerator.writeStringField("state",actualFragment.getState().toString());
                jsonGenerator.writeNumberField("intensity",actualFragment.getExperimentalIntensity());
                jsonGenerator.writeBooleanField("unique",nrpFragmentTree.isUnique(actualFragment));
                jsonGenerator.writeStringField("graphID",actualFragment.getGraphID());
                if( simpleGraph.outgoingEdgesOf(actualFragment).size()!=0){
                    jsonGenerator.writeArrayFieldStart("children");
                }else{
                    jsonGenerator.writeEndObject();
                }

            }

            map.put(actualFragment,actualLevel);
            previousLevel=actualLevel;
        }

        levelDiff =1-previousLevel;
        for (int i=1;i<=Math.abs(levelDiff);i++){
            jsonGenerator.writeEndArray();
            jsonGenerator.writeEndObject();
        }

    }


}
