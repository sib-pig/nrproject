package utils.temp;

import molecules.NRP;
import molecules.NRPBuilder;
import molecules.norine.NorineNRP;
import molecules.norine.NorineNRPStore;

/**
 * Created by ericart on 22.09.2016.
 */
public class NumberPossibleNRPs {
    public static void main(String[] args) {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();

        NRPBuilder nrpBuilder = new NRPBuilder();

        int createdNRPs=0;
        int notCreatedNRPs=0;
        int dontHaveSMILES =0;
        int haveSMILES =0;

        for(NorineNRP norineNRP:norineNRPStore.getAllNRPs()){
            try{

                NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP(norineNRP.getId()));
                createdNRPs++;

            } catch(Exception ex) {
                if(norineNRP.getSmiles()==null){
                    dontHaveSMILES++;

                }else{
                    haveSMILES++;
                }


                notCreatedNRPs++;
            }

        }
        System.out.println("Total NRPs in Norine= "+norineNRPStore.getAllNRPs().size());
        System.out.println("Possible to analyze= "+createdNRPs);
        System.out.println("NOT possible to analyze= "+notCreatedNRPs);
        System.out.println("NOT possible because missing Smiles= "+dontHaveSMILES);
        System.out.println("NOT possible because other issues= "+haveSMILES);

    }
}
