package utils;

import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.silent.SilentChemObjectBuilder;
import org.openscience.cdk.smiles.SmilesParser;


import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * Created by ericart on 07.03.2016.
 */


public class SmilesUtils {

    public static String smilesToFormula(String smiles, boolean implicitHydrogens) throws CDKException {

        String formula ="";
        SmilesParser sp  = new SmilesParser(SilentChemObjectBuilder.getInstance());
        IAtomContainer iAtomContainer = sp.parseSmiles(smiles);

        Map<String,AtomicInteger> atomOcurrences = new HashMap<>();

        for (IAtom iAtom :iAtomContainer.atoms()){
            if (implicitHydrogens){

                if (iAtom.getImplicitHydrogenCount()!=0){

                    if (atomOcurrences.containsKey("H")){
                        for (int i=0;i<iAtom.getImplicitHydrogenCount();i++)
                            atomOcurrences.get("H").incrementAndGet();
                    }else{
                        AtomicInteger initialValue = new AtomicInteger(iAtom.getImplicitHydrogenCount());
                        atomOcurrences.put("H",initialValue);
                    }
                }
            }

            if (atomOcurrences.containsKey(iAtom.getSymbol())){
                atomOcurrences.get(iAtom.getSymbol()).incrementAndGet();

            }else{

                AtomicInteger initialValue = new AtomicInteger(1);
                atomOcurrences.put(iAtom.getSymbol(),initialValue);

            }

        }

        for (String atom: atomOcurrences.keySet()){
            if (atomOcurrences.get(atom).intValue() != 1){
                formula=formula+atom+atomOcurrences.get(atom).toString();
            }else{
                formula = formula+atom;
            }
        }

        return formula;
    }


}

