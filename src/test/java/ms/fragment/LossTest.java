package ms.fragment;

import molecules.*;
import molecules.norine.NorineNRP;
import molecules.norine.NorineNRPStore;
import ms.fragment.Loss;
import org.jgrapht.graph.SimpleGraph;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ericart on 12.03.2016.
 */
public class LossTest {

    @Test
    public void testGetM1() throws Exception {
        NorineNRP norineNRP1 = NorineNRPStore.getInstance().getNRP("NOR00601");
        NRP nrp1 = new NRPBuilder().createNRP(norineNRP1);
        SimpleGraph<Monomer,Bond> clonedGraph= nrp1.cloneSimpleGraph();
        Bond bond= (Bond) nrp1.getAllBond().toArray()[1];
        clonedGraph.removeEdge(bond);
        NRPFragment monomericNrp2 = new NRPFragment(clonedGraph, nrp1.getComposition(), nrp1.getNrpId(),0);
        Loss loss =new Loss(nrp1, monomericNrp2, bond);
        assertEquals(nrp1,loss.getM1());
    }

    @Test
    public void testGetM2() throws Exception {
        NorineNRP norineNRP1 = NorineNRPStore.getInstance().getNRP("NOR00601");
        NRP nrp1 = new NRPBuilder().createNRP(norineNRP1);
        SimpleGraph<Monomer,Bond> clonedGraph= nrp1.cloneSimpleGraph();
        Bond bond= (Bond) nrp1.getAllBond().toArray()[1];
        clonedGraph.removeEdge(bond);
        NRPFragment monomericNrp2 = new NRPFragment(clonedGraph, nrp1.getComposition(), nrp1.getNrpId(),0);
        Loss loss =new Loss(nrp1, monomericNrp2, bond);
        assertEquals(monomericNrp2,loss.getM2());
    }

    @Test
    public void testGetLostEdge() throws Exception {
        NorineNRP norineNRP1 = NorineNRPStore.getInstance().getNRP("NOR00601");
        NRP nrp1 = new NRPBuilder().createNRP(norineNRP1);
        SimpleGraph<Monomer,Bond> clonedGraph= nrp1.cloneSimpleGraph();
        Bond bond= (Bond) nrp1.getAllBond().toArray()[1];
        clonedGraph.removeEdge(bond);
        NRPFragment monomericNrp2 = new NRPFragment(clonedGraph, nrp1.getComposition(), nrp1.getNrpId(),0);
        Loss loss =new Loss(nrp1, monomericNrp2, bond);
        assertEquals(bond,loss.getLostEdge());

    }

    @Test
    public void testToString() throws Exception {
        NorineNRP norineNRP1 = NorineNRPStore.getInstance().getNRP("NOR00601");
        NRP nrp1 = new NRPBuilder().createNRP(norineNRP1);
        SimpleGraph<Monomer,Bond> clonedGraph= nrp1.cloneSimpleGraph();
        Bond bond= (Bond) nrp1.getAllBond().toArray()[1];
        clonedGraph.removeEdge(bond);
        NRPFragment monomericNrp2 = new NRPFragment(clonedGraph, nrp1.getComposition(), nrp1.getNrpId(),0);
        Loss loss =new Loss(nrp1, monomericNrp2, bond);
        assertEquals("(Ala : D-I-NMe-Tyr , AMINO)",loss.toString());

    }
}