package ms.fragment;


import molecules.*;
import molecules.norine.NorineNRPStore;
import org.junit.Test;
import java.util.HashMap;
import java.util.Map;


import static org.junit.Assert.*;


/**
 * Created by ericart on 15.12.2015.
 */
public class NRPFragmenterTest {
    //we test a linear peptide

    @Test
    public void testCreateFragmentationTree() throws Exception {

        Map<String,Double> fragmentsNOR00485 = new HashMap<>();
        fragmentsNOR00485.put("[Me-Suc, bPhe, C8:3(t2.t4.t6), D-Val]",479.242021181);
        fragmentsNOR00485.put("[bPhe, C8:3(t2.t4.t6), D-Val]",367.20216774);
        fragmentsNOR00485.put("[Me-Suc]",112.0398534401);
        fragmentsNOR00485.put("[C8:3(t2.t4.t6)]",121.065331);
        fragmentsNOR00485.put("[Me-Suc, bPhe, D-Val]",358.176681271);
        fragmentsNOR00485.put("[bPhe, C8:3(t2.t4.t6)]",268.133753825);
        fragmentsNOR00485.put("[Me-Suc, D-Val]",211.108267356);
        fragmentsNOR00485.put("[bPhe, D-Val]",246.13682783);
        fragmentsNOR00485.put("[D-Val]",99.068413915);
        fragmentsNOR00485.put("[bPhe]",147.068413915);


        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00485"));

        NRPFragmenter nrpFragmenter = new NRPFragmenter();
        NRPFragmentTree fragmentationTree = nrpFragmenter.createFragmentationTree(nrp);
        //ACTIVATE AGAIN ONCE FIXED!!
        //assertTrue(fragmentationTree.incomingEdgesOf(nrp).isEmpty());
        //assertEquals(6,fragmentationTree.outgoingEdgesOf(nrp).size());
        assertEquals(10, fragmentationTree.getSetNotRepeatedFragment().size());


        for (MonomericGraph monomericGraph:fragmentationTree.getSetNotRepeatedFragment()){
            assertEquals(fragmentsNOR00485.get(monomericGraph.toString()),monomericGraph.getMolecularMass(),0.00001);
        }

    }
    //we test a cyclic peptide

    @Test
    public void testCreateFragmentationTree2() throws Exception {
        Map<String,Double> fragmentsNOR00601 = new HashMap<>();

        fragmentsNOR00601.put("[Ala, C9:1(4)-Me(2.4.6)-OH(8), bTyr, D-I-NMe-Tyr]",733.222393777);
        fragmentsNOR00601.put("[C9:1(4)-Me(2.4.6)-OH(8), bTyr, D-I-NMe-Tyr]",662.18528);
        fragmentsNOR00601.put("[Ala]",71.037113787);
        fragmentsNOR00601.put("[C9:1(4)-Me(2.4.6)-OH(8)]",196.146329884);
        fragmentsNOR00601.put("[Ala, bTyr, D-I-NMe-Tyr]",537.076063893);
        fragmentsNOR00601.put("[C9:1(4)-Me(2.4.6)-OH(8), bTyr]",359.209658421);
        fragmentsNOR00601.put("[Ala, D-I-NMe-Tyr]",374.012735356);
        fragmentsNOR00601.put("[bTyr, D-I-NMe-Tyr]",466.038950106);
        fragmentsNOR00601.put("[Ala, C9:1(4)-Me(2.4.6)-OH(8)]",267.183443671);
        fragmentsNOR00601.put("[D-I-NMe-Tyr]",302.975621569);
        fragmentsNOR00601.put("[Ala, C9:1(4)-Me(2.4.6)-OH(8), bTyr]",430.246772208);
        fragmentsNOR00601.put("[bTyr]",163.063328537);
        fragmentsNOR00601.put("[Ala, C9:1(4)-Me(2.4.6)-OH(8), D-I-NMe-Tyr]",570.15906524);


        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00601"));

        NRPFragmenter nrpFragmenter = new NRPFragmenter();
        NRPFragmentTree fragmentationTree = nrpFragmenter.createFragmentationTree(nrp);


        assertEquals("NOR00601", fragmentationTree.getRoot().getPrecursorId());
        assertEquals(nrp.getAllMonomerNode(), fragmentationTree.getRoot().getAllMonomerNode());
        assertEquals(0, fragmentationTree.getRoot().getFragmentationStage());
        assertEquals(17, fragmentationTree.getSetNotRepeatedFragment().size());
        for (MonomericGraph monomericGraph:fragmentationTree.getSetNotRepeatedFragment()){
            assertEquals(fragmentsNOR00601.get(monomericGraph.toString()),monomericGraph.getMolecularMass(),0.00001);
        }

    }

    //branched??

}