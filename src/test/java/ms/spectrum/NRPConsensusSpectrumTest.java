package ms.spectrum;

import molecules.NRP;
import molecules.NRPBuilder;
import molecules.NRPFragment;
import molecules.norine.NorineNRPStore;
import ms.fragment.Loss;
import ms.fragment.NRPFragmentTree;
import ms.fragment.NRPFragmenter;
import org.expasy.mzjava.core.mol.AtomicSymbol;
import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.peakfilter.AbstractMergePeakFilter;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.hamcrest.core.IsEqual;
import org.hamcrest.core.IsNot;
import org.junit.Assert;
import org.junit.Test;

import java.net.URI;
import java.util.*;

import static org.junit.Assert.*;

/**
 * Created by ericart on 15.06.2016.
 */
public class NRPConsensusSpectrumTest {
    @Test
    public void testgetNRP() throws Exception {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00487"));
        NRPConsensusSpectrum nrpConsensusSpectrum1 = new NRPConsensusSpectrum(nrp, PeakList.Precision.DOUBLE,new HashSet<>());
        assertEquals(nrp,nrpConsensusSpectrum1.getNRP());
    }

    @Test
    public void testStatus() throws Exception {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00487"));
        NRPConsensusSpectrum nrpConsensusSpectrum1 = new NRPConsensusSpectrum(nrp, PeakList.Precision.DOUBLE,new HashSet<>());
        assertEquals(NRPConsensusSpectrum.Status.UNKNOWN,nrpConsensusSpectrum1.getStatus());
        nrpConsensusSpectrum1.setStatus(NRPConsensusSpectrum.Status.INQUORATE_UNCONFIRMED);
        assertEquals(NRPConsensusSpectrum.Status.INQUORATE_UNCONFIRMED,nrpConsensusSpectrum1.getStatus());
    }

    @Test
    public void testSpectrumSource() throws Exception {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00487"));
        NRPConsensusSpectrum nrpConsensusSpectrum1 = new NRPConsensusSpectrum(nrp, PeakList.Precision.DOUBLE,new HashSet<>());
        nrpConsensusSpectrum1.setSpectrumSource(new URI("org.expasy"));
        assertEquals("org.expasy",nrpConsensusSpectrum1.getSpectrumSource().toString());

    }

    @Test
    public void testMzsIntensities() throws Exception {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00487"));
        NRPFragmenter NRPFragmenter = new NRPFragmenter();
        NRPFragmentTree simpleDirectedGraph= NRPFragmenter.createFragmentationTree(nrp);
        NRPPeaksGenerator.Builder peaksGeneratorBuilder = new NRPPeaksGenerator.Builder(simpleDirectedGraph);
        peaksGeneratorBuilder.addIon(AtomicSymbol.H,1,nrp.getAllMonomerNode());
        NRPPeaksGenerator peaksGenerator= peaksGeneratorBuilder.build();
        List<NRPSpectrum> peaks =peaksGenerator.generatePeaks();

        Set<MsnSpectrum> experimentalSpectra1 = new HashSet<>();

        MsnSpectrum spectrum = new MsnSpectrum();
        spectrum.add(114.09134008669999, 10.0);
        spectrum.add(157.10972980469998, 11.0);
        spectrum.add(158.1162121657, 12.0);
        spectrum.add(200.1162121657, 10.0);
        spectrum.add(270.1937937837, 12.0);
        spectrum.add(271.2002761447, 10.0);
        spectrum.add(300.1162121657, 13.0);
        spectrum.add(350.1162121657, 10.0);
        spectrum.add(400.1162121657, 11.0);
        spectrum.add(427.3027298417, 10.0);
        spectrum.setPrecursor(new Peak(426.30272,10.0,1));
        experimentalSpectra1.add(spectrum);


        NRPConsensusSpectrum nrpConsensusSpectrum1 = NRPConsensusSpectrum.builder(PeakList.Precision.DOUBLE,new URI("unknownSource")).
                setConsensusParameters(0.2,0.2, AbstractMergePeakFilter.IntensityMode.MEAN_ALL_INTENSITY).
                setAnnotationParameters(new AbsoluteTolerance(0.2)).
                setFilterParameters(0.2,2).
                buildConsensus(1,nrp,peaks.get(0),experimentalSpectra1);


        double[] mzList = {114.09134008669999,157.10972980469998,158.1162121657,200.1162121657,270.1937937837,271.2002761447,300.1162121657,350.1162121657,400.1162121657,427.3027298417};
        assertTrue(Arrays.equals(mzList, nrpConsensusSpectrum1.getMzs(new double[10])));
        assertEquals(200.1162121657,nrpConsensusSpectrum1.getMz(3),0);

        double[] intensitiesList = {10.0, 11.0, 12.0, 10.0,12.0, 10.0, 13.0, 10.0,11.0, 10.0};
        assertTrue(Arrays.equals(intensitiesList, nrpConsensusSpectrum1.getIntensities(new double[10])));
        assertEquals(10.00,nrpConsensusSpectrum1.getIntensity(3),0);
        assertEquals(6,nrpConsensusSpectrum1.getMostIntenseIndex());
        assertEquals(109,nrpConsensusSpectrum1.getTotalIonCurrent(),0);

        nrpConsensusSpectrum1.setIntensityAt(20.0,0);
        assertEquals(20.0,nrpConsensusSpectrum1.getIntensity(0),0);

    }

    @Test
    public void testNRPConsensusSpectrumCreation() throws Exception {
        /* ASK MARKUS
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00487"));
        NRPFragment nrpFragment = new NRPFragment(nrp.cloneSimpleGraph(),nrp.getComposition(),nrp.getNrpId(),1);

        NRPFragmentAnnotation nrpFragmentAnnotation = new NRPFragmentAnnotation(1,nrpFragment,nrpFragment.getComposition());
        NRPLibPeakAnnotation libPeakAnnotation1 = new NRPLibPeakAnnotation(1,0.0,0.0, Optional.of(nrpFragmentAnnotation));
        */

    }

    @Test
    public void testEquals() throws Exception {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00487"));
        NRPFragmenter NRPFragmenter = new NRPFragmenter();
        NRPFragmentTree simpleDirectedGraph= NRPFragmenter.createFragmentationTree(nrp);
        NRPPeaksGenerator.Builder peaksGeneratorBuilder = new NRPPeaksGenerator.Builder(simpleDirectedGraph);
        peaksGeneratorBuilder.addIon(AtomicSymbol.H,1,nrp.getAllMonomerNode());
        NRPPeaksGenerator peaksGenerator= peaksGeneratorBuilder.build();
        List<NRPSpectrum> peaks =peaksGenerator.generatePeaks();

        Set<MsnSpectrum> experimentalSpectra1 = new HashSet<>();
        Set<MsnSpectrum> experimentalSpectra2 = new HashSet<>();

        MsnSpectrum spectrum = new MsnSpectrum();
        spectrum.add(114.09134008669999, 10.0);
        spectrum.add(157.10972980469998, 11.0);
        spectrum.add(158.1162121657, 12.0);
        spectrum.add(200.1162121657, 10.0);
        spectrum.add(270.1937937837, 12.0);
        spectrum.add(271.2002761447, 10.0);
        spectrum.add(300.1162121657, 13.0);
        spectrum.add(350.1162121657, 10.0);
        spectrum.add(400.1162121657, 11.0);
        spectrum.add(427.3027298417, 10.0);
        spectrum.setPrecursor(new Peak(426.30272,10.0,1));
        experimentalSpectra1.add(spectrum);


        MsnSpectrum spectrum2 = new MsnSpectrum();
        spectrum2.add(114.19134008669999, 11.0);
        spectrum2.add(157.20972980469998, 10.0);
        spectrum2.add(158.2162121657, 10.0);
        spectrum2.add(270.1937937837, 10.0);
        spectrum2.add(271.2002761447, 10.0);
        spectrum2.add(300.1162121657, 13.0);
        spectrum2.add(350.1162121657, 14.0);
        spectrum2.add(400.0162121657, 10.0);
        spectrum2.add(427.3027298417, 10.0);
        spectrum2.setPrecursor(new Peak(427.30272,10.0,1));
        experimentalSpectra1.add(spectrum2);

        MsnSpectrum spectrum3 = new MsnSpectrum();
        spectrum3.add(114.09134008669999, 10.0);
        spectrum3.add(157.10972980469998, 11.0);
        spectrum3.add(158.1162121657, 12.0);
        spectrum3.add(200.1162121657, 10.0);
        spectrum3.add(270.1937937837, 12.0);
        spectrum3.add(271.2002761447, 10.0);
        spectrum3.add(300.1162121657, 13.0);
        spectrum3.add(350.1162121657, 10.0);
        spectrum3.add(400.1162121657, 11.0);
        spectrum3.add(427.3027298417, 10.0);
        spectrum3.setPrecursor(new Peak(426.30272,10.0,1));
        experimentalSpectra2.add(spectrum3);


        MsnSpectrum spectrum4 = new MsnSpectrum();
        spectrum4.add(114.19134008669999, 11.0);
        spectrum4.add(157.20972980469998, 10.0);
        spectrum4.add(158.2162121657, 10.0);
        spectrum4.add(270.1937937837, 10.0);
        spectrum4.add(271.2002761447, 10.0);
        spectrum4.add(300.1162121657, 13.0);
        spectrum4.add(350.1162121657, 14.0);
        spectrum4.add(400.0162121657, 10.0);
        spectrum4.add(427.3027298417, 10.0);
        spectrum4.setPrecursor(new Peak(427.30272,10.0,1));
        experimentalSpectra2.add(spectrum4);



        NRPConsensusSpectrum nrpConsensusSpectrum1 = NRPConsensusSpectrum.builder(PeakList.Precision.DOUBLE,new URI("")).
                setConsensusParameters(0.2,0.2, AbstractMergePeakFilter.IntensityMode.MEAN_ALL_INTENSITY).
                setAnnotationParameters(new AbsoluteTolerance(0.2)).
                setFilterParameters(0.2,2).
                buildConsensus(1,nrp,peaks.get(0),experimentalSpectra1);

        NRPConsensusSpectrum nrpConsensusSpectrum2 = NRPConsensusSpectrum.builder(PeakList.Precision.DOUBLE,new URI("")).
                setConsensusParameters(0.2,0.2, AbstractMergePeakFilter.IntensityMode.MEAN_ALL_INTENSITY).
                setAnnotationParameters(new AbsoluteTolerance(0.2)).
                setFilterParameters(0.2,2).
                buildConsensus(1,nrp,peaks.get(0),experimentalSpectra2);

        Assert.assertTrue(nrpConsensusSpectrum1.equals(nrpConsensusSpectrum2));

        nrpConsensusSpectrum2 = NRPConsensusSpectrum.builder(PeakList.Precision.DOUBLE,new URI("")).
                setConsensusParameters(0.2,0.2, AbstractMergePeakFilter.IntensityMode.MEAN_ALL_INTENSITY).
                setAnnotationParameters(new AbsoluteTolerance(0.2)).
                setFilterParameters(0.0,1).
                buildConsensus(1,nrp,peaks.get(0),experimentalSpectra2);

        Assert.assertFalse(nrpConsensusSpectrum1.equals(nrpConsensusSpectrum2));
    }

    @Test
    public void testHashCode() throws Exception {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00487"));
        NRPFragmenter NRPFragmenter = new NRPFragmenter();
        NRPFragmentTree simpleDirectedGraph= NRPFragmenter.createFragmentationTree(nrp);
        NRPPeaksGenerator.Builder peaksGeneratorBuilder = new NRPPeaksGenerator.Builder(simpleDirectedGraph);
        peaksGeneratorBuilder.addIon(AtomicSymbol.H,1,nrp.getAllMonomerNode());
        NRPPeaksGenerator peaksGenerator= peaksGeneratorBuilder.build();
        List<NRPSpectrum> peaks =peaksGenerator.generatePeaks();

        Set<MsnSpectrum> experimentalSpectra1 = new HashSet<>();
        Set<MsnSpectrum> experimentalSpectra2 = new HashSet<>();

        MsnSpectrum spectrum = new MsnSpectrum();
        spectrum.add(114.09134008669999, 10.0);
        spectrum.add(157.10972980469998, 11.0);
        spectrum.add(158.1162121657, 12.0);
        spectrum.add(200.1162121657, 10.0);
        spectrum.add(270.1937937837, 12.0);
        spectrum.add(271.2002761447, 10.0);
        spectrum.add(300.1162121657, 13.0);
        spectrum.add(350.1162121657, 10.0);
        spectrum.add(400.1162121657, 11.0);
        spectrum.add(427.3027298417, 10.0);
        spectrum.setPrecursor(new Peak(426.30272,10.0,1));
        experimentalSpectra1.add(spectrum);


        MsnSpectrum spectrum2 = new MsnSpectrum();
        spectrum2.add(114.19134008669999, 11.0);
        spectrum2.add(157.20972980469998, 10.0);
        spectrum2.add(158.2162121657, 10.0);
        spectrum2.add(270.1937937837, 10.0);
        spectrum2.add(271.2002761447, 10.0);
        spectrum2.add(300.1162121657, 13.0);
        spectrum2.add(350.1162121657, 14.0);
        spectrum2.add(400.0162121657, 10.0);
        spectrum2.add(427.3027298417, 10.0);
        spectrum2.setPrecursor(new Peak(427.30272,10.0,1));
        experimentalSpectra1.add(spectrum2);

        MsnSpectrum spectrum3 = new MsnSpectrum();
        spectrum3.add(114.09134008669999, 10.0);
        spectrum3.add(157.10972980469998, 11.0);
        spectrum3.add(158.1162121657, 12.0);
        spectrum3.add(200.1162121657, 10.0);
        spectrum3.add(270.1937937837, 12.0);
        spectrum3.add(271.2002761447, 10.0);
        spectrum3.add(300.1162121657, 13.0);
        spectrum3.add(350.1162121657, 10.0);
        spectrum3.add(400.1162121657, 11.0);
        spectrum3.add(427.3027298417, 10.0);
        spectrum3.setPrecursor(new Peak(426.30272,10.0,1));
        experimentalSpectra2.add(spectrum3);


        MsnSpectrum spectrum4 = new MsnSpectrum();
        spectrum4.add(114.19134008669999, 11.0);
        spectrum4.add(157.20972980469998, 10.0);
        spectrum4.add(158.2162121657, 10.0);
        spectrum4.add(270.1937937837, 10.0);
        spectrum4.add(271.2002761447, 10.0);
        spectrum4.add(300.1162121657, 13.0);
        spectrum4.add(350.1162121657, 14.0);
        spectrum4.add(400.0162121657, 10.0);
        spectrum4.add(427.3027298417, 10.0);
        spectrum4.setPrecursor(new Peak(427.30272,10.0,1));
        experimentalSpectra2.add(spectrum4);

        NRPConsensusSpectrum nrpConsensusSpectrum1 = NRPConsensusSpectrum.builder(PeakList.Precision.DOUBLE,new URI("")).
                setConsensusParameters(0.2,0.2, AbstractMergePeakFilter.IntensityMode.MEAN_ALL_INTENSITY).
                setAnnotationParameters(new AbsoluteTolerance(0.2)).
                setFilterParameters(0.2,2).
                buildConsensus(1,nrp,peaks.get(0),experimentalSpectra1);

        NRPConsensusSpectrum nrpConsensusSpectrum2 = NRPConsensusSpectrum.builder(PeakList.Precision.DOUBLE,new URI("")).
                setConsensusParameters(0.2,0.2, AbstractMergePeakFilter.IntensityMode.MEAN_ALL_INTENSITY).
                setAnnotationParameters(new AbsoluteTolerance(0.2)).
                setFilterParameters(0.2,2).
                buildConsensus(1,nrp,peaks.get(0),experimentalSpectra2);

        Assert.assertEquals(nrpConsensusSpectrum1.hashCode(), nrpConsensusSpectrum2.hashCode());

        nrpConsensusSpectrum2 = NRPConsensusSpectrum.builder(PeakList.Precision.DOUBLE,new URI("")).
                setConsensusParameters(0.2,0.2, AbstractMergePeakFilter.IntensityMode.MEAN_ALL_INTENSITY).
                setAnnotationParameters(new AbsoluteTolerance(0.2)).
                setFilterParameters(0.0,1).
                buildConsensus(1,nrp,peaks.get(0),experimentalSpectra2);

        Assert.assertThat(nrpConsensusSpectrum1.hashCode(), IsNot.not(IsEqual.equalTo(nrpConsensusSpectrum2.hashCode())));

    }



    @Test
    public void testBuilder() throws Exception {

        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00487"));
        NRPFragmenter NRPFragmenter = new NRPFragmenter();
        NRPFragmentTree simpleDirectedGraph= NRPFragmenter.createFragmentationTree(nrp);
        NRPPeaksGenerator.Builder peaksGeneratorBuilder = new NRPPeaksGenerator.Builder(simpleDirectedGraph);
        peaksGeneratorBuilder.addIon(AtomicSymbol.H,1,nrp.getAllMonomerNode());
        NRPPeaksGenerator peaksGenerator= peaksGeneratorBuilder.build();
        List<NRPSpectrum> peaks =peaksGenerator.generatePeaks();

        Set<MsnSpectrum> experimentalSpectra = new HashSet<>();

        MsnSpectrum spectrum = new MsnSpectrum();
        spectrum.add(114.09134008669999, 10.0);//leu
        spectrum.add(157.10972980469998, 11.0);//nacleu
        spectrum.add(158.1162121657, 12.0);//argal
        spectrum.add(200.1162121657, 10.0);//unknown
        spectrum.add(270.1937937837, 12.0);//nacleuleu
        spectrum.add(300.1162121657, 13.0);//unknown
        spectrum.add(350.1162121657, 10.0);//unknown
        spectrum.add(400.1162121657, 11.0);//unknown
        spectrum.add(427.3027298417, 10.0);//nacleuleuargal
        spectrum.setPrecursor(new Peak(426.30272,10.0,1));
        experimentalSpectra.add(spectrum);

        MsnSpectrum spectrum2 = new MsnSpectrum();

        spectrum2.add(157.20972980469998, 10.0);//nacleu
        spectrum2.add(158.2162121657, 10.0);//argal
        spectrum2.add(270.1937937837, 10.0);//nacleuleu
        spectrum2.add(271.7002761447, 10.0);//argalleu
        spectrum2.add(300.1162121657, 13.0);//unknown
        spectrum2.add(350.1162121657, 14.0);//unknown
        spectrum2.add(400.0162121657, 10.0);//unknown
        spectrum2.add(427.3027298417, 10.0);//nacleuleuargal
        spectrum2.setPrecursor(new Peak(427.30272,10.0,1));
        experimentalSpectra.add(spectrum2);

        MsnSpectrum spectrum3 = new MsnSpectrum();
        spectrum3.add(157.00972980469998, 10.0);//nacleu
        spectrum3.add(158.1162121657, 10.0);//argal
        spectrum3.add(270.1937937837, 10.0);//nacleuleu
        spectrum3.add(300.1162121657, 10.0);//unknown
        spectrum3.add(350.0162121657, 11.0);//unknown
        spectrum3.add(400.1162121657, 12.0);//unknown
        spectrum3.add(427.3027298417, 12.0);//nacleuleuargal
        spectrum3.setPrecursor(new Peak(428.30272,10.0,1));
        experimentalSpectra.add(spectrum3);

        NRPConsensusSpectrum nrpConsensusSpectrum = NRPConsensusSpectrum.builder(PeakList.Precision.DOUBLE,new URI("")).
                setConsensusParameters(0.2,0.2, AbstractMergePeakFilter.IntensityMode.MEAN_ALL_INTENSITY).
                setAnnotationParameters(new AbsoluteTolerance(0.2)).
                setFilterParameters(0.2,2).
                buildConsensus(1,nrp,peaks.get(0),experimentalSpectra);

        Assert.assertEquals(nrpConsensusSpectrum.size(),8);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotationIndexes().length,8);

        Assert.assertTrue(nrpConsensusSpectrum.getMemberIds().contains(spectrum.getId()));
        Assert.assertTrue(nrpConsensusSpectrum.getMemberIds().contains(spectrum2.getId()));
        Assert.assertTrue(nrpConsensusSpectrum.getMemberIds().contains(spectrum3.getId()));

        Assert.assertEquals(nrpConsensusSpectrum.getPrecursor().getMz(),427.30272,0.0001);
        Assert.assertEquals(nrpConsensusSpectrum.getPrecursor().getMz(),nrpConsensusSpectrum.getPrecursorMzMean(),0.0001);
        Assert.assertEquals(nrpConsensusSpectrum.getPrecursor().getIntensity(),10,0.0001);
        double mean = nrpConsensusSpectrum.getPrecursorMzMean();
        double std = (426.30272-mean)*(426.30272-mean)+(427.30272-mean)*(427.30272-mean)+(428.30272-mean)*(428.30272-mean);
        std = Math.sqrt(std/2);
        Assert.assertEquals(nrpConsensusSpectrum.getPrecursorMzStdev(),std,0.0001);


        Assert.assertEquals(nrpConsensusSpectrum.getIntensity(0),3.3333333,0.0001);
        Assert.assertEquals(nrpConsensusSpectrum.getMz(0),114.09134008669999,0.0001);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(0).size(),1);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(0).get(0).getMergedPeakCount(),1);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(0).get(0).getMzStd(),0,0.00001);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(0).get(0).getIntensityStd(),0,0.00001);

        Assert.assertEquals(nrpConsensusSpectrum.getIntensity(1),10.333333333333334,0.0001);
        Assert.assertEquals(nrpConsensusSpectrum.getMz(1),157.10972980469998,0.0001);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(1).size(),1);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(1).get(0).getMergedPeakCount(),3);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(1).get(0).getMzStd(),0.08164,0.00001);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(1).get(0).getIntensityStd(),0.4714,0.00001);

        Assert.assertEquals(nrpConsensusSpectrum.getIntensity(2),10.666666666666666,0.0001);
        Assert.assertEquals(nrpConsensusSpectrum.getMz(2),158.1474621657,0.0001);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(2).size(),1);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(2).get(0).getMergedPeakCount(),3);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(2).get(0).getMzStd(),0.04718,0.00001);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(2).get(0).getIntensityStd(),0.94280,0.00001);

        Assert.assertEquals(nrpConsensusSpectrum.getIntensity(3),10.666666666666666,0.0001);
        Assert.assertEquals(nrpConsensusSpectrum.getMz(3),270.1937937837,0.0001);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(3).size(),1);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(3).get(0).getMergedPeakCount(),3);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(3).get(0).getMzStd(),0,0.00001);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(3).get(0).getIntensityStd(),0.9428,0.00001);

        Assert.assertEquals(nrpConsensusSpectrum.getIntensity(4),12.0,0.0001);
        Assert.assertEquals(nrpConsensusSpectrum.getMz(4),300.1162121657,0.0001);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(4).size(),1);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(4).get(0).getMergedPeakCount(),3);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(4).get(0).getMzStd(),0,0.00001);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(4).get(0).getIntensityStd(),1.4142135,0.00001);

        Assert.assertEquals(nrpConsensusSpectrum.getIntensity(5),11.666666666666666,0.0001);
        Assert.assertEquals(nrpConsensusSpectrum.getMz(5),350.08478359427147,0.0001);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(5).size(),1);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(5).get(0).getMergedPeakCount(),3);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(5).get(0).getMzStd(),0.04717,0.00001);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(5).get(0).getIntensityStd(),1.6996731,0.00001);

        Assert.assertEquals(nrpConsensusSpectrum.getIntensity(6),11.0,0.0001);
        Assert.assertEquals(nrpConsensusSpectrum.getMz(6),400.08590913539695,0.0001);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(6).size(),1);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(6).get(0).getMergedPeakCount(),3);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(6).get(0).getMzStd(),0.04723,0.00001);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(6).get(0).getIntensityStd(),0.81649,0.00001);

        Assert.assertEquals(nrpConsensusSpectrum.getIntensity(7),10.666666666666666,0.0001);
        Assert.assertEquals(nrpConsensusSpectrum.getMz(7),427.30272984169994,0.0001);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(7).size(),1);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(7).get(0).getMergedPeakCount(),3);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(7).get(0).getMzStd(),5.684E-14,0.00001);
        Assert.assertEquals(nrpConsensusSpectrum.getAnnotations(7).get(0).getIntensityStd(),0.942809,0.00001);

    }
}