package ms.spectrum;

import junit.framework.Assert;
import molecules.NRP;
import molecules.NRPBuilder;
import molecules.norine.NorineNRPStore;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.hamcrest.core.IsEqual;
import org.hamcrest.core.IsNot;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ericart on 16.06.2016.
 */
public class NRPSpectrumTest {


    @Test
    public void testEquals() throws Exception {


        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00487"));

        NRPSpectrum spectrum1 = new NRPSpectrum(nrp, 2, PeakList.Precision.DOUBLE);
        spectrum1.add(100.00,10.00);
        spectrum1.add(200.00,11.00);
        spectrum1.add(350.00,13.00);

        NRPSpectrum spectrum2 = new NRPSpectrum(nrp, 2, PeakList.Precision.DOUBLE);
        spectrum2.add(100.00,10.00);
        spectrum2.add(200.00,11.00);
        spectrum2.add(350.00,13.00);

        NRPSpectrum spectrum3 = new NRPSpectrum(nrp, 1, PeakList.Precision.DOUBLE);
        spectrum3.add(100.00,10.00);
        spectrum3.add(200.00,11.00);
        spectrum3.add(350.00,13.00);


        Assert.assertTrue(spectrum1.equals(spectrum2));

        spectrum2.add(202.00,32.00);

        Assert.assertFalse(spectrum1.equals(spectrum2));
        Assert.assertFalse(spectrum1.equals(spectrum3));

    }

    @Test
    public void testHashCode() throws Exception {

        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00487"));

        NRPSpectrum spectrum1 = new NRPSpectrum(nrp, 2, PeakList.Precision.DOUBLE);
        spectrum1.add(100.00,10.00);
        spectrum1.add(200.00,11.00);
        spectrum1.add(350.00,13.00);

        NRPSpectrum spectrum2 = new NRPSpectrum(nrp, 2, PeakList.Precision.DOUBLE);
        spectrum2.add(100.00,10.00);
        spectrum2.add(200.00,11.00);
        spectrum2.add(350.00,13.00);

        NRPSpectrum spectrum3 = new NRPSpectrum(nrp,1, PeakList.Precision.DOUBLE);
        spectrum3.add(100.00,10.00);
        spectrum3.add(200.00,11.00);
        spectrum3.add(350.00,13.00);

        Assert.assertEquals(spectrum1.hashCode(), spectrum2.hashCode());

        assertThat(spectrum1.hashCode(), IsNot.not(IsEqual.equalTo(spectrum3.hashCode())));

    }

}