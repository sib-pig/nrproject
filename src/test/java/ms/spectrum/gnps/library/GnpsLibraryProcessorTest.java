package ms.spectrum.gnps.library;

import ms.spectrum.gnps.library.GnpsLibraryProcessor;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ericart on 31/10/2016.
 */

public class GnpsLibraryProcessorTest {

    @Test
    public void testGetInstance1() throws Exception {
        GnpsLibraryProcessor mainGnpsAnalyzer = GnpsLibraryProcessor.getInstance();
        assertEquals(GnpsLibraryProcessor.class,mainGnpsAnalyzer.getClass());
    }

}