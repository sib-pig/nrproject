package ms.spectrum;

import molecules.NRP;
import molecules.NRPBuilder;
import molecules.norine.NorineNRPStore;
import org.expasy.mzjava.core.ms.consensus.ConsensusPeakSink;
import org.expasy.mzjava.core.ms.consensus.ConsensusSpectrum;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.LibPeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.HashSet;
import java.util.UUID;

import static org.junit.Assert.*;

/**
 * Created by ericart on 15.06.2016.
 */
public class NRPConsensusPeakSinkTest {


    @Test
    public void testProcessPeak() throws Exception {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();

        //we create a monomeric graph of the NRP we are interested in:
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00487"));
        NRPConsensusSpectrum consensus = new NRPConsensusSpectrum(nrp, PeakList.Precision.DOUBLE,new HashSet<UUID>());

        NRPConsensusPeakSink filter = new NRPConsensusPeakSink(consensus,0.2,10);

        for (int i=0;i<100;i++) {
            filter.processPeak(100.0+i*10.0,10.0, Collections.singletonList(new NRPLibPeakAnnotation(i, 1.0, 1.0)) );
        }

        Assert.assertEquals(consensus.size(),90);
    }


}