package ms.spectrum;

import com.google.common.collect.*;
import molecules.*;
import molecules.modification.Modification;
import molecules.norine.NorineNRPStore;
import org.expasy.mzjava.core.mol.AtomicSymbol;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.ms.spectrum.AnnotatedPeak;
import org.junit.Before;

import org.junit.Test;

import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Created by ericart on 02.05.2016.
 */

public class NRPFragmentAnnotationTest {

    private AnnotatedPeak<NRPFragmentAnnotation> peak;
    private NRPFragment nrpFragment;
    private Monomer modifiedMonomer1;
    private Monomer modifiedMonomer2;

    @Before
    public void setUp() throws Exception {

        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00487"));
        nrpFragment = new NRPFragment(nrp.cloneSimpleGraph(), nrp.getComposition(), nrp.getNrpId(),0);

        modifiedMonomer1=(Monomer)nrpFragment.getAllMonomerNode().toArray()[0];
        modifiedMonomer2=(Monomer)nrpFragment.getAllMonomerNode().toArray()[1];
        Composition proton = new Composition.Builder().add(AtomicSymbol.H).charge(1).build();
        Composition phosphorylation = Composition.parseComposition("HPO3");

        ListMultimap<Monomer, Modification> monomersModifications = ArrayListMultimap.create();
        monomersModifications.put(modifiedMonomer1,new Modification(phosphorylation, Modification.ModificationType.FIX));
        monomersModifications.put(modifiedMonomer2,new Modification(phosphorylation, Modification.ModificationType.VARIABLE));

        this.peak = new AnnotatedPeak<>(0.01, 0.01, 1, new NRPFragmentAnnotation(1, nrpFragment, monomersModifications, proton, phosphorylation));


    }
    @Test
    public void testGetCharge() throws Exception {

        assertEquals(1,this.peak.getCharge());
    }

    @Test
    public void testGetChargedGroup() throws Exception {
        Composition proton = new Composition.Builder().add(AtomicSymbol.H).charge(1).build();
        assertEquals(proton,this.peak.getAnnotation(0).getChargedGroup());
    }

    @Test
    public void testGetFragment() throws Exception {

        assertEquals(nrpFragment,this.peak.getAnnotation(0).getFragment());

    }

    @Test
    public void testGetModifications() throws Exception {
        Composition phosphorylationx2 = Composition.parseComposition("H2P2O6");
        assertEquals(phosphorylationx2,this.peak.getAnnotation(0).getModifications());

    }

    @Test
    public void testGetAllModifiedMonomers() throws Exception {
        Set<Monomer> modifiedMonomers= Sets.newHashSet(modifiedMonomer1,modifiedMonomer2);
        assertEquals(modifiedMonomers,this.peak.getAnnotation(0).getAllModifiedMonomers());
    }

    @Test
    public void testGetAllModifications() throws Exception {
        Composition phosphorylation = Composition.parseComposition("HPO3");
        Set setModifications = Sets.newHashSet(new Modification (phosphorylation, Modification.ModificationType.VARIABLE),new Modification (phosphorylation, Modification.ModificationType.FIX));
        Set setModifications2 = Sets.newHashSet(this.peak.getAnnotation(0).getAllModifications());
        assertEquals(setModifications,setModifications2);
    }

    @Test
    public void testGetModifications1() throws Exception {
        Composition phosphorylation = Composition.parseComposition("HPO3");
        List<Modification> modifications = Lists.newArrayList(new Modification (phosphorylation, Modification.ModificationType.FIX));
        assertEquals(modifications,this.peak.getAnnotation(0).getModifications(modifiedMonomer1));
    }

    @Test
    public void testCopy() throws Exception {
        AnnotatedPeak<NRPFragmentAnnotation> peak2 = this.peak.copy();
        assertEquals(peak2.getAnnotation(0).getFragment(),this.peak.getAnnotation(0).getFragment());
        assertEquals(peak2.getAnnotation(0).getChargedGroup(),this.peak.getAnnotation(0).getChargedGroup());
        assertEquals(peak2.getAnnotation(0).getFragment(),this.peak.getAnnotation(0).getFragment());
        assertEquals(peak2.getAnnotation(0).getModifications(),this.peak.getAnnotation(0).getModifications());
        assertEquals(peak2.getAnnotation(0).getAllModifiedMonomers(),this.peak.getAnnotation(0).getAllModifiedMonomers());
        assertEquals(peak2.getAnnotation(0).getAllModifications(),this.peak.getAnnotation(0).getAllModifications());
        assertEquals(peak2.getAnnotation(0).getModifications(modifiedMonomer1),this.peak.getAnnotation(0).getModifications(modifiedMonomer1));
    }
}