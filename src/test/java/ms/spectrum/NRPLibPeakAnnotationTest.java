package ms.spectrum;

import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ericart on 21.06.2016.
 */
public class NRPLibPeakAnnotationTest {

    @Test
    public void testCopy() throws Exception {
        NRPLibPeakAnnotation annotation = new NRPLibPeakAnnotation(3, 1.0, 100.0);

        NRPLibPeakAnnotation copyAnnotation = annotation.copy();
        NRPLibPeakAnnotation copyConstructedAnnotation = new NRPLibPeakAnnotation(annotation);

        assertNotSame(copyAnnotation, copyConstructedAnnotation);
        assertTrue(copyAnnotation.equals(copyConstructedAnnotation));
        assertTrue(copyConstructedAnnotation.equals(copyAnnotation));
    }

    @Test
    public void testEquals() throws Exception {
        NRPLibPeakAnnotation annotation1 = new NRPLibPeakAnnotation(3, 1.0, 100.0);
        NRPLibPeakAnnotation annotation2 = new NRPLibPeakAnnotation(3, 1.0, 100.0);
        NRPLibPeakAnnotation annotation3 = new NRPLibPeakAnnotation(1, 1.0, 100.0);

        assertNotSame(annotation1, annotation2);

        assertTrue(annotation1.equals(annotation2));
        assertTrue(annotation2.equals(annotation1));

        assertFalse(annotation1.equals(annotation3));
        assertFalse(annotation3.equals(annotation1));
    }

    @Test
    public void testHashCode() throws Exception {
        NRPLibPeakAnnotation annotation1 = new NRPLibPeakAnnotation(3, 1.0, 100.0);
        NRPLibPeakAnnotation annotation2 = new NRPLibPeakAnnotation(3, 1.0, 100.0);
        NRPLibPeakAnnotation annotation3 = new NRPLibPeakAnnotation(1, 1.0, 100.0);

        int hash1 = annotation1.hashCode();
        int hash2 = annotation2.hashCode();
        int hash3 = annotation3.hashCode();

        assertEquals(hash1, hash2);
        assertTrue(hash1 != hash3);
    }
}