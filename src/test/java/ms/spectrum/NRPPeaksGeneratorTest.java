package ms.spectrum;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Sets;
import molecules.*;
import molecules.norine.NorineNRPStore;
import ms.fragment.NRPFragmentTree;
import ms.fragment.Loss;
import ms.fragment.NRPFragmenter;
import org.expasy.mzjava.core.mol.AtomicSymbol;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.ms.spectrum.AnnotatedPeak;

import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;


/**
 * Created by ericart on 24.03.2016.
 */
public class NRPPeaksGeneratorTest {

    //we test with no modifications
    @Test
    public void testGenerateAllPeaks1() throws Exception {

        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();

        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00487"));


        NRPFragmenter NRPFragmenter = new NRPFragmenter();
        NRPFragmentTree simpleDirectedGraph= NRPFragmenter.createFragmentationTree(nrp);

        NRPPeaksGenerator.Builder peaksGeneratorBuilder = new NRPPeaksGenerator.Builder(simpleDirectedGraph);
        peaksGeneratorBuilder.addIon(AtomicSymbol.H,1,nrp.getAllMonomerNode());
        NRPPeaksGenerator peaksGenerator= peaksGeneratorBuilder.build();

        Composition proton =new Composition.Builder().add(AtomicSymbol.H).charge(1).build();
        Composition EMPTY_COMPOSITION = new Composition(new Composition[0]);
        List<NRPSpectrum> peaks =peaksGenerator.generatePeaks();

        for (NRPSpectrum p1 : peaks){

            for (NRPFragmentAnnotation p : p1.getAnnotations(0)){

                assertEquals(0,p.getAllModifiedMonomers().size());
                assertEquals(0,p.getAllModifications().size());
                assertEquals(EMPTY_COMPOSITION,p.getModifications());
                assertEquals(proton,p.getChargedGroup());
                assertEquals(1,p.getCharge());


            }

        }
        assertEquals(1,peaks.size());
        assertEquals(6,peaks.get(0).getAnnotationIndexes().length);

    }

    //we test fix modification
    @Test
    public void testGenerateAllPeaks2() throws Exception {

        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();

        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00487"));


        NRPFragmenter NRPFragmenter = new NRPFragmenter();
        NRPFragmentTree simpleDirectedGraph= NRPFragmenter.createFragmentationTree(nrp);

        NRPPeaksGenerator.Builder peaksGeneratorBuilder = new NRPPeaksGenerator.Builder(simpleDirectedGraph);
        peaksGeneratorBuilder.addFixModification(Composition.parseComposition("H"),nrp.getAllMonomerNode());
        peaksGeneratorBuilder.addIon(AtomicSymbol.H,1,nrp.getAllMonomerNode());
        NRPPeaksGenerator peaksGenerator= peaksGeneratorBuilder.build();

        Composition proton =new Composition.Builder().add(AtomicSymbol.H).charge(1).build();

        List<NRPSpectrum> peaks =peaksGenerator.generatePeaks();

        for (int i : peaks.get(0).getAnnotationIndexes()) {
            for (NRPFragmentAnnotation p : peaks.get(0).getAnnotations(i)) {
                assertEquals(p.getFragment().getAllMonomerNode(), p.getAllModifiedMonomers());
                assertEquals(new Composition.Builder().add(AtomicSymbol.H, p.getAllModifiedMonomers().size()).build(), p.getModifications());
                assertEquals(proton, p.getChargedGroup());
                assertEquals(1, p.getCharge());
            }
        }

        assertEquals(1,peaks.size());
        assertEquals(6,peaks.get(0).getMzs(new double[peaks.get(0).getAnnotationIndexes().length]).length);
    }


    //we test variable modifications and ion adducts
    @Test
    public void testGenerateAllPeaks3() throws Exception {

        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00487"));
        NRPFragmenter NRPFragmenter = new NRPFragmenter();
        NRPFragmentTree simpleDirectedGraph= NRPFragmenter.createFragmentationTree(nrp);

        Monomer leu=(Monomer)nrp.getAllMonomerNode().toArray()[0];
        Monomer nacLeu=(Monomer)nrp.getAllMonomerNode().toArray()[1];
        //Monomer argal=(Monomer)nrp.getAllMonomerNode().toArray()[2];

        Set<Monomer> nacleu_leu= Sets.newHashSet(nacLeu,leu);
        Set<Monomer> leuSet= Sets.newHashSet(leu);

        //Set<Monomer> nacleu_leu_argal= Sets.newHashSet(nacLeu,leu,argal);
        //Set<Monomer> leu_argal= Sets.newHashSet(leu,argal);
        //Set<Monomer> nacleuSet= Sets.newHashSet(nacLeu);
        //Set<Monomer> argalSet=Sets.newHashSet(argal);

        NRPPeaksGenerator.Builder peaksGeneratorBuilder = new NRPPeaksGenerator.Builder(simpleDirectedGraph);
        peaksGeneratorBuilder.addIon(AtomicSymbol.H,1,nrp.getAllMonomerNode());
        peaksGeneratorBuilder.addVariableModification(Composition.parseComposition("HPO3"),leuSet);
        peaksGeneratorBuilder.addVariableModification(Composition.parseComposition("C3"),nacleu_leu);
        peaksGeneratorBuilder.addIon(AtomicSymbol.Cl,1,nacleu_leu);

        NRPPeaksGenerator peaksGenerator= peaksGeneratorBuilder.build();
        List<NRPSpectrum> peaks =peaksGenerator.generatePeaks();

        ObjectMapper mapper = new ObjectMapper();
        List<List<String>> stringPeaksList = new ArrayList<>();
        for (NRPSpectrum p1 : peaks){
            List<String> stringPeaks = new ArrayList<>();
            for (int i : p1.getAnnotationIndexes()) {
                for (NRPFragmentAnnotation p : p1.getAnnotations(i)) {
                    List<String> fragmentMonomers = monomersSetTostrSortList(p.getFragment().getAllMonomerNode());
                    List<String> modifiedMonomers = monomersSetTostrSortList(p.getAllModifiedMonomers());
                    String nrpString = mapper.writeValueAsString(fragmentMonomers + " " + modifiedMonomers + " " + p.getModifications() + " " + p.getChargedGroup());
                    stringPeaks.add(nrpString);
                    //System.out.println(nrpString);
                }
            }
            stringPeaksList.add(stringPeaks);
        }

        int first = 0;
        int second = 1;

        if(!stringPeaksList.get(0).contains("\"[Argal, Leu, NAc-Leu] [Leu] C3HO3P ClH(2+)\"")){
            first = 1;
            second = 0;
        }

        assertTrue(stringPeaksList.get(first).contains("\"[Argal, Leu, NAc-Leu] [Leu] C3HO3P ClH(2+)\""));
        assertTrue(stringPeaksList.get(first).contains("\"[Argal] []  H(+)\""));
        assertTrue(stringPeaksList.get(first).contains("\"[Leu, NAc-Leu] [Leu] C3HO3P Cl(+)\""));
        assertTrue(stringPeaksList.get(first).contains("\"[Leu, NAc-Leu] [Leu] C3HO3P ClH(2+)\""));
        assertTrue(stringPeaksList.get(first).contains("\"[NAc-Leu] []  Cl(+)\""));
        assertTrue(stringPeaksList.get(first).contains("\"[NAc-Leu] []  H(+)\""));
        assertTrue(stringPeaksList.get(first).contains("\"[Argal, Leu] [Leu] C3HO3P Cl(+)\""));
        assertTrue(stringPeaksList.get(first).contains("\"[Argal, Leu] [Leu] C3HO3P ClH(2+)\""));
        assertTrue(stringPeaksList.get(first).contains("\"[Argal, Leu] [Leu] C3HO3P H(+)\""));
        assertTrue(stringPeaksList.get(first).contains("\"[Leu] [Leu] C3HO3P Cl(+)\""));
        assertTrue(stringPeaksList.get(first).contains("\"[Leu] [Leu] C3HO3P H(+)\""));

        assertTrue(stringPeaksList.get(second).contains("\"[Argal, Leu, NAc-Leu] [Leu, NAc-Leu] C3HO3P ClH(2+)\""));
        assertTrue(stringPeaksList.get(second).contains("\"[Argal] []  H(+)\""));
        assertTrue(stringPeaksList.get(second).contains("\"[Leu, NAc-Leu] [Leu, NAc-Leu] C3HO3P Cl(+)\""));
        assertTrue(stringPeaksList.get(second).contains("\"[Leu, NAc-Leu] [Leu, NAc-Leu] C3HO3P ClH(2+)\""));
        assertTrue(stringPeaksList.get(second).contains("\"[NAc-Leu] [NAc-Leu] C3 Cl(+)\""));
        assertTrue(stringPeaksList.get(second).contains("\"[NAc-Leu] [NAc-Leu] C3 H(+)\""));
        assertTrue(stringPeaksList.get(second).contains("\"[Argal, Leu] [Leu] HPO3 Cl(+)\""));
        assertTrue(stringPeaksList.get(second).contains("\"[Argal, Leu] [Leu] HPO3 ClH(2+)\""));
        assertTrue(stringPeaksList.get(second).contains("\"[Argal, Leu] [Leu] HPO3 H(+)\""));
        assertTrue(stringPeaksList.get(second).contains("\"[Leu] [Leu] HPO3 Cl(+)\""));
        assertTrue(stringPeaksList.get(second).contains("\"[Leu] [Leu] HPO3 H(+)\""));

        assertEquals(2,peaks.size());
        assertEquals(11,stringPeaksList.get(first).size());
        assertEquals(11,stringPeaksList.get(second).size());

    }


    //we test neutral Losses
    @Test
    public void testGenerateAllPeaks4() throws Exception {

        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00487"));
        NRPFragmenter NRPFragmenter = new NRPFragmenter();
        NRPFragmentTree simpleDirectedGraph= NRPFragmenter.createFragmentationTree(nrp);

        Monomer leu=(Monomer)nrp.getAllMonomerNode().toArray()[0];
        Monomer nacLeu=(Monomer)nrp.getAllMonomerNode().toArray()[1];
        Monomer argal=(Monomer)nrp.getAllMonomerNode().toArray()[2];

        Set<Monomer> nacleu_leu= Sets.newHashSet(nacLeu,leu);
        Set<Monomer> leu_argal= Sets.newHashSet(leu,argal);


        NRPPeaksGenerator.Builder peaksGeneratorBuilder = new NRPPeaksGenerator.Builder(simpleDirectedGraph);
        peaksGeneratorBuilder.addIon(AtomicSymbol.H,1,nrp.getAllMonomerNode());
        peaksGeneratorBuilder.addNeutralLoss(Composition.parseComposition("H2O"),nacleu_leu);
        peaksGeneratorBuilder.addNeutralLoss(Composition.parseComposition("NO"),leu_argal);
        peaksGeneratorBuilder.addNeutralLoss(Composition.parseComposition("H2"),leu_argal);


        NRPPeaksGenerator peaksGenerator= peaksGeneratorBuilder.build();
        List<NRPSpectrum> peaks =peaksGenerator.generatePeaks();

        assertEquals(1,peaks.size());

        ObjectMapper mapper = new ObjectMapper();

        List<String> stringPeaks = new ArrayList<>();
        for (int i : peaks.get(0).getAnnotationIndexes()){
            for (NRPFragmentAnnotation p : peaks.get(0).getAnnotations(i)) {
                List<String> fragmentMonomers = monomersSetTostrSortList( p.getFragment().getAllMonomerNode());
                List<String> modifiedMonomers = monomersSetTostrSortList( p.getAllModifiedMonomers());
                String nrpString=mapper.writeValueAsString(fragmentMonomers+" "+modifiedMonomers+" "+p.getModifications()+" "+p.getChargedGroup()+" "+p.getNeutralLoss());
                String nrpString2=mapper.writeValueAsString(peaks.get(0).getMz(i));

                /*System.out.println(nrpString2);
                for (Monomer m : p.getFragment().getAllMonomerNode()){
                    System.out.println(m+" "+m.getNorineResidue().getComposition());
                }
                */
                Composition c1 = new Composition.Builder().add(AtomicSymbol.C).build();
                Composition c3 = new Composition.Builder().add(AtomicSymbol.O).build();
                Composition c2 = new Composition.Builder().add(AtomicSymbol.H).build();
                Composition c4 = new Composition.Builder().add(AtomicSymbol.N).build();
                //System.out.println("C "+ c1.getMolecularMass());
                //System.out.println("O "+ c3.getMolecularMass());
                //System.out.println("H "+ c2.getMolecularMass());
                //System.out.println("N "+ c4.getMolecularMass());
                //System.out.println();

                stringPeaks.add(nrpString);
            }

        }
        //System.out.println("PRECURSOR "+ peaks.get(0).getPrecursor().getMz());
        assertTrue(stringPeaks.contains("\"[Argal, Leu, NAc-Leu] []  H(+) \""));
        assertTrue(stringPeaks.contains("\"[Argal, Leu, NAc-Leu] []  H(+) N2O2\""));
        assertTrue(stringPeaks.contains("\"[Argal, Leu, NAc-Leu] []  H(+) H2O\""));
        assertTrue(stringPeaks.contains("\"[Argal, Leu, NAc-Leu] []  H(+) H2NO\""));
        assertTrue(stringPeaks.contains("\"[Argal, Leu, NAc-Leu] []  H(+) H4\""));
        assertTrue(stringPeaks.contains("\"[Argal, Leu, NAc-Leu] []  H(+) H2NO2\""));
        assertTrue(stringPeaks.contains("\"[Argal, Leu, NAc-Leu] []  H(+) NO\""));
        assertTrue(stringPeaks.contains("\"[Argal, Leu, NAc-Leu] []  H(+) H4O2\""));
        assertTrue(stringPeaks.contains("\"[Argal, Leu, NAc-Leu] []  H(+) H4O\""));
        assertTrue(stringPeaks.contains("\"[Argal, Leu, NAc-Leu] []  H(+) H2\""));
        assertTrue(stringPeaks.contains("\"[Argal] []  H(+) \""));
        assertTrue(stringPeaks.contains("\"[Argal] []  H(+) NO\""));
        assertTrue(stringPeaks.contains("\"[Argal] []  H(+) H2\""));
        assertTrue(stringPeaks.contains("\"[Leu, NAc-Leu] []  H(+) \""));
        assertTrue(stringPeaks.contains("\"[Leu, NAc-Leu] []  H(+) H2O\""));
        assertTrue(stringPeaks.contains("\"[Leu, NAc-Leu] []  H(+) H2NO2\""));
        assertTrue(stringPeaks.contains("\"[Leu, NAc-Leu] []  H(+) NO\""));
        assertTrue(stringPeaks.contains("\"[Leu, NAc-Leu] []  H(+) H4O2\""));
        assertTrue(stringPeaks.contains("\"[Leu, NAc-Leu] []  H(+) H4O\""));
        assertTrue(stringPeaks.contains("\"[Leu, NAc-Leu] []  H(+) H2\""));
        assertTrue(stringPeaks.contains("\"[NAc-Leu] []  H(+) \""));
        assertTrue(stringPeaks.contains("\"[NAc-Leu] []  H(+) H2O\""));
        assertTrue(stringPeaks.contains("\"[Argal, Leu] []  H(+) \""));
        assertTrue(stringPeaks.contains("\"[Argal, Leu] []  H(+) N2O2\""));
        assertTrue(stringPeaks.contains("\"[Argal, Leu] []  H(+) H2O\""));
        assertTrue(stringPeaks.contains("\"[Argal, Leu] []  H(+) H2NO\""));
        assertTrue(stringPeaks.contains("\"[Argal, Leu] []  H(+) H4\""));
        assertTrue(stringPeaks.contains("\"[Argal, Leu] []  H(+) H2NO2\""));
        assertTrue(stringPeaks.contains("\"[Argal, Leu] []  H(+) NO\""));
        assertTrue(stringPeaks.contains("\"[Argal, Leu] []  H(+) H4O\""));
        assertTrue(stringPeaks.contains("\"[Argal, Leu] []  H(+) H2\""));
        assertTrue(stringPeaks.contains("\"[Leu] []  H(+) \""));
        assertTrue(stringPeaks.contains("\"[Leu] []  H(+) H2O\""));
        assertTrue(stringPeaks.contains("\"[Leu] []  H(+) NO\""));
        assertTrue(stringPeaks.contains("\"[Leu] []  H(+) H2\""));

        assertEquals(35,stringPeaks.size());

    }

    private List<String> monomersSetTostrSortList(Set<Monomer> monomerSet){
        List<String> sortedList = new ArrayList<>();
        for (Monomer monomer : monomerSet){
            sortedList.add(monomer.getMonomerSymbol());
        }
        Collections.sort(sortedList);
        return sortedList;
    }


    //we test doubly charged
    @Test
    public void testGenerateAllPeaks5() throws Exception {

        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00487"));
        NRPFragmenter NRPFragmenter = new NRPFragmenter();
        NRPFragmentTree simpleDirectedGraph= NRPFragmenter.createFragmentationTree(nrp);
        NRPPeaksGenerator.Builder peaksGeneratorBuilder = new NRPPeaksGenerator.Builder(simpleDirectedGraph);
        peaksGeneratorBuilder.addIon(AtomicSymbol.H,1,nrp.getAllMonomerNode());
        peaksGeneratorBuilder.addIon(AtomicSymbol.H,1,nrp.getAllMonomerNode());

        NRPPeaksGenerator peaksGenerator2= peaksGeneratorBuilder.build();
        List<NRPSpectrum> peaks2 =peaksGenerator2.generatePeaks();

        ObjectMapper mapper = new ObjectMapper();
        List<String> stringPeaks = new ArrayList<>();
        for (int i : peaks2.get(0).getAnnotationIndexes()){

            for (NRPFragmentAnnotation p :peaks2.get(0).getAnnotations(i)){

                String fragment = mapper.writeValueAsString(p.getFragment().getAllMonomerNode()+" "+p.getChargedGroup());
                stringPeaks.add(fragment);
            }

        }
        assertTrue(stringPeaks.contains("\"[Leu] H(+)\""));
        assertTrue(stringPeaks.contains("\"[Leu, NAc-Leu] H2(2+)\""));
        assertTrue(stringPeaks.contains("\"[Leu, Argal] H2(2+)\""));
        assertTrue(stringPeaks.contains("\"[NAc-Leu] H(+)\""));
        assertTrue(stringPeaks.contains("\"[Argal] H(+)\""));
        assertTrue(stringPeaks.contains("\"[Leu, NAc-Leu, Argal] H2(2+)\""));
        assertTrue(stringPeaks.contains("\"[Leu, NAc-Leu] H(+)\""));
        assertTrue(stringPeaks.contains("\"[Leu, Argal] H(+)\""));


    }

    @Test(expected = IllegalStateException.class)
    public void testGenerateAllPeaksException() throws Exception {

        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00487"));

        NRPFragmenter NRPFragmenter = new NRPFragmenter();
        NRPFragmentTree simpleDirectedGraph= NRPFragmenter.createFragmentationTree(nrp);

        NRPPeaksGenerator peaksGenerator = new NRPPeaksGenerator.Builder(simpleDirectedGraph).build();

    }

/*
    private int binomialCoeff(int n, int k)
    {
        int res = 1;

        // Since C(n, k) = C(n, n-k)
        if ( k > n - k )
            k = n - k;

        // Calculate value of [n * (n-1) *---* (n-k+1)] / [k * (k-1) *----* 1]
        for (int i = 0; i < k; ++i)
        {
            res *= (n - i);
            res /= (i + 1);
        }

        return res;
    }
*/
}
