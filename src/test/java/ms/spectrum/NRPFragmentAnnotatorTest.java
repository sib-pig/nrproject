package ms.spectrum;

import molecules.*;
import molecules.norine.NorineNRPStore;
import ms.fragment.Loss;
import ms.fragment.NRPFragmentTree;
import ms.fragment.NRPFragmenter;
import org.expasy.mzjava.core.mol.AtomicSymbol;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakList;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;


/**
 * Created by ericart on 16.06.2016.
 */
public class NRPFragmentAnnotatorTest {
    //general test
    @Test
    public void testprocessPeak1() throws Exception {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();

        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00487"));
        NRPFragmenter NRPFragmenter = new NRPFragmenter();
        NRPFragmentTree simpleDirectedGraph= NRPFragmenter.createFragmentationTree(nrp);
        NRPPeaksGenerator.Builder peaksGeneratorBuilder = new NRPPeaksGenerator.Builder(simpleDirectedGraph);
        peaksGeneratorBuilder.addIon(AtomicSymbol.H,1,nrp.getAllMonomerNode());
        NRPPeaksGenerator peaksGenerator= peaksGeneratorBuilder.build();
        List<NRPSpectrum> peaks =peaksGenerator.generatePeaks();
        NRPSpectrum theoreticalSpectrum = peaks.get(0);

        Set<UUID> memberIds = new HashSet<>();
        memberIds.add(UUID.randomUUID());
        NRPConsensusSpectrum consensus = new NRPConsensusSpectrum(nrp, PeakList.Precision.DOUBLE,memberIds);
        NRPFragmentAnnotator annotator = new NRPFragmentAnnotator(new AbsoluteTolerance(0.5), theoreticalSpectrum);

        NRPConsensusPeakSink sink = new NRPConsensusPeakSink(consensus,0.2,2);
        //sink.setPeakList(consensus);
        annotator.setSink(sink);

        annotator.start(20);

        annotator.processPeak(114.19, 10.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));
        annotator.processPeak(157.20, 10.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));
        annotator.processPeak(158.21, 10.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));
        annotator.processPeak(270.19, 5.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));
        annotator.processPeak(271.20, 10.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));
        annotator.processPeak(300.11, 10.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));
        annotator.processPeak(350.11, 5.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));
        annotator.processPeak(400.01, 10.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));
        annotator.processPeak(427.30, 10.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));

        annotator.end();

        Assert.assertEquals(9, consensus.size());
        checkNRPAnnotation(1,new Composition.Builder(AtomicSymbol.H).charge(1).build(),new Composition(),new Composition(), 0, consensus.getAnnotations(0));
        checkNRPAnnotation(1,new Composition.Builder(AtomicSymbol.H).charge(1).build(),new Composition(),new Composition(), 0, consensus.getAnnotations(1));
        checkNRPAnnotation(1,new Composition.Builder(AtomicSymbol.H).charge(1).build(),new Composition(),new Composition(), 0, consensus.getAnnotations(2));
        checkNRPAnnotation(2,new Composition.Builder(AtomicSymbol.H).charge(1).build(),new Composition(),new Composition(), 0, consensus.getAnnotations(3));
        checkNRPAnnotation(2,new Composition.Builder(AtomicSymbol.H).charge(1).build(),new Composition(),new Composition(), 0, consensus.getAnnotations(4));
        Assert.assertFalse(consensus.getAnnotations(5).get(0).getOptFragmentAnnotation().isPresent());
        Assert.assertFalse(consensus.getAnnotations(6).get(0).getOptFragmentAnnotation().isPresent());
        Assert.assertFalse(consensus.getAnnotations(7).get(0).getOptFragmentAnnotation().isPresent());
        checkNRPAnnotation(3,new Composition.Builder(AtomicSymbol.H).charge(1).build(),new Composition(),new Composition(), 0, consensus.getAnnotations(8));

    }
//test neutralLosses annotation
    @Test
    public void testprocessPeak2() throws Exception {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();

        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00487"));
        NRPFragmenter NRPFragmenter = new NRPFragmenter();
        NRPFragmentTree simpleDirectedGraph= NRPFragmenter.createFragmentationTree(nrp);
        NRPPeaksGenerator.Builder peaksGeneratorBuilder = new NRPPeaksGenerator.Builder(simpleDirectedGraph);
        peaksGeneratorBuilder.addIon(AtomicSymbol.H,1,nrp.getAllMonomerNode());

        for (Monomer monomer : nrp.getAllMonomerNode()){
            if (monomer.getMonomerSymbol().equals("Argal")){
                Set<Monomer> setMonomers = new HashSet<>();
                Monomer argal = monomer;
                setMonomers.add(argal);
                peaksGeneratorBuilder.addNeutralLoss(Composition.parseComposition("H2O"),setMonomers);
            }
        }


        NRPPeaksGenerator peaksGenerator= peaksGeneratorBuilder.build();
        List<NRPSpectrum> peaks =peaksGenerator.generatePeaks();
        NRPSpectrum theoreticalSpectrum = peaks.get(0);

        Set<UUID> memberIds = new HashSet<>();
        memberIds.add(UUID.randomUUID());
        NRPConsensusSpectrum consensus = new NRPConsensusSpectrum(nrp, PeakList.Precision.DOUBLE,memberIds);
        NRPFragmentAnnotator annotator = new NRPFragmentAnnotator(new AbsoluteTolerance(0.5), theoreticalSpectrum);

        NRPConsensusPeakSink sink = new NRPConsensusPeakSink(consensus,0.2,2);
        //sink.setPeakList(consensus);
        annotator.setSink(sink);

        annotator.start(20);

        annotator.processPeak(114.79, 10.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));//leu
        annotator.processPeak(140.15, 10.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));//argal-h2o
        annotator.processPeak(157.20, 10.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));//nacleu
        annotator.processPeak(158.21, 10.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));//argal
        annotator.processPeak(253.04, 5.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));//argalleu-h2o
        annotator.processPeak(270.19, 5.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));//nacleuleu
        annotator.processPeak(271.20, 10.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));//argalleu
        annotator.processPeak(409.14, 10.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));//nacleuleuargal-h2o
        annotator.processPeak(427.30, 10.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));//nacleuleuargal

        annotator.end();

        Assert.assertEquals(9, consensus.size());
        Assert.assertFalse(consensus.getAnnotations(0).get(0).getOptFragmentAnnotation().isPresent());
        checkNRPAnnotation(1,new Composition.Builder(AtomicSymbol.H).charge(1).build(),Composition.parseComposition("H2O"),new Composition(), 0, consensus.getAnnotations(1));
        checkNRPAnnotation(1,new Composition.Builder(AtomicSymbol.H).charge(1).build(),new Composition(),new Composition(), 0, consensus.getAnnotations(2));
        checkNRPAnnotation(1,new Composition.Builder(AtomicSymbol.H).charge(1).build(),new Composition(),new Composition(), 0, consensus.getAnnotations(3));
        checkNRPAnnotation(2,new Composition.Builder(AtomicSymbol.H).charge(1).build(),Composition.parseComposition("H2O"),new Composition(), 0, consensus.getAnnotations(4));
        checkNRPAnnotation(2,new Composition.Builder(AtomicSymbol.H).charge(1).build(),new Composition(),new Composition(), 0, consensus.getAnnotations(5));
        checkNRPAnnotation(2,new Composition.Builder(AtomicSymbol.H).charge(1).build(),new Composition(),new Composition(), 0, consensus.getAnnotations(6));
        checkNRPAnnotation(3,new Composition.Builder(AtomicSymbol.H).charge(1).build(),Composition.parseComposition("H2O"),new Composition(), 0, consensus.getAnnotations(7));
        checkNRPAnnotation(3,new Composition.Builder(AtomicSymbol.H).charge(1).build(),new Composition(),new Composition(), 0, consensus.getAnnotations(8));

    }
    //test modifications annotation
    @Test
    public void testprocessPeak3() throws Exception {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();

        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00487"));
        NRPFragmenter NRPFragmenter = new NRPFragmenter();
        NRPFragmentTree simpleDirectedGraph = NRPFragmenter.createFragmentationTree(nrp);
        NRPPeaksGenerator.Builder peaksGeneratorBuilder = new NRPPeaksGenerator.Builder(simpleDirectedGraph);
        peaksGeneratorBuilder.addIon(AtomicSymbol.H, 1, nrp.getAllMonomerNode());

        for (Monomer monomer : nrp.getAllMonomerNode()) {
            if (monomer.getMonomerSymbol().equals("Argal")) {
                Set<Monomer> setMonomers = new HashSet<>();
                Monomer argal = monomer;
                setMonomers.add(argal);
                peaksGeneratorBuilder.addFixModification(Composition.parseComposition("O2"), setMonomers);
            }
        }


        NRPPeaksGenerator peaksGenerator = peaksGeneratorBuilder.build();
        List<NRPSpectrum> peaks = peaksGenerator.generatePeaks();
        NRPSpectrum theoreticalSpectrum = peaks.get(0);

        Set<UUID> memberIds = new HashSet<>();
        memberIds.add(UUID.randomUUID());
        NRPConsensusSpectrum consensus = new NRPConsensusSpectrum(nrp, PeakList.Precision.DOUBLE, memberIds);
        NRPFragmentAnnotator annotator = new NRPFragmentAnnotator(new AbsoluteTolerance(0.5), theoreticalSpectrum);

        NRPConsensusPeakSink sink = new NRPConsensusPeakSink(consensus, 0.2, 2);
        //sink.setPeakList(consensus);
        annotator.setSink(sink);

        annotator.start(20);

        annotator.processPeak(114.79, 10.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));//leu
        annotator.processPeak(157.20, 10.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));//nacleu
        annotator.processPeak(190.21, 10.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));//argal+o2
        annotator.processPeak(270.19, 5.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));//nacleuleu
        annotator.processPeak(303.20, 10.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));//argalleu+o2
        annotator.processPeak(459.30, 10.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));//nacleuleuargal+o2

        annotator.end();

        Assert.assertEquals(6, consensus.size());
        Assert.assertFalse(consensus.getAnnotations(0).get(0).getOptFragmentAnnotation().isPresent());
        checkNRPAnnotation(1, new Composition.Builder(AtomicSymbol.H).charge(1).build(), new Composition(), new Composition(), 0, consensus.getAnnotations(1));
        checkNRPAnnotation(1, new Composition.Builder(AtomicSymbol.H).charge(1).build(), new Composition(), Composition.parseComposition("O2"), 1, consensus.getAnnotations(2));
        checkNRPAnnotation(2, new Composition.Builder(AtomicSymbol.H).charge(1).build(), new Composition(), new Composition(), 0, consensus.getAnnotations(3));
        checkNRPAnnotation(2, new Composition.Builder(AtomicSymbol.H).charge(1).build(), new Composition(), Composition.parseComposition("O2"), 1, consensus.getAnnotations(4));
        checkNRPAnnotation(3, new Composition.Builder(AtomicSymbol.H).charge(1).build(), new Composition(), Composition.parseComposition("O2"), 1, consensus.getAnnotations(5));

    }
    //test ions annotation
    @Test
    public void testprocessPeak4() throws Exception {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();

        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00487"));
        NRPFragmenter NRPFragmenter = new NRPFragmenter();
        NRPFragmentTree simpleDirectedGraph = NRPFragmenter.createFragmentationTree(nrp);
        NRPPeaksGenerator.Builder peaksGeneratorBuilder = new NRPPeaksGenerator.Builder(simpleDirectedGraph);
        peaksGeneratorBuilder.addIon(AtomicSymbol.H, 1, nrp.getAllMonomerNode());

        Set<Monomer> setMonomers = new HashSet<>();
        for (Monomer monomer : nrp.getAllMonomerNode()) {
            if (monomer.getMonomerSymbol().equals("NAc-Leu"))
                setMonomers.add(monomer);

            if (monomer.getMonomerSymbol().equals("Leu"))
                setMonomers.add(monomer);

        }

        peaksGeneratorBuilder.addIon(AtomicSymbol.Cl, 1, setMonomers);
        Composition cl=new Composition.Builder().add(AtomicSymbol.Cl).charge(1).build();
        Composition h=new Composition.Builder().add(AtomicSymbol.H).charge(1).build();
        //System.out.println(cl.getMolecularMass());
        //System.out.println(h.getMolecularMass());
        //System.out.println(cl.getMolecularMass()+h.getMolecularMass());
        NRPPeaksGenerator peaksGenerator = peaksGeneratorBuilder.build();
        List<NRPSpectrum> peaks = peaksGenerator.generatePeaks();
        NRPSpectrum theoreticalSpectrum = peaks.get(0);
        //System.out.println(theoreticalSpectrum.getPrecursor().getMz());

        Set<UUID> memberIds = new HashSet<>();
        memberIds.add(UUID.randomUUID());
        NRPConsensusSpectrum consensus = new NRPConsensusSpectrum(nrp, PeakList.Precision.DOUBLE, memberIds);
        NRPFragmentAnnotator annotator = new NRPFragmentAnnotator(new AbsoluteTolerance(0.5), theoreticalSpectrum);

        NRPConsensusPeakSink sink = new NRPConsensusPeakSink(consensus, 0.2, 2);
        //sink.setPeakList(consensus);
        annotator.setSink(sink);

        annotator.start(20);

        annotator.processPeak(114.19, 10.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));//leuH+
        annotator.processPeak(148.15, 10.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));//leuCl+
        annotator.processPeak(152.57, 5.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));//nacleuleuclh+
        annotator.processPeak(153.08, 10.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));//argalleuclh+
        annotator.processPeak(157.20, 10.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));//nacleuH+
        annotator.processPeak(158.21, 10.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));//argalH+
        annotator.processPeak(191.16, 10.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));//nacleuCl+
        annotator.processPeak(231.13, 10.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));//nacleuleuargalclh+
        annotator.processPeak(271.20, 10.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));//argalleuh+
        annotator.processPeak(304.15, 5.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));//nacleuleucl+
        annotator.processPeak(305.16, 10.0, Collections.singletonList(new NRPLibPeakAnnotation(10, 0, 0)));//argalleucl+

        annotator.end();

        Assert.assertEquals(11, consensus.size());
        checkNRPAnnotation(1, new Composition.Builder(AtomicSymbol.H).charge(1).build(), new Composition(), new Composition(), 0, consensus.getAnnotations(0));
        checkNRPAnnotation(1, new Composition.Builder(AtomicSymbol.Cl).charge(1).build(), new Composition(), new Composition(), 0, consensus.getAnnotations(1));
        checkNRPAnnotation(2, new Composition.Builder(AtomicSymbol.H).add(AtomicSymbol.Cl).charge(2).build(), new Composition(), new Composition(), 0, consensus.getAnnotations(2));
        checkNRPAnnotation(2, new Composition.Builder(AtomicSymbol.H).add(AtomicSymbol.Cl).charge(2).build(), new Composition(), new Composition(), 0, consensus.getAnnotations(3));
        checkNRPAnnotation(1, new Composition.Builder(AtomicSymbol.H).charge(1).build(), new Composition(),new Composition(), 0, consensus.getAnnotations(4));
        checkNRPAnnotation(1, new Composition.Builder(AtomicSymbol.H).charge(1).build(), new Composition(), new Composition(), 0, consensus.getAnnotations(5));
        checkNRPAnnotation(1, new Composition.Builder(AtomicSymbol.Cl).charge(1).build(), new Composition(), new Composition(), 0, consensus.getAnnotations(6));
        checkNRPAnnotation(3, new Composition.Builder(AtomicSymbol.H).add(AtomicSymbol.Cl).charge(2).build(), new Composition(), new Composition(), 0, consensus.getAnnotations(7));
        checkNRPAnnotation(2, new Composition.Builder(AtomicSymbol.H).charge(1).build(), new Composition(),new Composition(), 0, consensus.getAnnotations(8));
        checkNRPAnnotation(2, new Composition.Builder(AtomicSymbol.Cl).charge(1).build(), new Composition(), new Composition(), 0, consensus.getAnnotations(9));
        checkNRPAnnotation(2, new Composition.Builder(AtomicSymbol.Cl).charge(1).build(), new Composition(), new Composition(), 0, consensus.getAnnotations(10));
    }


    private void checkNRPAnnotation(int expectedResidueNumber, Composition expectedCharge, Composition expectedNeutralLoss, Composition expectedModifications, int expectedModNumber, List<NRPLibPeakAnnotation> actual) {

        Assert.assertEquals(1, actual.size());
        NRPFragmentAnnotation actualAnnotation = actual.get(0).getOptFragmentAnnotation().get();
        Assert.assertEquals(expectedResidueNumber,actualAnnotation.getFragment().size());
        Assert.assertEquals(expectedCharge,actualAnnotation.getChargedGroup());
        Assert.assertEquals(expectedNeutralLoss, actualAnnotation.getNeutralLoss());
        Assert.assertEquals(expectedModifications, actualAnnotation.getModifications());
        Assert.assertEquals(expectedModNumber, actualAnnotation.getAllModifications().size());
    }



}