package utils;

import molecules.Monomer;
import molecules.NRP;
import molecules.NRPBuilder;
import molecules.norine.NorineNRP;
import molecules.norine.NorineNRPStore;
import molecules.norine.NorineResidue;
import molecules.norine.NorineResidueStore;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ericart on 08.03.2016.
 */
public class SmilesUtilsTest {

    @Test
    public void testSmilesToFormula() throws Exception {
        NorineNRP nrp = NorineNRPStore.getInstance().getNRP("NOR00693");
        String predictedFormula= SmilesUtils.smilesToFormula(nrp.getSmiles(), true);
        assertEquals(nrp.general.getFormula(),predictedFormula);
    }

    @Test
    public void testSmilesToFormula2() throws Exception {

        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp1 =nrpBuilder.createNRP(NorineNRPStore.getInstance().getNRP("NOR00331"));
        Monomer ser=null;

        for (Monomer monomer : nrp1.getAllMonomerNode()){
            if (monomer.getMonomerSymbol().equals("Ser")){
                ser = monomer;
                break;
            }
        }
        String predictedFormula= SmilesUtils.smilesToFormula(ser.getNorineResidue().getSmarts(), false);
        assertEquals("C3H5NO2",predictedFormula);
    }

}