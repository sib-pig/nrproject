package utils;

import molecules.Bond;
import molecules.Monomer;
import molecules.NRP;
import molecules.NRPBuilder;
import molecules.norine.*;
import org.expasy.mzjava.core.mol.Composition;

import org.jgrapht.graph.SimpleGraph;
import org.junit.Test;
import utils.CompositionsJoiner;

import static org.junit.Assert.*;

/**
 * Created by ericart on 02.02.2016.
 */

public class CompositionsJoinerTest {

    @Test
    public void testcalculateComposition() throws Exception {

        //we create the simple graph for the NOR00209
        NRP nrp = new NRPBuilder().createNRP(NorineNRPStore.getInstance().getNRP("NOR00209"));

        Composition predictedComposition = CompositionsJoiner.joinCompositions(nrp.getAllMonomerNode());
        Composition norineComposition = Composition.parseComposition("C12H18N2O5");

        assertEquals(norineComposition,predictedComposition);

    }
}