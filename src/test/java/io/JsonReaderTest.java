package io;

import com.fasterxml.jackson.databind.JsonNode;
import io.JsonReader;
import molecules.norine.NorineMonomer;

import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static java.lang.System.getProperty;
import static org.junit.Assert.*;

/**
 * Created by ericart on 11.11.2015.
 */
public class JsonReaderTest {


    @Test
    public void testReadFile() throws IOException {
        JsonReader reader = new JsonReader();
        String NRPResource=System.getProperty("NRP.store.resource", "molecules/norine/norineNRPs.json");
        JsonNode jsonNode = reader.readFile(ClassLoader.getSystemResourceAsStream(NRPResource));
        assertFalse(jsonNode.path("peptides").isMissingNode());
    }


    @Test
    public void testReadFile2() throws IOException {
        JsonReader reader = new JsonReader();
        String resourcesDir = getProperty("user.dir").replace("\\","/") +"/src/main/resources/";
        String NRPResource=System.getProperty("NRP.store.resource", "/molecules/norine/norineNRPs.json");
        Path path= Paths.get(resourcesDir+NRPResource);
        JsonNode jsonNode = reader.readFile(path);
        assertFalse(jsonNode.path("peptides").isMissingNode());
    }

    @Ignore
    public void testReadREST () throws IOException {
        JsonReader reader = new JsonReader();
        String NRPResource=System.getProperty("NRP.store.resource", "http://bioinfo.lifl.fr/norine/rest/name/json/ALL");
        JsonNode jsonNode =reader.readREST(NRPResource);
        assertFalse(jsonNode.path("peptides").isMissingNode());
    }

    @Test
    public void testjson2list () throws IOException {
        JsonReader reader = new JsonReader();
        String MonomerResource=System.getProperty("monomer.store.resource", "molecules/norine/norineMonomers.json");
        JsonNode jsonNode =reader.readFile(ClassLoader.getSystemResourceAsStream(MonomerResource));
        List <NorineMonomer> arrayNorineMonomers = reader.jsonNode2list(jsonNode, NorineMonomer.class);

        assertFalse(arrayNorineMonomers.isEmpty());
        assertEquals("beta-methyl-phenylalanine", arrayNorineMonomers.get(0).getNameAA());
        assertEquals("C10H13NO2", arrayNorineMonomers.get(0).getMolecularFormula());
        assertEquals("C1=CC=C(C=C1)C(C)C(C(=O)O)N", arrayNorineMonomers.get(0).getSmiles());
        assertEquals(179.2291, arrayNorineMonomers.get(0).getMolecularWeight(),0);
        assertNull(arrayNorineMonomers.get(0).getPubchem());
        assertNull(arrayNorineMonomers.get(0).getIsomeric());

    }



}
