package io.avro;

import com.google.common.base.Optional;
import molecules.NRP;
import molecules.NRPBuilder;
import molecules.NRPFragment;
import molecules.norine.NorineNRPStore;
import ms.spectrum.NRPFragmentAnnotation;
import ms.spectrum.NRPLibPeakAnnotation;
import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.apache.avro.io.EncoderFactory;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.expasy.mzjava.core.mol.AtomicSymbol;
import org.expasy.mzjava.core.mol.Composition;
import org.junit.Assert;
import org.junit.Test;

import java.io.StringWriter;

import static org.junit.Assert.*;

/**
 * Created by ericart on 13.07.2016.
 */
public class NRPLibPeakAnnotationWriterTest {

    //without fragment annotation
    @Test
    public void testWrite() throws Exception {
        NRPLibPeakAnnotation annotation = new NRPLibPeakAnnotation(20, 0.04, 200);

        NRPLibPeakAnnotationWriter writer = new NRPLibPeakAnnotationWriter();

        StringWriter out = new StringWriter();
        JsonGenerator gen = new JsonFactory().createJsonGenerator(out);
        Encoder encoder = EncoderFactory.get().jsonEncoder(writer.createSchema(), gen);
        writer.write(annotation, encoder);
        encoder.flush();

        Assert.assertEquals("{" +
                "\"mergedPeakCount\":20," +
                "\"mzStd\":0.04," +
                "\"intensityStd\":200.0," +
                "\"fragAnnotation\":null}", out.toString());

    }

    //with fragment annotation
    @Test
    public void testWrite2() throws Exception {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00487"));
        NRPFragment nrpFragment = new NRPFragment(nrp.cloneSimpleGraph(),nrp.getComposition(),nrp.getNrpId(),1);
        NRPLibPeakAnnotation annotation = new NRPLibPeakAnnotation(20, 0.04, 200, Optional.of(new NRPFragmentAnnotation(1, nrpFragment, new Composition.Builder().add(AtomicSymbol.H).charge(1).build())));

        NRPLibPeakAnnotationWriter writer = new NRPLibPeakAnnotationWriter();

        StringWriter out = new StringWriter();
        JsonGenerator gen = new JsonFactory().createJsonGenerator(out);
        Encoder encoder = EncoderFactory.get().jsonEncoder(writer.createSchema(), gen);
        writer.write(annotation, encoder);
        encoder.flush();

        Assert.assertEquals( "{\"mergedPeakCount\":20," +
                "\"mzStd\":0.04," +
                "\"intensityStd\":200.0," +
                "\"fragAnnotation\":{" +
                "\"ms.spectrum.NRPFragmentAnnotation\":{" +
                "\"fragment\":{" +
                "\"monomerIndexes\":[0,1,2]," +
                "\"bonds\":[[0,2],[0,1]]," +
                "\"fragmentComposition\":{" +
                "\"composition\":{\"H\":38,\"C\":20,\"N\":6,\"O\":4}}," +
                "\"precursorId\":\"NOR00487\"," +
                "\"msStage\":1}," +
                "\"charge\":1," +
                "\"chargeComposition\":{\"composition\":{\"H\":1}}," +
                "\"neutralLoss\":{\"composition\":{}}," +
                "\"modifications\":{\"composition\":{}}," +
                "\"modificationPositions\":{}}}}", out.toString());

    }


    @Test
    public void testCreateRecordFields() throws Exception {

        String expected = "{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"NRPLibPeakAnnotation\",\n" +
                "  \"namespace\" : \"ms.spectrum\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"mergedPeakCount\",\n" +
                "    \"type\" : \"int\"\n" +
                "  }, {\n" +
                "    \"name\" : \"mzStd\",\n" +
                "    \"type\" : \"double\"\n" +
                "  }, {\n" +
                "    \"name\" : \"intensityStd\",\n" +
                "    \"type\" : \"double\"\n" +
                "  }, {\n" +
                "    \"name\" : \"fragAnnotation\",\n" +
                "    \"type\" : [ {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"NRPFragmentAnnotation\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"fragment\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"record\",\n" +
                "          \"name\" : \"NRPFragment\",\n" +
                "          \"namespace\" : \"molecules\",\n" +
                "          \"fields\" : [ {\n" +
                "            \"name\" : \"monomerIndexes\",\n" +
                "            \"type\" : {\n" +
                "              \"type\" : \"array\",\n" +
                "              \"items\" : \"int\"\n" +
                "            }\n" +
                "          }, {\n" +
                "            \"name\" : \"bonds\",\n" +
                "            \"type\" : {\n" +
                "              \"type\" : \"array\",\n" +
                "              \"items\" : {\n" +
                "                \"type\" : \"array\",\n" +
                "                \"items\" : \"int\"\n" +
                "              }\n" +
                "            }\n" +
                "          }, {\n" +
                "            \"name\" : \"fragmentComposition\",\n" +
                "            \"type\" : {\n" +
                "              \"type\" : \"record\",\n" +
                "              \"name\" : \"Composition\",\n" +
                "              \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "              \"fields\" : [ {\n" +
                "                \"name\" : \"composition\",\n" +
                "                \"type\" : {\n" +
                "                  \"type\" : \"map\",\n" +
                "                  \"values\" : \"int\"\n" +
                "                }\n" +
                "              } ]\n" +
                "            }\n" +
                "          }, {\n" +
                "            \"name\" : \"precursorId\",\n" +
                "            \"type\" : \"string\"\n" +
                "          }, {\n" +
                "            \"name\" : \"msStage\",\n" +
                "            \"type\" : \"int\"\n" +
                "          } ]\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"charge\",\n" +
                "        \"type\" : \"int\"\n" +
                "      }, {\n" +
                "        \"name\" : \"chargeComposition\",\n" +
                "        \"type\" : \"org.expasy.mzjava_avro.core.mol.Composition\"\n" +
                "      }, {\n" +
                "        \"name\" : \"neutralLoss\",\n" +
                "        \"type\" : \"org.expasy.mzjava_avro.core.mol.Composition\"\n" +
                "      }, {\n" +
                "        \"name\" : \"modifications\",\n" +
                "        \"type\" : \"org.expasy.mzjava_avro.core.mol.Composition\"\n" +
                "      }, {\n" +
                "        \"name\" : \"modificationPositions\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"map\",\n" +
                "          \"values\" : {\n" +
                "            \"type\" : \"array\",\n" +
                "            \"items\" : {\n" +
                "              \"type\" : \"record\",\n" +
                "              \"name\" : \"Modification\",\n" +
                "              \"namespace\" : \"molecules.modification\",\n" +
                "              \"fields\" : [ {\n" +
                "                \"name\" : \"modificationComposition\",\n" +
                "                \"type\" : \"org.expasy.mzjava_avro.core.mol.Composition\"\n" +
                "              }, {\n" +
                "                \"name\" : \"modificationType\",\n" +
                "                \"type\" : \"string\"\n" +
                "              } ]\n" +
                "            }\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, \"null\" ]\n" +
                "  } ]\n" +
                "}";


        assertEquals(new Schema.Parser().parse(expected).toString(true),new NRPLibPeakAnnotationWriter().createSchema().toString(true));

    }
}