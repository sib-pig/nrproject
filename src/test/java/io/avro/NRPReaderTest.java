package io.avro;

import molecules.Monomer;
import molecules.NRP;
import molecules.NRPBuilder;
import molecules.norine.*;
import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ericart on 13.07.2016.
 */
public class NRPReaderTest {

    @Test
    public void testRead() throws Exception {

        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00487"));

        String leuResidue="";
        String nacLeuResidue="";
        String argalResidue="";

        for (Monomer monomer:nrp.getAllMonomerNode()){
            if (monomer.getMonomerSymbol().equals("Leu")){
                leuResidue=monomer.getNorineResidue().getId();
            }else if (monomer.getMonomerSymbol().equals("NAc-Leu")){
                nacLeuResidue=monomer.getNorineResidue().getId();
            }else if (monomer.getMonomerSymbol().equals("Argal")){
                argalResidue=monomer.getNorineResidue().getId();
            }
        }


        NRPReader reader = new NRPReader();
        Decoder in = DecoderFactory.get().jsonDecoder(reader.createSchema(),
                "{\"precursorId\":\"NOR00487\"," +
                        "\"precursorStructure\":\"LINEAR\"," +
                        "\"precursorComposition\":{" +
                        "\"composition\":{\"H\":38,\"C\":20,\"N\":6,\"O\":4}}," +
                        "\"precursorMonomers\":[{" +
                        "\"monomerIndex\":0," +
                        "\"originalMonomer\":\"Leu\"," +
                        "\"residueID\":\""+leuResidue+"\"," +
                        "\"residueComposition\":{" +
                        "\"composition\":{\"H\":11,\"C\":6,\"N\":1,\"O\":1}}},{" +
                        "\"monomerIndex\":1," +
                        "\"originalMonomer\":\"NAc-Leu\"," +
                        "\"residueID\":\""+nacLeuResidue+"\"," +
                        "\"residueComposition\":{" +
                        "\"composition\":{\"H\":14,\"C\":8,\"N\":1,\"O\":2}}},{" +
                        "\"monomerIndex\":2," +
                        "\"originalMonomer\":\"Argal\"," +
                        "\"residueID\":\""+argalResidue+"\"," +
                        "\"residueComposition\":{" +
                        "\"composition\":{\"H\":13,\"C\":6,\"N\":4,\"O\":1}}}]," +
                        "\"precursorbonds\":[[0,2],[0,1]]}");


        NRP nrpRead = reader.read(in);

        assertEquals(nrp.getComposition(), nrpRead.getComposition());
        assertEquals(nrp.getNrpType(), nrpRead.getNrpType());
        assertEquals(nrp.getNrpId(), nrpRead.getNrpId());
        assertEquals(nrp.getAllMonomerNode().toString(),nrpRead.getAllMonomerNode().toString());
        assertEquals(nrp.getAllBond().toString(), nrpRead.getAllBond().toString());

    }

    @Test
    public void testCreateRecordFields() throws Exception {
        String expected = "{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"NRP\",\n" +
                "  \"namespace\" : \"molecules\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"precursorId\",\n" +
                "    \"type\" : \"string\"\n" +
                "  }, {\n" +
                "    \"name\" : \"precursorStructure\",\n" +
                "    \"type\" : \"string\"\n" +
                "  }, {\n" +
                "    \"name\" : \"precursorComposition\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"Composition\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"composition\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"map\",\n" +
                "          \"values\" : \"int\"\n" +
                "        }\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"precursorMonomers\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"array\",\n" +
                "      \"items\" : {\n" +
                "        \"type\" : \"record\",\n" +
                "        \"name\" : \"Monomer\",\n" +
                "        \"fields\" : [ {\n" +
                "          \"name\" : \"monomerIndex\",\n" +
                "          \"type\" : \"int\"\n" +
                "        }, {\n" +
                "          \"name\" : \"originalMonomer\",\n" +
                "          \"type\" : \"string\"\n" +
                "        }, {\n" +
                "          \"name\" : \"residueID\",\n" +
                "          \"type\" : \"string\"\n" +
                "        }, {\n" +
                "          \"name\" : \"residueComposition\",\n" +
                "          \"type\" : \"org.expasy.mzjava_avro.core.mol.Composition\"\n" +
                "        } ]\n" +
                "      }\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"precursorbonds\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"array\",\n" +
                "      \"items\" : {\n" +
                "        \"type\" : \"array\",\n" +
                "        \"items\" : \"int\"\n" +
                "      }\n" +
                "    }\n" +
                "  } ]\n" +
                "}";

        assertEquals(new Schema.Parser().parse(expected).toString(true),new NRPReader().createSchema().toString(true));


    }
}