package io.avro;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import molecules.Monomer;
import molecules.NRP;
import molecules.NRPBuilder;
import molecules.NRPFragment;
import molecules.modification.Modification;
import molecules.norine.NorineNRPStore;
import ms.spectrum.NRPFragmentAnnotation;
import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;
import org.expasy.mzjava.core.mol.AtomicSymbol;
import org.expasy.mzjava.core.mol.Composition;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ericart on 13.07.2016.
 */
public class NRPFragmentAnnotationReaderTest {

    @Test
    public void testRead() throws Exception {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00487"));

        NRPFragmentAnnotationReader reader = new NRPFragmentAnnotationReader();
        reader.setNRP(nrp);

        Decoder in = DecoderFactory.get().jsonDecoder(reader.createSchema(),
                                "{\"fragment\":{\"monomerIndexes\":[0,1,2]," +
                                        "\"bonds\":[[0,2],[0,1]]," +
                                        "\"fragmentComposition\":{" +
                                        "\"composition\":{\"H\":38,\"C\":20,\"N\":6,\"O\":4}}," +
                                        "\"precursorId\":\"NOR00487\"," +
                                        "\"msStage\":1},\"charge\":1," +
                                        "\"chargeComposition\":{\"composition\":{\"H\":1}}," +
                                        "\"neutralLoss\":{\"composition\":{\"H\":2,\"O\":1}}," +
                                        "\"modifications\":{\"composition\":{\"H\":3,\"O\":4,\"P\":1}}," +
                                        "\"modificationPositions\":{" +
                                        "\"1\":[{\"modificationComposition\":{\"composition\":{\"H\":2,\"O\":1}},\"modificationType\":\"VARIABLE\"}]," +
                                        "\"0\":[{\"modificationComposition\":{\"composition\":{\"H\":1,\"O\":3,\"P\":1}},\"modificationType\":\"FIX\"}]}}\n");


        NRPFragment nrpFragment = new NRPFragment(nrp.cloneSimpleGraph(),nrp.getComposition(),nrp.getNrpId(),1);
        Monomer modifiedMonomer1=null;
        Monomer modifiedMonomer2=null;
        for (Monomer monomer : nrpFragment.getAllMonomerNode()){
            if (monomer.getIndex()==0){
                modifiedMonomer1=monomer;
            }
            if (monomer.getIndex()==1){
                modifiedMonomer2=monomer;
            }
        }
        Composition proton = new Composition.Builder().add(AtomicSymbol.H).charge(1).build();
        Composition phosphorylation = Composition.parseComposition("HPO3");
        Composition neutralLoss = Composition.parseComposition("H2O");
        ListMultimap<Monomer, Modification> monomersModifications = ArrayListMultimap.create();
        monomersModifications.put(modifiedMonomer1,new Modification(phosphorylation, Modification.ModificationType.FIX));
        monomersModifications.put(modifiedMonomer2,new Modification(neutralLoss, Modification.ModificationType.VARIABLE));

        NRPFragmentAnnotation nrpFragmentAnnotation = new NRPFragmentAnnotation(1, nrpFragment,monomersModifications,proton, neutralLoss);
        NRPFragmentAnnotation nrpFragmentAnnotationRead =reader.read(in);


        assertEquals(nrpFragmentAnnotation.getFragment().getSimpleGraph(),nrpFragmentAnnotationRead.getFragment().getSimpleGraph());
        assertEquals(nrpFragmentAnnotation.getFragment().getComposition(),nrpFragmentAnnotationRead.getFragment().getComposition());
        assertEquals(nrpFragmentAnnotation.getModifications(),nrpFragmentAnnotationRead.getModifications());
        assertEquals(nrpFragmentAnnotation.getModifications(modifiedMonomer1),nrpFragmentAnnotationRead.getModifications(modifiedMonomer1));
        assertEquals(nrpFragmentAnnotation.getModifications(modifiedMonomer2),nrpFragmentAnnotationRead.getModifications(modifiedMonomer2));
        assertEquals(nrpFragmentAnnotation.getChargedGroup(),nrpFragmentAnnotationRead.getChargedGroup());
        assertEquals(nrpFragmentAnnotation.getCharge(),nrpFragmentAnnotationRead.getCharge());
        assertEquals(nrpFragmentAnnotation.getNeutralLoss(),nrpFragmentAnnotationRead.getNeutralLoss());
    }

    @Test
    public void testCreateRecordFields() throws Exception {


        String expected = "{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"NRPFragmentAnnotation\",\n" +
                "  \"namespace\" : \"ms.spectrum\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"fragment\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"NRPFragment\",\n" +
                "      \"namespace\" : \"molecules\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"monomerIndexes\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : \"int\"\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"bonds\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"array\",\n" +
                "            \"items\" : \"int\"\n" +
                "          }\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"fragmentComposition\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"record\",\n" +
                "          \"name\" : \"Composition\",\n" +
                "          \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "          \"fields\" : [ {\n" +
                "            \"name\" : \"composition\",\n" +
                "            \"type\" : {\n" +
                "              \"type\" : \"map\",\n" +
                "              \"values\" : \"int\"\n" +
                "            }\n" +
                "          } ]\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"precursorId\",\n" +
                "        \"type\" : \"string\"\n" +
                "      }, {\n" +
                "        \"name\" : \"msStage\",\n" +
                "        \"type\" : \"int\"\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"charge\",\n" +
                "    \"type\" : \"int\"\n" +
                "  }, {\n" +
                "    \"name\" : \"chargeComposition\",\n" +
                "    \"type\" : \"org.expasy.mzjava_avro.core.mol.Composition\"\n" +
                "  }, {\n" +
                "    \"name\" : \"neutralLoss\",\n" +
                "    \"type\" : \"org.expasy.mzjava_avro.core.mol.Composition\"\n" +
                "  }, {\n" +
                "    \"name\" : \"modifications\",\n" +
                "    \"type\" : \"org.expasy.mzjava_avro.core.mol.Composition\"\n" +
                "  }, {\n" +
                "    \"name\" : \"modificationPositions\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"map\",\n" +
                "      \"values\" : {\n" +
                "        \"type\" : \"array\",\n" +
                "        \"items\" : {\n" +
                "          \"type\" : \"record\",\n" +
                "          \"name\" : \"Modification\",\n" +
                "          \"namespace\" : \"molecules.modification\",\n" +
                "          \"fields\" : [ {\n" +
                "            \"name\" : \"modificationComposition\",\n" +
                "            \"type\" : \"org.expasy.mzjava_avro.core.mol.Composition\"\n" +
                "          }, {\n" +
                "            \"name\" : \"modificationType\",\n" +
                "            \"type\" : \"string\"\n" +
                "          } ]\n" +
                "        }\n" +
                "      }\n" +
                "    }\n" +
                "  } ]\n" +
                "}\n";


        assertEquals(new Schema.Parser().parse(expected).toString(true),new NRPFragmentAnnotationReader().createSchema().toString(true));



    }
}