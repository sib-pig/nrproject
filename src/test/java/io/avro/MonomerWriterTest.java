package io.avro;

import molecules.Monomer;
import molecules.norine.NorineMonomer;
import molecules.norine.NorineMonomerStore;
import molecules.norine.NorineResidue;
import molecules.norine.NorineResidueStore;
import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.apache.avro.io.EncoderFactory;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.expasy.mzjava.core.mol.Atom;
import org.expasy.mzjava.core.mol.Composition;
import org.junit.Test;

import java.io.StringWriter;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Created by ericart on 12.07.2016.
 */
public class MonomerWriterTest {

    @Test
    public void testWrite() throws Exception {
        NorineMonomer norineMonomer = NorineMonomerStore.getInstance().getMonomer("Leu");
        NorineResidue norineResidue = NorineResidueStore.getInstance().getResidue("1533");
        Monomer monomer = new Monomer(norineMonomer,norineResidue,1);

        Composition comp =NorineResidueStore.getInstance().getResidue("1533").getComposition();
        String stringComposition  = "";
        for (Atom atom : comp.getAtoms()) {
            stringComposition= stringComposition+"\""+atom.toString()+"\""+":"+comp.getCount(atom)+",";
        }
        stringComposition = stringComposition.substring(0, stringComposition.length() - 1);

        StringWriter out = new StringWriter();
        JsonGenerator gen = new JsonFactory().createJsonGenerator(out);
        MonomerWriter writer = new MonomerWriter();
        Encoder encoder = EncoderFactory.get().jsonEncoder(writer.createSchema(), gen);
        writer.write(monomer, encoder);
        encoder.flush();

        assertEquals("{\"monomerIndex\":1," +
                "\"originalMonomer\":\"Leu\"," +
                "\"residueID\":\"1533\"," +
                "\"residueComposition\":{" +
                "\"composition\":{" +
                stringComposition+"}}}", out.toString());
    }

    @Test
    public void testCreateRecordFields() throws Exception {

        String expected = "{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"Monomer\",\n" +
                "  \"namespace\" : \"molecules\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"monomerIndex\",\n" +
                "    \"type\" : \"int\"\n" +
                "  }, {\n" +
                "    \"name\" : \"originalMonomer\",\n" +
                "    \"type\" : \"string\"\n" +
                "  }, {\n" +
                "    \"name\" : \"residueID\",\n" +
                "    \"type\" : \"string\"\n" +
                "  }, {\n" +
                "    \"name\" : \"residueComposition\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"Composition\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"composition\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"map\",\n" +
                "          \"values\" : \"int\"\n" +
                "        }\n" +
                "      } ]\n" +
                "    }\n" +
                "  } ]\n" +
                "}";

        assertEquals(new Schema.Parser().parse(expected).toString(true),new MonomerWriter().createSchema().toString(true));

    }
}