package io.avro;

import molecules.Monomer;
import molecules.modification.Modification;
import molecules.norine.NorineMonomer;
import molecules.norine.NorineMonomerStore;
import molecules.norine.NorineResidue;
import molecules.norine.NorineResidueStore;
import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;
import org.expasy.mzjava.core.mol.Composition;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ericart on 12.07.2016.
 */
public class MonomerReaderTest {

    @Test
    public void testRead() throws Exception {

        MonomerReader reader = new MonomerReader();
        Decoder in = DecoderFactory.get().jsonDecoder(reader.createSchema(),
                        "{\"monomerIndex\":1," +
                        "\"originalMonomer\":\"Leu\"," +
                        "\"residueID\":\"1533\"," +
                        "\"residueComposition\":{" +
                        "\"composition\":" +
                        "{\"H\":11,\"C\":6,\"N\":1,\"O\":1}}}");


        NorineMonomer norineMonomer = NorineMonomerStore.getInstance().getMonomer("Leu");
        NorineResidue norineResidue = NorineResidueStore.getInstance().getResidue("1533");
        Monomer monomer = new Monomer(norineMonomer,norineResidue,1);
        Monomer monomerRead = reader.read(in);
        assertEquals(monomer.getComposition(), monomerRead.getComposition());
        assertEquals(monomer.getIndex(),monomerRead.getIndex());
        assertEquals(monomer.getMonomerSymbol(), monomerRead.getMonomerSymbol());
        assertEquals(monomer.getNorineMonomer(), monomerRead.getNorineMonomer());
        assertEquals(monomer.getNorineResidue(), monomerRead.getNorineResidue());

    }

    @Test
    public void testCreateRecordFields() throws Exception {
        String expected = "{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"Monomer\",\n" +
                "  \"namespace\" : \"molecules\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"monomerIndex\",\n" +
                "    \"type\" : \"int\"\n" +
                "  }, {\n" +
                "    \"name\" : \"originalMonomer\",\n" +
                "    \"type\" : \"string\"\n" +
                "  }, {\n" +
                "    \"name\" : \"residueID\",\n" +
                "    \"type\" : \"string\"\n" +
                "  }, {\n" +
                "    \"name\" : \"residueComposition\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"Composition\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"composition\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"map\",\n" +
                "          \"values\" : \"int\"\n" +
                "        }\n" +
                "      } ]\n" +
                "    }\n" +
                "  } ]\n" +
                "}";

        assertEquals(new Schema.Parser().parse(expected).toString(true),new MonomerReader().createSchema().toString(true));
    }
}