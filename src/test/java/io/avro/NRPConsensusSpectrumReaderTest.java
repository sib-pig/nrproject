package io.avro;

import molecules.NRP;
import molecules.NRPBuilder;
import molecules.norine.NorineNRPStore;
import ms.spectrum.NRPConsensusSpectrum;
import ms.spectrum.NRPLibPeakAnnotation;
import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessor;
import org.expasy.mzjava.utils.URIBuilder;
import org.junit.Ignore;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

/**
 * Created by ericart on 13.07.2016.
 */
@Ignore
public class NRPConsensusSpectrumReaderTest {

    @Test
    public void testRead() throws Exception {

        NRPConsensusSpectrumReader reader = new NRPConsensusSpectrumReader(com.google.common.base.Optional.<PeakList.Precision>absent(),
                Collections.<PeakProcessor<NRPLibPeakAnnotation, NRPLibPeakAnnotation>>emptyList(),new NRPLibPeakAnnotationReader());

        Decoder in = DecoderFactory.get().jsonDecoder(reader.createSchema(), "{\"precursor\":" +
                "{\"polarity\":\"POSITIVE\"," +
                "\"charge\":[2]," +
                "\"mz\":1789.98765," +
                "\"intensity\":875.68}," +
                "\"msLevel\":2," +
                "\"name\":\"name\"," +
                "\"simScoreMean\":0.8," +
                "\"simScoreStdev\":0.2," +
                "\"precursorMzMean\":3412.96," +
                "\"precursorMzStdev\":0.01," +
                "\"memberIds\":[{" +
                "\"mostSignificantBits\":6906448715414564879," +
                "\"leastSignificantBits\":-3377628232451089806},{" +
                "\"mostSignificantBits\":8159020250014517468," +
                "\"leastSignificantBits\":-3276528637398377287},{" +
                "\"mostSignificantBits\":6453290794183652199," +
                "\"leastSignificantBits\":-7855891324758780007},{" +
                "\"mostSignificantBits\":-2439663895838265701," +
                "\"leastSignificantBits\":5432637156828437242},{" +
                "\"mostSignificantBits\":-2330906535205672779," +
                "\"leastSignificantBits\":3380397291928878215},{" +
                "\"mostSignificantBits\":272555917829435557," +
                "\"leastSignificantBits\":-502732256918155454},{" +
                "\"mostSignificantBits\":-6464087018820727044," +
                "\"leastSignificantBits\":9158344342526998714},{" +
                "\"mostSignificantBits\":2954576207831929," +
                "\"leastSignificantBits\":-7401919710944613243},{" +
                "\"mostSignificantBits\":-7477072015556504347," +
                "\"leastSignificantBits\":-4759461241505446441}]," +
                "\"nrp\":{\"precursorId\":\"NOR00487\"," +
                "\"precursorStructure\":\"LINEAR\"," +
                "\"precursorComposition\":{\"composition\":{\"H\":38,\"C\":20,\"N\":6,\"O\":4}}," +
                "\"precursorMonomers\":[{\"monomerIndex\":0," +
                "\"originalMonomer\":\"Leu\"," +
                "\"residueID\":\"1533\"," +
                "\"residueComposition\":{\"composition\":{\"H\":11,\"C\":6,\"N\":1,\"O\":1}}},{" +
                "\"monomerIndex\":1," +
                "\"originalMonomer\":\"NAc-Leu\"," +
                "\"residueID\":\"1894\"," +
                "\"residueComposition\":{\"composition\":{\"H\":14,\"C\":8,\"N\":1,\"O\":2}}},{" +
                "\"monomerIndex\":2," +
                "\"originalMonomer\":\"Argal\"," +
                "\"residueID\":\"65\"," +
                "\"residueComposition\":{\"composition\":{\"H\":13,\"C\":6,\"N\":4,\"O\":1}}}]," +
                "\"precursorbonds\":[[0,2],[0,1]]}," +
                "\"source\":\"software://www.expasy.org/liberator\"," +
                "\"status\":\"NORMAL\"," +
                "\"id\":{\"mostSignificantBits\":6881239458523073728," +
                "\"leastSignificantBits\":-6090640400048236173}," +
                "\"precision\":\"DOUBLE\"," +
                "\"peaks\":{\"org.expasy.mzjava_avro.core.ms.peaklist.AnnotatedDoublePeakList\":{" +
                "\"peaks\":[{" +
                "\"mz\":12.93234579876," +
                "\"i\":145.2587412563541," +
                "\"annotations\":[]},{" +
                "\"mz\":58.92698543776," +
                "\"i\":145.2587412563541," +
                "\"annotations\":[{\"ms.spectrum.NRPLibPeakAnnotation\":{" +
                "\"mergedPeakCount\":1," +
                "\"mzStd\":1.0," +
                "\"intensityStd\":5.0," +
                "\"fragAnnotation\":null}}]},{" +
                "\"mz\":152.956783276," +
                "\"i\":145.2587412563541," +
                "\"annotations\":[{\"ms.spectrum.NRPLibPeakAnnotation\":{" +
                "\"mergedPeakCount\":2," +
                "\"mzStd\":1.0," +
                "\"intensityStd\":3.0," +
                "\"fragAnnotation\":{\"ms.spectrum.NRPFragmentAnnotation\":{" +
                "\"fragment\":{" +
                "\"monomerIndexes\":[0,1,2]," +
                "\"bonds\":[[0,2],[0,1]]," +
                "\"fragmentComposition\":{" +
                "\"composition\":{\"H\":38,\"C\":20,\"N\":6,\"O\":4}}," +
                "\"precursorId\":\"NOR00487\"," +
                "\"msStage\":1}," +
                "\"charge\":1," +
                "\"chargeComposition\":{" +
                "\"composition\":{\"H\":20,\"C\":10}}," +
                "\"neutralLoss\":{\"composition\":{}}," +
                "\"modifications\":{\"composition\":{}}," +
                "\"modificationPositions\":{}}}}}]}]}}}\n");

        Set<UUID> memberIds = new HashSet<>(Arrays.asList(
                new UUID(6453290794183652199l, -7855891324758780007l),
                new UUID(- 2330906535205672779l, 3380397291928878215l),
                new UUID(- 7477072015556504347l, -4759461241505446441l),
                new UUID(8159020250014517468l, -3276528637398377287l),
                new UUID(2954576207831929l, -7401919710944613243l),
                new UUID(- 6464087018820727044l, 9158344342526998714l),
                new UUID(272555917829435557l, -502732256918155454l),
                new UUID(- 2439663895838265701l, 5432637156828437242l),
                new UUID(6906448715414564879l, -3377628232451089806l)
        ));

        NRPConsensusSpectrum spectrumRead =reader.read(in);
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00487"));

        List annotation = new ArrayList<>();
        annotation.add(new NRPLibPeakAnnotation(1,1.00,5.00));
        List annotation2 = new ArrayList<>();
        annotation2.add(new NRPLibPeakAnnotation(2,1.00,3.00,spectrumRead.getAnnotations(2).get(0).getOptFragmentAnnotation()));


        assertEquals(nrp.getNrpId(),spectrumRead.getNRP().getNrpId());
        assertEquals(nrp.getAllMonomerNode().toString(),spectrumRead.getNRP().getAllMonomerNode().toString());
        assertEquals(nrp.getAllBond().toString(),spectrumRead.getNRP().getAllBond().toString());
        assertEquals(nrp.getNrpType(),spectrumRead.getNRP().getNrpType());
        assertEquals(nrp.getComposition(),spectrumRead.getNRP().getComposition());
        assertEquals(new ArrayList<>(),spectrumRead.getAnnotations(0));
        assertEquals(annotation,spectrumRead.getAnnotations(1));
        assertEquals(annotation2,spectrumRead.getAnnotations(2));
        assertEquals(nrp.getAllMonomerNode().toString(),spectrumRead.getAnnotations(2).get(0).getOptFragmentAnnotation().get().getFragment().getAllMonomerNode().toString());
        assertEquals(nrp.getAllBond().toString(),spectrumRead.getAnnotations(2).get(0).getOptFragmentAnnotation().get().getFragment().getAllBond().toString());
        assertEquals(nrp.getAllBond().toString(),spectrumRead.getAnnotations(2).get(0).getOptFragmentAnnotation().get().getFragment().getAllBond().toString());
        assertEquals(Composition.parseComposition(""),spectrumRead.getAnnotations(2).get(0).getOptFragmentAnnotation().get().getModifications());
        assertEquals(Composition.parseComposition(""),spectrumRead.getAnnotations(2).get(0).getOptFragmentAnnotation().get().getNeutralLoss());
        assertEquals(1,spectrumRead.getAnnotations(2).get(0).getOptFragmentAnnotation().get().getCharge());
        assertEquals(new URIBuilder("www.expasy.org","liberator").build(),spectrumRead.getSpectrumSource());
        assertEquals(NRPConsensusSpectrum.Status.NORMAL,spectrumRead.getStatus());
        assertEquals(memberIds,spectrumRead.getMemberIds());
        assertEquals("name",spectrumRead.getName());
        assertEquals(3412.96,spectrumRead.getPrecursorMzMean(),0);
        assertEquals(0.01,spectrumRead.getPrecursorMzStdev(),0);
        assertEquals(0.8,spectrumRead.getSimScoreMean(),0);
        assertEquals(0.2,spectrumRead.getSimScoreStdev(),0);
        assertEquals(UUID.fromString("5f7f12d4-43ab-4cc0-ab79-b2d3647b5973"),spectrumRead.getId());
        assertEquals(2,spectrumRead.getMsLevel());
        assertEquals(1789.98765,spectrumRead.getPrecursor().getMz(),0);
        assertEquals(875.68,spectrumRead.getPrecursor().getIntensity(),0);
        assertEquals(2,spectrumRead.getPrecursor().getCharge(),0);
        assertEquals(12.93234579876,spectrumRead.getMz(0),0);
        assertEquals(58.92698543776,spectrumRead.getMz(1),0);
        assertEquals(152.956783276,spectrumRead.getMz(2),0);
        assertEquals(145.258741256354125,spectrumRead.getIntensity(0),0);
        assertEquals(145.258741256354125,spectrumRead.getIntensity(1),0);
        assertEquals(145.258741256354125,spectrumRead.getIntensity(2),0);

    }

    @Test
    public void testCreateRecordFields() throws Exception {
        String expected ="{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"NRPConsensusSpectrum\",\n" +
                "  \"namespace\" : \"ms.spectrum\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"precursor\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"Peak\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"polarity\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"enum\",\n" +
                "          \"name\" : \"Polarity\",\n" +
                "          \"namespace\" : \"org.expasy.mzjava.core.ms.peaklist\",\n" +
                "          \"symbols\" : [ \"POSITIVE\", \"NEGATIVE\", \"UNKNOWN\" ]\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"charge\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : \"int\"\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"mz\",\n" +
                "        \"type\" : \"double\"\n" +
                "      }, {\n" +
                "        \"name\" : \"intensity\",\n" +
                "        \"type\" : \"double\"\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"msLevel\",\n" +
                "    \"type\" : \"int\"\n" +
                "  }, {\n" +
                "    \"name\" : \"name\",\n" +
                "    \"type\" : \"string\"\n" +
                "  }, {\n" +
                "    \"name\" : \"simScoreMean\",\n" +
                "    \"type\" : \"double\"\n" +
                "  }, {\n" +
                "    \"name\" : \"simScoreStdev\",\n" +
                "    \"type\" : \"double\"\n" +
                "  }, {\n" +
                "    \"name\" : \"precursorMzMean\",\n" +
                "    \"type\" : \"double\"\n" +
                "  }, {\n" +
                "    \"name\" : \"precursorMzStdev\",\n" +
                "    \"type\" : \"double\"\n" +
                "  }, {\n" +
                "    \"name\" : \"memberIds\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"array\",\n" +
                "      \"items\" : {\n" +
                "        \"type\" : \"record\",\n" +
                "        \"name\" : \"UUID\",\n" +
                "        \"namespace\" : \"java.util\",\n" +
                "        \"fields\" : [ {\n" +
                "          \"name\" : \"mostSignificantBits\",\n" +
                "          \"type\" : \"long\"\n" +
                "        }, {\n" +
                "          \"name\" : \"leastSignificantBits\",\n" +
                "          \"type\" : \"long\"\n" +
                "        } ]\n" +
                "      }\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"nrp\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"NRP\",\n" +
                "      \"namespace\" : \"molecules\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"precursorId\",\n" +
                "        \"type\" : \"string\"\n" +
                "      }, {\n" +
                "        \"name\" : \"precursorStructure\",\n" +
                "        \"type\" : \"string\"\n" +
                "      }, {\n" +
                "        \"name\" : \"precursorComposition\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"record\",\n" +
                "          \"name\" : \"Composition\",\n" +
                "          \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "          \"fields\" : [ {\n" +
                "            \"name\" : \"composition\",\n" +
                "            \"type\" : {\n" +
                "              \"type\" : \"map\",\n" +
                "              \"values\" : \"int\"\n" +
                "            }\n" +
                "          } ]\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"precursorMonomers\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"Monomer\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"monomerIndex\",\n" +
                "              \"type\" : \"int\"\n" +
                "            }, {\n" +
                "              \"name\" : \"originalMonomer\",\n" +
                "              \"type\" : \"string\"\n" +
                "            }, {\n" +
                "              \"name\" : \"residueID\",\n" +
                "              \"type\" : \"string\"\n" +
                "            }, {\n" +
                "              \"name\" : \"residueComposition\",\n" +
                "              \"type\" : \"org.expasy.mzjava_avro.core.mol.Composition\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"precursorbonds\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"array\",\n" +
                "            \"items\" : \"int\"\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"source\",\n" +
                "    \"type\" : \"string\"\n" +
                "  }, {\n" +
                "    \"name\" : \"status\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"enum\",\n" +
                "      \"name\" : \"Status\",\n" +
                "      \"symbols\" : [ \"UNKNOWN\", \"SINGLETON\", \"NORMAL\", \"INQUORATE_UNCONFIRMED\", \"CONFLICTING_ID\", \"IMPURE\", \"DECOY\" ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"id\",\n" +
                "    \"type\" : \"java.util.UUID\"\n" +
                "  }, {\n" +
                "    \"name\" : \"precision\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"enum\",\n" +
                "      \"name\" : \"Precision\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava.core.ms.peaklist\",\n" +
                "      \"symbols\" : [ \"DOUBLE\", \"FLOAT\", \"DOUBLE_FLOAT\", \"DOUBLE_CONSTANT\", \"FLOAT_CONSTANT\" ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"peaks\",\n" +
                "    \"type\" : [ {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"DoublePeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"DoublePeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"i\",\n" +
                "              \"type\" : \"double\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"FloatPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"FloatPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"float\"\n" +
                "            }, {\n" +
                "              \"name\" : \"i\",\n" +
                "              \"type\" : \"float\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"DoubleFloatPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"DoubleFloatPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"i\",\n" +
                "              \"type\" : \"float\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"DoubleConstantPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"intensity\",\n" +
                "        \"type\" : \"double\"\n" +
                "      }, {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"DoubleConstantPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"FloatConstantPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"intensity\",\n" +
                "        \"type\" : \"double\"\n" +
                "      }, {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"FloatConstantPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"float\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"AnnotatedDoublePeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"AnnotatedDoublePeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"i\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"annotations\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"array\",\n" +
                "                \"items\" : [ {\n" +
                "                  \"type\" : \"record\",\n" +
                "                  \"name\" : \"NRPLibPeakAnnotation\",\n" +
                "                  \"namespace\" : \"ms.spectrum\",\n" +
                "                  \"fields\" : [ {\n" +
                "                    \"name\" : \"mergedPeakCount\",\n" +
                "                    \"type\" : \"int\"\n" +
                "                  }, {\n" +
                "                    \"name\" : \"mzStd\",\n" +
                "                    \"type\" : \"double\"\n" +
                "                  }, {\n" +
                "                    \"name\" : \"intensityStd\",\n" +
                "                    \"type\" : \"double\"\n" +
                "                  }, {\n" +
                "                    \"name\" : \"fragAnnotation\",\n" +
                "                    \"type\" : [ {\n" +
                "                      \"type\" : \"record\",\n" +
                "                      \"name\" : \"NRPFragmentAnnotation\",\n" +
                "                      \"fields\" : [ {\n" +
                "                        \"name\" : \"fragment\",\n" +
                "                        \"type\" : {\n" +
                "                          \"type\" : \"record\",\n" +
                "                          \"name\" : \"NRPFragment\",\n" +
                "                          \"namespace\" : \"molecules\",\n" +
                "                          \"fields\" : [ {\n" +
                "                            \"name\" : \"monomerIndexes\",\n" +
                "                            \"type\" : {\n" +
                "                              \"type\" : \"array\",\n" +
                "                              \"items\" : \"int\"\n" +
                "                            }\n" +
                "                          }, {\n" +
                "                            \"name\" : \"bonds\",\n" +
                "                            \"type\" : {\n" +
                "                              \"type\" : \"array\",\n" +
                "                              \"items\" : {\n" +
                "                                \"type\" : \"array\",\n" +
                "                                \"items\" : \"int\"\n" +
                "                              }\n" +
                "                            }\n" +
                "                          }, {\n" +
                "                            \"name\" : \"fragmentComposition\",\n" +
                "                            \"type\" : \"org.expasy.mzjava_avro.core.mol.Composition\"\n" +
                "                          }, {\n" +
                "                            \"name\" : \"precursorId\",\n" +
                "                            \"type\" : \"string\"\n" +
                "                          }, {\n" +
                "                            \"name\" : \"msStage\",\n" +
                "                            \"type\" : \"int\"\n" +
                "                          } ]\n" +
                "                        }\n" +
                "                      }, {\n" +
                "                        \"name\" : \"charge\",\n" +
                "                        \"type\" : \"int\"\n" +
                "                      }, {\n" +
                "                        \"name\" : \"chargeComposition\",\n" +
                "                        \"type\" : \"org.expasy.mzjava_avro.core.mol.Composition\"\n" +
                "                      }, {\n" +
                "                        \"name\" : \"neutralLoss\",\n" +
                "                        \"type\" : \"org.expasy.mzjava_avro.core.mol.Composition\"\n" +
                "                      }, {\n" +
                "                        \"name\" : \"modifications\",\n" +
                "                        \"type\" : \"org.expasy.mzjava_avro.core.mol.Composition\"\n" +
                "                      }, {\n" +
                "                        \"name\" : \"modificationPositions\",\n" +
                "                        \"type\" : {\n" +
                "                          \"type\" : \"map\",\n" +
                "                          \"values\" : {\n" +
                "                            \"type\" : \"array\",\n" +
                "                            \"items\" : {\n" +
                "                              \"type\" : \"record\",\n" +
                "                              \"name\" : \"Modification\",\n" +
                "                              \"namespace\" : \"molecules.modification\",\n" +
                "                              \"fields\" : [ {\n" +
                "                                \"name\" : \"modificationComposition\",\n" +
                "                                \"type\" : \"org.expasy.mzjava_avro.core.mol.Composition\"\n" +
                "                              }, {\n" +
                "                                \"name\" : \"modificationType\",\n" +
                "                                \"type\" : \"string\"\n" +
                "                              } ]\n" +
                "                            }\n" +
                "                          }\n" +
                "                        }\n" +
                "                      } ]\n" +
                "                    }, \"null\" ]\n" +
                "                  } ]\n" +
                "                } ]\n" +
                "              }\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"AnnotatedFloatPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"AnnotatedFloatPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"float\"\n" +
                "            }, {\n" +
                "              \"name\" : \"i\",\n" +
                "              \"type\" : \"float\"\n" +
                "            }, {\n" +
                "              \"name\" : \"annotations\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"array\",\n" +
                "                \"items\" : [ \"ms.spectrum.NRPLibPeakAnnotation\" ]\n" +
                "              }\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"AnnotatedDoubleFloatPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"AnnotatedDoubleFloatPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"i\",\n" +
                "              \"type\" : \"float\"\n" +
                "            }, {\n" +
                "              \"name\" : \"annotations\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"array\",\n" +
                "                \"items\" : [ \"ms.spectrum.NRPLibPeakAnnotation\" ]\n" +
                "              }\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"AnnotatedDoubleConstantPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"intensity\",\n" +
                "        \"type\" : \"double\"\n" +
                "      }, {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"AnnotatedDoubleConstantPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"annotations\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"array\",\n" +
                "                \"items\" : [ \"ms.spectrum.NRPLibPeakAnnotation\" ]\n" +
                "              }\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"AnnotatedFloatConstantPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"intensity\",\n" +
                "        \"type\" : \"double\"\n" +
                "      }, {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"AnnotatedFloatConstantPeak\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"float\"\n" +
                "            }, {\n" +
                "              \"name\" : \"annotations\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"array\",\n" +
                "                \"items\" : [ \"ms.spectrum.NRPLibPeakAnnotation\" ]\n" +
                "              }\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    } ]\n" +
                "  } ]\n" +
                "}";
        assertEquals(new Schema.Parser().parse(expected).toString(true),new NRPConsensusSpectrumReader(com.google.common.base.Optional.<PeakList.Precision>absent(),
                Collections.<PeakProcessor<NRPLibPeakAnnotation, NRPLibPeakAnnotation>>emptyList(),new NRPLibPeakAnnotationReader()).createSchema().toString(true));

    }
}