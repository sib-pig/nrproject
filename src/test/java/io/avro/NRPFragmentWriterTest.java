package io.avro;

import molecules.NRP;
import molecules.NRPBuilder;
import molecules.NRPFragment;
import molecules.norine.NorineNRPStore;
import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.apache.avro.io.EncoderFactory;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.junit.Test;

import java.io.StringWriter;

import static org.junit.Assert.*;

/**
 * Created by ericart on 14.07.2016.
 */
public class NRPFragmentWriterTest {

    @Test
    public void testWrite() throws Exception {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00487"));
        NRPFragment nrpFragment = new NRPFragment(nrp.cloneSimpleGraph(),nrp.getComposition(),nrp.getNrpId(),1);

        StringWriter out = new StringWriter();
        JsonGenerator gen = new JsonFactory().createJsonGenerator(out);
        NRPFragmentWriter writer = new NRPFragmentWriter();
        Encoder encoder = EncoderFactory.get().jsonEncoder(writer.createSchema(), gen);
        writer.write(nrpFragment, encoder);
        encoder.flush();


        assertEquals("{\"monomerIndexes\":[0,1,2]," +
                "\"bonds\":[[0,2],[0,1]]," +
                "\"fragmentComposition\":{\"composition\":{\"H\":38,\"C\":20,\"N\":6,\"O\":4}}," +
                "\"precursorId\":\"NOR00487\"," +
                "\"msStage\":1}", out.toString());



    }

    @Test
    public void testCreateRecordFields() throws Exception {

        String expected="{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"NRPFragment\",\n" +
                "  \"namespace\" : \"molecules\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"monomerIndexes\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"array\",\n" +
                "      \"items\" : \"int\"\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"bonds\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"array\",\n" +
                "      \"items\" : {\n" +
                "        \"type\" : \"array\",\n" +
                "        \"items\" : \"int\"\n" +
                "      }\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"fragmentComposition\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"Composition\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"composition\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"map\",\n" +
                "          \"values\" : \"int\"\n" +
                "        }\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"precursorId\",\n" +
                "    \"type\" : \"string\"\n" +
                "  }, {\n" +
                "    \"name\" : \"msStage\",\n" +
                "    \"type\" : \"int\"\n" +
                "  } ]\n" +
                "}";

        assertEquals(new Schema.Parser().parse(expected).toString(true),new NRPFragmentWriter().createSchema().toString(true));


    }
}