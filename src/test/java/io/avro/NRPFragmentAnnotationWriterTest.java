package io.avro;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import molecules.Monomer;
import molecules.NRP;
import molecules.NRPBuilder;
import molecules.NRPFragment;
import molecules.modification.Modification;
import molecules.norine.NorineNRPStore;
import ms.spectrum.NRPFragmentAnnotation;
import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.apache.avro.io.EncoderFactory;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.expasy.mzjava.core.mol.AtomicSymbol;
import org.expasy.mzjava.core.mol.Composition;
import org.junit.Test;

import java.io.StringWriter;

import static org.junit.Assert.*;

/**
 * Created by ericart on 13.07.2016.
 */
public class NRPFragmentAnnotationWriterTest {

    @Test
    public void testWrite() throws Exception {

        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00487"));
        NRPFragment nrpFragment = new NRPFragment(nrp.cloneSimpleGraph(),nrp.getComposition(),nrp.getNrpId(),1);

        Monomer modifiedMonomer1=null;
        for (Monomer monomer : nrpFragment.getAllMonomerNode()){
            if (monomer.getIndex()==0){
                modifiedMonomer1=monomer;
            }
        }

        Composition proton = new Composition.Builder().add(AtomicSymbol.H).charge(1).build();
        Composition phosphorylation = Composition.parseComposition("HPO3");
        Composition neutralLoss = Composition.parseComposition("H2O");
        ListMultimap<Monomer, Modification> monomersModifications = ArrayListMultimap.create();
        monomersModifications.put(modifiedMonomer1,new Modification(phosphorylation, Modification.ModificationType.FIX));

        NRPFragmentAnnotation nrpFragmentAnnotation = new NRPFragmentAnnotation(1, nrpFragment,monomersModifications,proton, neutralLoss);
        NRPFragmentAnnotationWriter writer = new NRPFragmentAnnotationWriter();

        StringWriter out = new StringWriter();
        JsonGenerator gen = new JsonFactory().createJsonGenerator(out);
        Encoder encoder = EncoderFactory.get().jsonEncoder(writer.createSchema(), gen);
        writer.write(nrpFragmentAnnotation, encoder);
        encoder.flush();

        assertEquals("{\"fragment\":{\"monomerIndexes\":[0,1,2]," +
                "\"bonds\":[[0,2],[0,1]]," +
                "\"fragmentComposition\":{" +
                "\"composition\":{\"H\":38,\"C\":20,\"N\":6,\"O\":4}}," +
                "\"precursorId\":\"NOR00487\",\"msStage\":1}," +
                "\"charge\":1,\"chargeComposition\":{\"composition\":{\"H\":1}}," +
                "\"neutralLoss\":{\"composition\":{\"H\":2,\"O\":1}},\"modifications\":{" +
                "\"composition\":{\"H\":1,\"O\":3,\"P\":1}}," +
                "\"modificationPositions\":{" +
                "\"0\":[{\"modificationComposition\":{\"composition\":{\"H\":1,\"O\":3,\"P\":1}},\"modificationType\":\"FIX\"}]}}",out.toString());

    }

    @Test
    public void testCreateRecordFields() throws Exception {


        String expected = "{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"NRPFragmentAnnotation\",\n" +
                "  \"namespace\" : \"ms.spectrum\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"fragment\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"NRPFragment\",\n" +
                "      \"namespace\" : \"molecules\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"monomerIndexes\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : \"int\"\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"bonds\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"array\",\n" +
                "            \"items\" : \"int\"\n" +
                "          }\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"fragmentComposition\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"record\",\n" +
                "          \"name\" : \"Composition\",\n" +
                "          \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "          \"fields\" : [ {\n" +
                "            \"name\" : \"composition\",\n" +
                "            \"type\" : {\n" +
                "              \"type\" : \"map\",\n" +
                "              \"values\" : \"int\"\n" +
                "            }\n" +
                "          } ]\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"precursorId\",\n" +
                "        \"type\" : \"string\"\n" +
                "      }, {\n" +
                "        \"name\" : \"msStage\",\n" +
                "        \"type\" : \"int\"\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"charge\",\n" +
                "    \"type\" : \"int\"\n" +
                "  }, {\n" +
                "    \"name\" : \"chargeComposition\",\n" +
                "    \"type\" : \"org.expasy.mzjava_avro.core.mol.Composition\"\n" +
                "  }, {\n" +
                "    \"name\" : \"neutralLoss\",\n" +
                "    \"type\" : \"org.expasy.mzjava_avro.core.mol.Composition\"\n" +
                "  }, {\n" +
                "    \"name\" : \"modifications\",\n" +
                "    \"type\" : \"org.expasy.mzjava_avro.core.mol.Composition\"\n" +
                "  }, {\n" +
                "    \"name\" : \"modificationPositions\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"map\",\n" +
                "      \"values\" : {\n" +
                "        \"type\" : \"array\",\n" +
                "        \"items\" : {\n" +
                "          \"type\" : \"record\",\n" +
                "          \"name\" : \"Modification\",\n" +
                "          \"namespace\" : \"molecules.modification\",\n" +
                "          \"fields\" : [ {\n" +
                "            \"name\" : \"modificationComposition\",\n" +
                "            \"type\" : \"org.expasy.mzjava_avro.core.mol.Composition\"\n" +
                "          }, {\n" +
                "            \"name\" : \"modificationType\",\n" +
                "            \"type\" : \"string\"\n" +
                "          } ]\n" +
                "        }\n" +
                "      }\n" +
                "    }\n" +
                "  } ]\n" +
                "}\n";


        assertEquals(new Schema.Parser().parse(expected).toString(true),new NRPFragmentAnnotationWriter().createSchema().toString(true));


    }
}