package io.avro;

import molecules.modification.Modification;
import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;
import org.expasy.mzjava.core.mol.Composition;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ericart on 12.07.2016.
 */
public class ModificationReaderTest {

    @Test
    public void testRead() throws Exception {
        ModificationReader reader = new ModificationReader();
        Decoder in = DecoderFactory.get().jsonDecoder(reader.createSchema(),
                        "{" +
                        "\"modificationComposition\":" +
                        "{\"composition\":{\"H\":2}}," +
                        "\"modificationType\":\"FIX\"" +
                        "}");
        Assert.assertEquals(new Modification(Composition.parseComposition("H2"), molecules.modification.Modification.ModificationType.FIX), reader.read(in));

    }

    @Test
    public void testCreateRecordFields() throws Exception {
        String expected ="{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"Modification\",\n" +
                "  \"namespace\" : \"molecules.modification\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"modificationComposition\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"Composition\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"composition\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"map\",\n" +
                "          \"values\" : \"int\"\n" +
                "        }\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"modificationType\",\n" +
                "    \"type\" : \"string\"\n" +
                "  } ]\n" +
                "}\n";

        assertEquals(new Schema.Parser().parse(expected).toString(true),new ModificationReader().createSchema().toString(true));

    }
}