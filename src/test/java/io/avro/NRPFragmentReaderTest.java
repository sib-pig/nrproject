package io.avro;

import molecules.*;
import molecules.norine.NorineNRPStore;
import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;
import org.expasy.mzjava.core.mol.Composition;
import org.jgrapht.graph.SimpleGraph;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ericart on 14.07.2016.
 */
public class NRPFragmentReaderTest {

    @Test
    public void testRead() throws Exception {

        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp1 = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00487"));

        NRPFragmentReader reader = new NRPFragmentReader();
        reader.setNRP(nrp1);

        Decoder in = DecoderFactory.get().jsonDecoder(reader.createSchema(),
                "{\"monomerIndexes\":[0,1]," +
                        "\"bonds\":[[0,1]]," +
                        "\"fragmentComposition\":{" +
                        "\"composition\":{\"H\":10,\"C\":20}}," +
                        "\"precursorId\":\"NOR00487\"," +
                        "\"msStage\":1}");


        SimpleGraph<Monomer, Bond> simpleGraph = nrp1.cloneSimpleGraph();
        for (Monomer monomer : nrp1.getAllMonomerNode()){
            if (monomer.getIndex()==2){
                simpleGraph.removeVertex(monomer);
            }
        }
        for (Bond bond : nrp1.getAllBond()){
            if (bond.getMonomerNodeSource().getIndex()==2 || bond.getMonomerNodeSource().getIndex()==2 ){
                simpleGraph.removeEdge(bond);
            }
        }
        NRPFragment nrpFragment = new NRPFragment(simpleGraph, Composition.parseComposition("C20H10"),"NOR00487",1);

        NRPFragment nrpFragmentRead = reader.read(in);

        assertEquals(nrpFragment.getSimpleGraph(),nrpFragmentRead.getSimpleGraph());
        assertEquals(nrpFragment.getComposition(),nrpFragmentRead.getComposition());

    }

    @Test
    public void testCreateRecordFields() throws Exception {
        String expected="{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"NRPFragment\",\n" +
                "  \"namespace\" : \"molecules\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"monomerIndexes\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"array\",\n" +
                "      \"items\" : \"int\"\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"bonds\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"array\",\n" +
                "      \"items\" : {\n" +
                "        \"type\" : \"array\",\n" +
                "        \"items\" : \"int\"\n" +
                "      }\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"fragmentComposition\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"Composition\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"composition\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"map\",\n" +
                "          \"values\" : \"int\"\n" +
                "        }\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"precursorId\",\n" +
                "    \"type\" : \"string\"\n" +
                "  }, {\n" +
                "    \"name\" : \"msStage\",\n" +
                "    \"type\" : \"int\"\n" +
                "  } ]\n" +
                "}";

        assertEquals(new Schema.Parser().parse(expected).toString(true),new NRPFragmentReader().createSchema().toString(true));

    }

    @Test
    public void testSetNRP() throws Exception {

    }
}