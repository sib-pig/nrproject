package io.avro;

import molecules.modification.Modification;
import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.apache.avro.io.EncoderFactory;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;

import org.expasy.mzjava.core.mol.Composition;
import org.junit.Test;

import java.io.StringWriter;

import static org.junit.Assert.*;

/**
 * Created by ericart on 12.07.2016.
 */
public class ModificationWriterTest {

    @Test
    public void testWrite() throws Exception {


        Modification modification = new Modification(Composition.parseComposition("H2"), Modification.ModificationType.FIX);
        StringWriter out = new StringWriter();
        JsonGenerator gen = new JsonFactory().createJsonGenerator(out);

        ModificationWriter writer = new ModificationWriter();
        Encoder encoder = EncoderFactory.get().jsonEncoder(writer.createSchema(), gen);
        writer.write(modification, encoder);
        encoder.flush();

        assertEquals("{\"modificationComposition\":{\"composition\":{\"H\":2}},\"modificationType\":\"FIX\"}", out.toString());

    }


    @Test
    public void testCreateRecordFields() throws Exception {

        String expected ="{\n" +
        "  \"type\" : \"record\",\n" +
        "  \"name\" : \"Modification\",\n" +
        "  \"namespace\" : \"molecules.modification\",\n" +
        "  \"fields\" : [ {\n" +
        "    \"name\" : \"modificationComposition\",\n" +
        "    \"type\" : {\n" +
        "      \"type\" : \"record\",\n" +
        "      \"name\" : \"Composition\",\n" +
        "      \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
        "      \"fields\" : [ {\n" +
        "        \"name\" : \"composition\",\n" +
        "        \"type\" : {\n" +
        "          \"type\" : \"map\",\n" +
        "          \"values\" : \"int\"\n" +
        "        }\n" +
        "      } ]\n" +
        "    }\n" +
        "  }, {\n" +
        "    \"name\" : \"modificationType\",\n" +
        "    \"type\" : \"string\"\n" +
        "  } ]\n" +
        "}\n";

        assertEquals(new Schema.Parser().parse(expected).toString(true),new ModificationWriter().createSchema().toString(true));
    }
}