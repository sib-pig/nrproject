package serializers;

import com.fasterxml.jackson.databind.ObjectMapper;
import molecules.norine.NorineMonomer;
import molecules.norine.NorineMonomerStore;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ericart on 12.03.2016.
 */
public class NorineMonomerSerializerTest {

    @Test
    public void testSerialize() throws Exception {
        ObjectMapper mapper = new ObjectMapper();

        NorineMonomer norineMonomer = NorineMonomerStore.getInstance().getMonomer("Asp");
        String monomerString=mapper.writeValueAsString(norineMonomer);
        //System.out.println(monomerString);

        //Test monomerID
        String monomerId = monomerString.split("\"code\":\"|\",\"nameAA")[1];
        assertEquals("Asp",monomerId);


        //Test monomerName
        String monomerName = monomerString.split("nameAA\":\"|\",\"smiles\":")[1];
        assertEquals("Aspartic Acid",monomerName);

        //Test smiles
        String monomerSmiles=monomerString.split("\"smiles\":\"")[1];
        monomerSmiles =monomerSmiles.replace("\"}","");
        assertEquals("C(C(C(=O)O)N)C(=O)O",monomerSmiles);
    }
}