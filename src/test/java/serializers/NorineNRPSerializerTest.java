package serializers;

import com.fasterxml.jackson.databind.ObjectMapper;
import molecules.norine.NorineNRP;
import molecules.norine.NorineNRPStore;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.*;

/**
 * Created by ericart on 02.03.2016.
 */
public class NorineNRPSerializerTest {

    @Test
    public void testSerialize() throws Exception {

        ObjectMapper mapper = new ObjectMapper();
        NorineNRP nrp = NorineNRPStore.getInstance().getNRP("NOR00137");
        String nrpString=mapper.writeValueAsString(nrp);

        //Test norineID
        String nrpId = nrpString.split("\"id\":\"|\",\"graph\"")[1];

        assertEquals("NOR00137",nrpId);

        //Test graph Edges
        String nrpStrEdges =(nrpString.split("]],\"V\":\\[|\"E\":\\[\\["))[1];

        String expectedStrEdges = "[0,1],[0,6],[1,2],[2,3],[3,4],[4,5],[5,6]";
        expectedStrEdges = expectedStrEdges.substring(1).substring(0,expectedStrEdges.length()-2);

        Set<Set<Integer>> expectedEdges = string2set(expectedStrEdges);
        Set<Set<Integer>> nrpEdges = string2set(nrpStrEdges);

        assertEquals(expectedEdges, nrpEdges);

        //Test graph Vertexes
        String nrpVertexes=nrpString.split("\"V\":|},\"name\":")[1];
        String expectedVertexes = "[\"D-Ala\",\"Leu\",\"D-Asp\",\"Har\",\"ADMAdda\",\"D-Glu\",\"NMe-Dha\"]";

        assertEquals(expectedVertexes,nrpVertexes);

        //Test name
        String nrpName=nrpString.split("\"name\":\"|\",\"smiles\":")[1];
        String expectedName = "[D-Asp3.ADMAdda5]microcystin-LHar";

        assertEquals(expectedName,nrpName);

        //Test Smiles
        String nrpSmiles=nrpString.split("\"smiles\":\"")[1];
        nrpSmiles =nrpSmiles.replace("\"}","");
        String expectedSmiles = "CC(C)CC2NC(=O)C(C)NC(=O)C(=C)N(C)C(=O)CCC(NC(=O)C(C)C(C=CC(C)=CC(C)C(Cc1ccccc1)OC(C)=O)NC(=O)C(CCCCN=C(N)N)NC(=O)CC(NC2(=O))C(=O)O)C(=O)O";

        assertEquals(expectedSmiles,nrpSmiles);

    }



    @Test
    public void testSerialize2() throws Exception {

        ObjectMapper mapper = new ObjectMapper();
        NorineNRP nrp = NorineNRPStore.getInstance().getNRP("NOR00689");
        String nrpString=mapper.writeValueAsString(nrp);

        //Test norineID
        String nrpId = nrpString.split("\"id\":\"|\",\"graph\"")[1];

        assertEquals("NOR00689",nrpId);

        //Test graph Edges
        String nrpStrEdges =(nrpString.split("]],\"V\":\\[|\"E\":\\[\\["))[1];

        String expectedStrEdges = "[0,1],[0,3],[1,2],[1,3],[3,4],[3,7],[4,5],[4,6],[4,7],[6,7]";
        expectedStrEdges = expectedStrEdges.substring(1).substring(0,expectedStrEdges.length()-2);

        Set<Set<Integer>> expectedEdges = string2set(expectedStrEdges);
        Set<Set<Integer>> nrpEdges = string2set(nrpStrEdges);

        assertEquals(expectedEdges, nrpEdges);

        //Test graph Vertexes
        String nrpVertexes=nrpString.split("\"V\":|},\"name\":")[1];
        String expectedVertexes = "[\"Asn\",\"bOH-Cl-Tyr\",\"NMe-Leu\",\"Hpg\",\"bOH-Cl-Tyr\",\"Ere\",\"Dhpg\",\"Hpg\"]";

        assertEquals(expectedVertexes,nrpVertexes);

        //Test name
        String nrpName=nrpString.split("\"name\":\"|\",\"smiles\":")[1];
        String expectedName = "chloroorienticin C";

        assertEquals(expectedName,nrpName);

        //Test Smiles
        String nrpSmiles=nrpString.split("\"smiles\":\"")[1];
        nrpSmiles =nrpSmiles.replace("\"}","");
        String expectedSmiles = "CNC(CC(C)C)C(=O)NC2C(=O)NC(CC(N)=O)C(=O)NC6c3cc(Oc1ccc(cc1Cl)C2(O))c(O)c(c3)Oc4ccc(cc4Cl)C(OC5CC(C)(N)C(O)C(C)O5)C8NC(=O)C(NC6(=O))c7ccc(O)c(c7)c9c(O)cc(O)cc9(C(NC8(=O))C(=O)O)";

        assertEquals(expectedSmiles,nrpSmiles);

    }

    private Set<Set<Integer>> string2set(String setString){
        Set<Set<Integer>> d = new HashSet<>();
        String[] items2 = setString.split("\\],\\[");
        for (String i : items2){
            Set<Integer> integerSet = new HashSet<>();
            String[] items3 =i.split(",");
            for (String item : items3){
                integerSet.add(Integer.parseInt(item));
            }
            d.add(integerSet);
        }
        return d;
    }
}