package molecules;


import molecules.norine.NorineMonomerStore;
import molecules.norine.NorineNRPStore;
import molecules.norine.NorineResidue;
import molecules.norine.NorineResidueStore;
import org.junit.Test;
import org.openscience.cdk.exception.CDKException;

import java.io.IOException;
import static org.junit.Assert.*;

/**
 * Created by ericart on 11.11.2015.
 */

public class BondTest {


    @Test
    public void testContructor() throws IOException, CDKException {
        NorineMonomerStore norineMonomerStore = NorineMonomerStore.getInstance();
        NorineResidue residue=NorineResidueStore.getInstance().getAllResidues().get(0);
        Monomer nodebLys = new Monomer(norineMonomerStore.getMonomer(residue.getMono()), residue,1);
        Monomer nodebAla = new Monomer(norineMonomerStore.getMonomer(residue.getMono()), residue,1);
        Bond bond =  new Bond( nodebLys, nodebAla, Bond.BondType.AMINO);
        assertNotNull(bond);
    }

    @Test
    public void testGetMonomerNodeSource() throws IOException, CDKException {

        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(NorineNRPStore.getInstance().getNRP("NOR00209"));

        Monomer alaMonomer=null;
        Monomer acaMonomer=null;
        for (Monomer monomer:nrp.getAllMonomerNode()){
            if (monomer.getMonomerSymbol().equals("Ala")){
               alaMonomer=monomer;
            }else if (monomer.getMonomerSymbol().equals("Aca")){
                acaMonomer=monomer;
            }
        }
        Bond bond =  new Bond( alaMonomer, acaMonomer, Bond.BondType.AMINO);
        assertEquals("Ala", bond.getMonomerNodeSource().getMonomerSymbol());
    }

    @Test
       public void testGetMonomerNodeTarget() throws IOException, CDKException {
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(NorineNRPStore.getInstance().getNRP("NOR00209"));

        Monomer alaMonomer=null;
        Monomer acaMonomer=null;
        for (Monomer monomer:nrp.getAllMonomerNode()){
            if (monomer.getMonomerSymbol().equals("Ala")){
                alaMonomer=monomer;
            }else if (monomer.getMonomerSymbol().equals("Aca")){
                acaMonomer=monomer;
            }
        }
        Bond bond =  new Bond( alaMonomer, acaMonomer, Bond.BondType.AMINO);
        assertEquals("Aca", bond.getMonomerNodeTarget().getMonomerSymbol());

    }

    @Test
    public void testGetBondType() throws IOException, CDKException {
        NorineMonomerStore norineMonomerStore = NorineMonomerStore.getInstance();
        NorineResidue residue=NorineResidueStore.getInstance().getAllResidues().get(0);
        Monomer nodebLys = new Monomer(norineMonomerStore.getMonomer(residue.getMono()), residue,1);
        Monomer nodebAla = new Monomer(norineMonomerStore.getMonomer(residue.getMono()), residue,1);
        Bond bond =  new Bond( nodebLys, nodebAla, Bond.BondType.AMINO);
        assertEquals(Bond.BondType.AMINO,bond.getBondType());

    }

    @Test
    public void testToString() throws IOException, CDKException {
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(NorineNRPStore.getInstance().getNRP("NOR00209"));

        Monomer alaMonomer=null;
        Monomer acaMonomer=null;
        for (Monomer monomer:nrp.getAllMonomerNode()){
            if (monomer.getMonomerSymbol().equals("Ala")){
                alaMonomer=monomer;
            }else if (monomer.getMonomerSymbol().equals("Aca")){
                acaMonomer=monomer;
            }
        }
        Bond bond =  new Bond( alaMonomer, acaMonomer, Bond.BondType.AMINO);
        assertEquals("(Ala : Aca , AMINO)", bond.toString());
    }


}
