package molecules;


import molecules.Bond;
import molecules.Monomer;
import molecules.NRP;
import molecules.NRPBuilder;
import molecules.norine.NorineNRPStore;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.proteomics.io.ms.ident.mzidentml.v110.ObjectFactory;
import org.jgrapht.graph.SimpleGraph;
import org.junit.Test;
import org.openscience.cdk.exception.CDKException;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Created by ericart on 15.12.2015.
 */
public class NRPTest {

    @Test
    public void testGetNrpId() throws Exception {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00228"));
        assertEquals("NOR00228", nrp.getNrpId());
    }

    @Test
    public void testCloneSimpleGraph() throws Exception {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00228"));
        SimpleGraph<Monomer, Bond> SimpleGraph = nrp.cloneSimpleGraph();
        assertEquals(11, SimpleGraph.vertexSet().size());
        assertEquals(12,SimpleGraph.edgeSet().size());


    }

    @Test
    public void testGetComposition() throws Exception {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00137"));
        Composition composition = nrp.getComposition();
        assertEquals("C50H74N10O13", composition.getFormula());
    }


    @Test
    public void testGetAllMonomerNode() throws Exception {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00228"));
        Set<Monomer> setMonomers = nrp.getAllMonomerNode();

        boolean containsChrAct = false;
        boolean containsAla = false;
        for (Monomer monomer : setMonomers){
            if (monomer.getMonomerSymbol().equals("ChrAct")){
                containsChrAct = true;
            }
            if (monomer.getMonomerSymbol().equals("Ala")){
                containsAla = true;
            }
        }

        assertFalse(setMonomers.isEmpty());
        assertEquals(nrp.cloneSimpleGraph().vertexSet().size(), setMonomers.size());
        assertTrue(containsChrAct);
        assertFalse(containsAla);

    }

    @Test
    public void testGetAllBond() throws Exception {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00228"));
        Set<Bond> setBonds = nrp.getAllBond();

        boolean containsChrActThr = false;
        boolean containsChrActAla = false;

        for (Bond bond : setBonds){
            if (bond.getMonomerNodeSource().getMonomerSymbol().equals("ChrAct") && bond.getMonomerNodeTarget().getMonomerSymbol().equals("Thr")){
                containsChrActThr = true;
            }
            if (bond.getMonomerNodeSource().getMonomerSymbol().equals("ChrAct") && bond.getMonomerNodeTarget().getMonomerSymbol().equals("Ala")){
                containsChrActAla = true;
            }
        }

        assertFalse(setBonds.isEmpty());
        assertEquals(nrp.cloneSimpleGraph().edgeSet().size(),setBonds.size());
        assertTrue(containsChrActThr);
        assertFalse(containsChrActAla);

    }

    @Test
    public void testGetBondsSubset() throws Exception {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00727"));
        Set <Monomer> subsetMonomers = new HashSet<>();
        for(Monomer monomer : nrp.getAllMonomerNode()){
            if (monomer.getMonomerSymbol().equals("bPhe") || monomer.getMonomerSymbol().equals("D-Val")){
                subsetMonomers.add(monomer);
            }
        }

        assertEquals(1, nrp.getBondsSubset(subsetMonomers).size());
    }

    @Test
    public void testIsEndMolecule() throws Exception {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00228"));
        assertFalse(nrp.isEndMolecule());
    }


    @Test
    public void testEquals1() throws Exception {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp1 = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00669"));
        NRP nrp2 = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00669"));
        assertFalse(nrp1.equals(nrp2));
        assertFalse(nrp1.hashCode() == nrp2.hashCode());

    }
    @Test
    public void testEquals2() throws Exception {

        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp1 = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00669"));
        NRP nrp2 = new NRP(nrp1.cloneSimpleGraph(), nrp1.getComposition(), nrp1.getNrpId(),nrp1.getNrpType());
        assertEquals(nrp1.getSimpleGraph(), nrp2.getSimpleGraph());
        assertEquals(nrp1.getComposition(), nrp2.getComposition());

        assertEquals(nrp1.getSimpleGraph().hashCode(), nrp2.getSimpleGraph().hashCode());
        assertEquals(nrp1.getComposition().hashCode(), nrp2.getComposition().hashCode());

    }


    @Test
    public void testToString() throws CDKException {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp1 = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00220"));
        assertEquals("[C14:0-NH2(3), Gln, Asn, Asn, Tyr, Ser, Asn, D-Pro]", nrp1.toString());

    }





}