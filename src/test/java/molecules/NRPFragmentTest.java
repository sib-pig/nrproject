package molecules;

import molecules.*;
import molecules.norine.NorineNRPStore;
import org.jgrapht.graph.SimpleGraph;
import org.junit.Test;

import static org.junit.Assert.*;



/**
 * Created by ericart on 03.02.2016.
 */
public class NRPFragmentTest {

    @Test
    public void testGetPrecursorId() throws Exception {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp1 = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00727"));
        SimpleGraph<Monomer, Bond> clonedGraph= nrp1.cloneSimpleGraph();
        Bond bond= (Bond) nrp1.getAllBond().toArray()[1];
        clonedGraph.removeEdge(bond);
        NRPFragment monomericNrp2 = new NRPFragment(clonedGraph, nrp1.getComposition(), nrp1.getNrpId(),0);
        assertEquals("NOR00727", monomericNrp2.getPrecursorId());
    }
    @Test
    public void testEquals() throws Exception {

        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp1 = nrpBuilder.createNRP(norineNRPStore.getNRP("NOR00727"));

        SimpleGraph<Monomer,Bond> clonedgraph= nrp1.cloneSimpleGraph();
        Bond bond= (Bond) nrp1.getAllBond().toArray()[1];
        Monomer monomer = (Monomer) nrp1.getAllMonomerNode().toArray()[1];
        clonedgraph.removeEdge(bond);
        clonedgraph.removeVertex(monomer);

        NRPFragment monomericNrp2 = new NRPFragment(clonedgraph, nrp1.getComposition(), nrp1.getNrpId(),0);

        assertNotSame(nrp1.getSimpleGraph(),monomericNrp2.getSimpleGraph());
        assertNotSame(nrp1.getSimpleGraph().hashCode(), monomericNrp2.getSimpleGraph().hashCode());


        NRPFragment monomericNrp3 = new NRPFragment(clonedgraph, nrp1.getComposition(), nrp1.getNrpId(),1);

        assertEquals(monomericNrp2.getSimpleGraph(),monomericNrp3.getSimpleGraph());
        assertEquals(monomericNrp2.getSimpleGraph().hashCode(), monomericNrp3.getSimpleGraph().hashCode());

        assertEquals(monomericNrp2.getComposition(),monomericNrp3.getComposition());
        assertEquals(monomericNrp2.getComposition().hashCode(), monomericNrp3.getComposition().hashCode());
    }

}