package molecules;

import molecules.norine.NorineNRP;
import molecules.norine.NorineNRPStore;
import org.expasy.mzjava.core.mol.Composition;
import org.junit.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Created by ericart on 15.12.2015.
 */
public class NRPBuilderTest {

    @Test
    public void testCreateNRP() throws Exception {

        NRPBuilder nrpBuilder = new NRPBuilder();
        NRP nrp = nrpBuilder.createNRP(NorineNRPStore.getInstance().getNRP("NOR00638"));

        Map<String,Set<Composition>> monomersResidues= new HashMap<>();
        Map<String,Set<Integer>> monomersEdges= new HashMap<>();

        for(Monomer monomer : nrp.getAllMonomerNode()){

            if (monomersEdges.containsKey(monomer.getMonomerSymbol())) {

                monomersEdges.get(monomer.getMonomerSymbol()).add(nrp.cloneSimpleGraph().degreeOf(monomer));
                monomersResidues.get(monomer.getMonomerSymbol()).add(monomer.getNorineResidue().getComposition());

            }else{

                Set<Integer> edges = new HashSet<>();
                edges.add(nrp.cloneSimpleGraph().degreeOf(monomer));
                monomersEdges.put(monomer.getMonomerSymbol(), edges);

                Set<Composition> compositions = new HashSet<>();
                compositions.add(monomer.getNorineResidue().getComposition());
                monomersResidues.put(monomer.getMonomerSymbol(),compositions);

            }

        }

        assertTrue(monomersEdges.get("bLys").contains(1));
        assertTrue(monomersEdges.get("Dpr").contains(3));//first instance of the monomer has 3 edges
        assertTrue(monomersEdges.get("bU-dAla").contains(2));
        assertTrue(monomersEdges.get("Cap").contains(2));
        assertTrue(monomersEdges.get("Dpr").contains(2));//first instance of the monomer has 2 edges
        assertTrue(monomersEdges.get("Ser").contains(2));

        assertTrue(monomersResidues.get("bLys").contains(Composition.parseComposition("C6H13N2O")));
        assertTrue(monomersResidues.get("Dpr").contains(Composition.parseComposition("C3H6N2O")));//first instance of the monomer has this residue
        assertTrue(monomersResidues.get("bU-dAla").contains(Composition.parseComposition("C4H5N3O2")));
        assertTrue(monomersResidues.get("Cap").contains(Composition.parseComposition("C6H10N4O")));
        assertTrue(monomersResidues.get("Dpr").contains(Composition.parseComposition("C3H5N2O")));//second instance of the monomer has different residue
        assertTrue(monomersResidues.get("Ser").contains(Composition.parseComposition("C3H5NO2")));

        assertEquals(6, nrp.getAllMonomerNode().size());
        assertEquals(5,monomersEdges.size());
        assertNotNull(nrp.getComposition());


    }

    @Test(expected = NullPointerException.class)
    public void testCreateNRP2() throws Exception {
        NorineNRP nonSmilesNRP = NorineNRPStore.getInstance().getNRP("NOR00774");
        NRPBuilder nrpBuilder = new NRPBuilder();
        nrpBuilder.createNRP(nonSmilesNRP);
    }
}