package molecules;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ericart on 04.04.2016.
 */
public class NRPTypeTest {
    @Test
    public void testNRPType() throws Exception {
        NRPType nrpPartialCyclic = NRPType.valueOf("partial cyclic".replaceAll(" ","_").toUpperCase());
        assertEquals(NRPType.PARTIAL_CYCLIC,nrpPartialCyclic);

        NRPType nrpLinear = NRPType.valueOf("linear".replaceAll(" ","_").toUpperCase());
        assertEquals(NRPType.LINEAR,nrpLinear);

        NRPType nrpDoubleCyclic = NRPType.valueOf("double cyclic".replaceAll(" ","_").toUpperCase());
        assertEquals(NRPType.DOUBLE_CYCLIC,nrpDoubleCyclic);

        NRPType nrpCyclic = NRPType.valueOf("cyclic".replaceAll(" ","_").toUpperCase());
        assertEquals(NRPType.CYCLIC,nrpCyclic);

        NRPType nrpBranched = NRPType.valueOf("branched".replaceAll(" ","_").toUpperCase());
        assertEquals(NRPType.BRANCHED,nrpBranched);

        NRPType nrpOther = NRPType.valueOf("other".replaceAll(" ","_").toUpperCase());
        assertEquals(NRPType.OTHER,nrpOther);
    }

}