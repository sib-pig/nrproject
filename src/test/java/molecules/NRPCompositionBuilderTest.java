package molecules;

import molecules.Bond;
import molecules.Monomer;
import molecules.NRPCompositionBuilder;
import molecules.norine.NorineMonomerStore;
import molecules.norine.NorineNRPStore;
import molecules.norine.NorineResidueStore;
import org.expasy.mzjava.core.mol.Composition;
import org.jgrapht.graph.SimpleGraph;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by ericart on 22.02.2016.
 */

public class NRPCompositionBuilderTest {
    @Test
    public void testCreateNRPComposition1() throws Exception {

        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRP nrp = new NRPBuilder().createNRP(norineNRPStore.getNRP("NOR00209"));

        NRPCompositionBuilder nrpCompositionBuilder = new NRPCompositionBuilder();
        Composition calculatedComposition =nrpCompositionBuilder.createNRPComposition(norineNRPStore.getNRP("NOR00209"),nrp.getSimpleGraph());

        //composition "NOR00209"
        Composition norineComposition = Composition.parseComposition("C12H18N2O5");

        assertEquals(norineComposition,calculatedComposition);

    }

    @Test
    public void testCreateNRPComposition2() throws Exception {

        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRP nrp = new NRPBuilder().createNRP(norineNRPStore.getNRP("NOR00209"));

        String smilesNOR00209 = "CC(N)C(=O)NC(CC1CCC(=O)C2OC12)C(=O)O";

        NRPCompositionBuilder nrpCompositionBuilder = new NRPCompositionBuilder();
        Composition calculatedComposition =nrpCompositionBuilder.createNRPComposition(null,smilesNOR00209,nrp.getSimpleGraph());

        //composition "NOR00209"
        Composition norineComposition = Composition.parseComposition("C12H18N2O5");
        assertEquals(norineComposition,calculatedComposition);

    }

    @Test (expected = IllegalStateException.class)
    public void testCreateNRPComposition3() throws Exception {

        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRP nrp = new NRPBuilder().createNRP(norineNRPStore.getNRP("NOR00209"));

        //different smiles
        String smilesNOR00638 = "NCCCC(N)CC(=O)NCC1NC(=O)C(CO)NC(=O)C(N)CNC(=O)C(NC(=O)C(=CNC(N)=O)NC1(=O))C2CCN=C(N)N2";

        NRPCompositionBuilder nrpCompositionBuilder = new NRPCompositionBuilder();
        nrpCompositionBuilder.createNRPComposition(null,smilesNOR00638,nrp.getSimpleGraph());

    }



    @Test(expected = IllegalStateException.class)
    public void testCreateNRPComposition4() throws Exception {

        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRP nrp = new NRPBuilder().createNRP(norineNRPStore.getNRP("NOR00209"));
        //different smiles
        String smilesNOR00638 = "NCCCC(N)CC(=O)NCC1NC(=O)C(CO)NC(=O)C(N)CNC(=O)C(NC(=O)C(=CNC(N)=O)NC1(=O))C2CCN=C(N)N2";

        NRPCompositionBuilder nrpCompositionBuilder = new NRPCompositionBuilder();
        nrpCompositionBuilder.createNRPComposition("C12H18N2O5",smilesNOR00638,nrp.getSimpleGraph());

    }

    @Test(expected = IllegalStateException.class)
    public void testCreateNRPComposition5() throws Exception {


        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        NRP nrp = new NRPBuilder().createNRP(norineNRPStore.getNRP("NOR00209"));

        String smilesNOR00209 = "CC(N)C(=O)NC(CC1CCC(=O)C2OC12)C(=O)O";

        NRPCompositionBuilder nrpCompositionBuilder = new NRPCompositionBuilder();
        //different formula
        nrpCompositionBuilder.createNRPComposition("C12H18N2O4",smilesNOR00209,nrp.getSimpleGraph());

    }





}