package molecules;

import molecules.Monomer;
import molecules.norine.NorineMonomerStore;
import molecules.norine.NorineNRPStore;
import molecules.norine.NorineResidue;
import molecules.norine.NorineResidueStore;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.junit.Test;
import org.openscience.cdk.exception.CDKException;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Created by ericart on 11.11.2015.
 */

public class MonomerTest {


    @Test
    public void testgetMonomerSymbol () throws IOException, CDKException {

        NorineMonomerStore norineMonomerStore = NorineMonomerStore.getInstance();
        NorineResidue residue=NorineResidueStore.getInstance().getAllResidues().get(0);
        Monomer nodebLys = new Monomer(norineMonomerStore.getMonomer(residue.getMono()), residue,1);
        assertEquals(residue.getMono(), nodebLys.getMonomerSymbol());
    }

    @Test
    public void testGetMonomer () throws IOException, CDKException {
        NorineMonomerStore norineMonomerStore = NorineMonomerStore.getInstance();
        NorineResidue residue=NorineResidueStore.getInstance().getAllResidues().get(0);
        Monomer nodebLys = new Monomer(norineMonomerStore.getMonomer(residue.getMono()), residue,1);
        assertEquals(norineMonomerStore.getMonomer(residue.getMono()), nodebLys.getNorineMonomer());

    }

    @Test
    public void testGetResidue() throws IOException, CDKException  {
        NorineMonomerStore norineMonomerStore = NorineMonomerStore.getInstance();
        NorineResidue residue=NorineResidueStore.getInstance().getAllResidues().get(0);
        Monomer nodebLys = new Monomer(norineMonomerStore.getMonomer(residue.getMono()),residue,1);
        assertEquals(residue, nodebLys.getNorineResidue());

    }

    @Test
    public void testGetComposition() throws IOException, CDKException  {
        NorineMonomerStore norineMonomerStore = NorineMonomerStore.getInstance();
        NorineResidue residue=NorineResidueStore.getInstance().getAllResidues().get(0);
        Monomer nodebLys = new Monomer(norineMonomerStore.getMonomer(residue.getMono()), residue,1);
        assertEquals(residue.getComposition(), nodebLys.getComposition());

    }

    @Test
    public void testGetIndex() throws IOException, CDKException  {
        NorineMonomerStore norineMonomerStore = NorineMonomerStore.getInstance();
        NorineResidue residue=NorineResidueStore.getInstance().getAllResidues().get(0);
        Monomer nodebLys = new Monomer(norineMonomerStore.getMonomer(residue.getMono()), residue,1);
        assertEquals(1, nodebLys.getIndex());

    }

    @Test
    public void testToString() throws IOException, CDKException {
        NorineMonomerStore norineMonomerStore = NorineMonomerStore.getInstance();
        NorineResidue residue=NorineResidueStore.getInstance().getAllResidues().get(0);
        Monomer nodebLys = new Monomer(norineMonomerStore.getMonomer(residue.getMono()),residue,1);
        assertEquals(residue.getMono(), nodebLys.toString());
    }


}
