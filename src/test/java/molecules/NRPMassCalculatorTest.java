package molecules;

import org.expasy.mzjava.core.mol.AtomicSymbol;
import org.expasy.mzjava.core.mol.Composition;
import org.junit.Test;


import static org.junit.Assert.*;

/**
 * Created by ericart on 09.06.2016.
 */
public class NRPMassCalculatorTest {

    @Test
    public void testCalculateMz() throws Exception {
        Composition h = new Composition.Builder().add(AtomicSymbol.H).charge(1).build();
        assertEquals(8.82, NRPMassCalculator.calculateMz(12.32,h,13.4,8.9),0.01);
        //System.out.println(c.getMolecularMass());
    }
}