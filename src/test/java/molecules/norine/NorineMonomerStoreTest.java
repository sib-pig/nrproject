package molecules.norine;

import molecules.norine.NorineMonomer;
import molecules.norine.NorineMonomerStore;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ericart on 14.12.2015.
 */
public class NorineMonomerStoreTest {

    @Test
    public void testGetInstance() throws Exception {
        NorineMonomerStore norineMonomerStore = NorineMonomerStore.getInstance();
        assertNotNull(norineMonomerStore);
        assertEquals(norineMonomerStore.getClass(),NorineMonomerStore.class);
    }

    @Test
    public void testGetMonomer() throws Exception {
        NorineMonomerStore norineMonomerStore = NorineMonomerStore.getInstance();
        assertNotNull(norineMonomerStore.getMonomer("bMe-Asp"));
        assertEquals(norineMonomerStore.getMonomer("bMe-Asp").getClass(), NorineMonomer.class);
    }

    @Test(expected=NullPointerException.class)
    public void testGetMonomer2() throws Exception {
        NorineMonomerStore norineMonomerStore = NorineMonomerStore.getInstance();
        norineMonomerStore.getMonomer("qqq");
    }

    @Test
    public void testGetAllMonomers() throws Exception {
        NorineMonomerStore norineMonomerStore = NorineMonomerStore.getInstance();
        assertNotNull(norineMonomerStore.getAllMonomers());
        assertEquals(norineMonomerStore.getAllMonomers().get(0).getClass(), NorineMonomer.class);
    }
}