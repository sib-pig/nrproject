package molecules.norine;

import molecules.norine.NorineNRP;
import molecules.norine.NorineNRPStore;
import org.junit.Test;
import static org.junit.Assert.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ericart on 11.11.2015.
 */

public class NorineNRPTest {

    @Test
    public void testgetId() throws IOException, IllegalAccessException, InstantiationException {

        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        assertEquals("NOR00001", norineNRPStore.getNRP("NOR00001").getId());
        assertEquals("NOR00001", norineNRPStore.getNRP("NOR00001").general.getId());
    }

    @Test
    public void testgetName() throws IOException, IllegalAccessException, InstantiationException {

        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        assertEquals("daptomycin", norineNRPStore.getNRP("NOR00001").getName());
        assertEquals("daptomycin", norineNRPStore.getNRP("NOR00001").general.getName());
    }

    @Test
    public void testgetSmiles() throws IOException, IllegalAccessException, InstantiationException {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        assertEquals("CCCCCCCCCC(=O)NC(Cc1c[nH]c2ccccc12)C(=O)NC(CC(N)=O)C(=O)NC(CC(=O)O)C(=O)NC4C(C)OC(=O)C(CC(=O)c3ccccc3(N))NC(=O)C(NC(=O)C(CO)NC(=O)CNC(=O)C(CC(=O)O)NC(=O)C(C)NC(=O)C(CC(=O)O)NC(=O)C(CCCN)NC(=O)CNC4(=O))C(C)CC(=O)O", norineNRPStore.getNRP("NOR00001").getSmiles());
        assertEquals("CCCCCCCCCC(=O)NC(Cc1c[nH]c2ccccc12)C(=O)NC(CC(N)=O)C(=O)NC(CC(=O)O)C(=O)NC4C(C)OC(=O)C(CC(=O)c3ccccc3(N))NC(=O)C(NC(=O)C(CO)NC(=O)CNC(=O)C(CC(=O)O)NC(=O)C(C)NC(=O)C(CC(=O)O)NC(=O)C(CCCN)NC(=O)CNC4(=O))C(C)CC(=O)O", norineNRPStore.getNRP("NOR00001").structure.getSmiles());
    }

    @Test
    public void testgetLinks() throws Exception {
        String pubchemId=null;
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        ArrayList<NorineNRP.Link> links= norineNRPStore.getNRP("NOR00001").getLinks();
        for (NorineNRP.Link l : links){
            if (l.getName_db().equals("PubChem")){
                pubchemId=l.getAcc();
            }
        }
        assertEquals("16129629",pubchemId);
        assertEquals(4,links.size(),0);
    }


    //test for the inner class GENERAL
    @Test
    public void testGeneral() throws IOException, IllegalAccessException, InstantiationException {

        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        assertNotNull(norineNRPStore.getNRP("NOR00001").general);
    }

    @Test
    public void testgetFamily() throws IOException, IllegalAccessException, InstantiationException {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        assertEquals("daptomycin", norineNRPStore.getNRP("NOR00001").general.getFamily());
    }

    @Test
    public void testgetSyno() throws IOException, IllegalAccessException, InstantiationException {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        assertEquals("A21978C, cubicin", norineNRPStore.getNRP("NOR00001").general.getSyno());
    }

    @Test
    public void testgetCategory() throws IOException, IllegalAccessException, InstantiationException {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        assertEquals("lipopeptide", norineNRPStore.getNRP("NOR00001").general.getCategory());
    }

    @Test
    public void testgetFormula() throws IOException, IllegalAccessException, InstantiationException {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        assertEquals("C72H101N17O26",norineNRPStore.getNRP("NOR00001").general.getFormula());
    }

    @Test
    public void testgetMW() throws IOException, IllegalAccessException, InstantiationException {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        assertEquals(1620.6707, norineNRPStore.getNRP("NOR00001").general.getMW(), 0);
    }

    @Test
    public void testgetStatus() throws IOException, IllegalAccessException, InstantiationException {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        assertEquals("curated", norineNRPStore.getNRP("NOR00001").general.getStatus());
    }

    //test for the inner class STRUCTURE

    @Test
    public void testStructure() throws IOException, IllegalAccessException, InstantiationException {

        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        assertNotNull(norineNRPStore.getNRP("NOR00001").structure);
    }


    @Test
    public void testgetType() throws IOException, IllegalAccessException, InstantiationException {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        assertEquals("partial cyclic", norineNRPStore.getNRP("NOR00001").structure.getType());
    }

    @Test
    public void testgetSize() throws IOException, IllegalAccessException, InstantiationException {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        assertEquals(14,norineNRPStore.getNRP("NOR00001").structure.getSize());
    }

    @Test
    public void testgetGraph() throws IOException, IllegalAccessException, InstantiationException {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        assertEquals("C10:0,Trp,D-Asn,Asp,Thr,Gly,Orn,Asp,D-Ala,Asp,Gly,D-Ser,3Me-Glu,Kyn@1@0,2@1,3@2,4@3,5,13@4,6@5,7@6,8@7,9@8,10@9,11@10,12@11,13@4,12", norineNRPStore.getNRP("NOR00001").structure.getGraph());
    }

    @Test
    public void testgetSmilesVerified() throws Exception {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        assertTrue(norineNRPStore.getNRP("NOR00001").structure.getSmilesVerified());
    }

    @Test
    public void testgetResidues() throws Exception {

        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        //List<String> residuesList = Arrays.asList("3787", "1687", "3301", "3779", "3779", "621", "3301", "2165", "2009", "377", "3310", "793", "3301", "2231");
        //assertTrue(residuesList.equals(norineNRPStore.getNRP("NOR00001").structure.getResidues()));
        assertEquals(14,norineNRPStore.getNRP("NOR00001").structure.getResidues().size());
    }

    @Test
    public void testgetResiduesEdges() throws Exception {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        List<int[]> edgesList = Arrays.asList(
                new int[]{0, 12},
                new int[]{0, 3},
                new int[]{0, 9},
                new int[]{1, 7},
                new int[]{2, 13},
                new int[]{2, 4},
                new int[]{3, 8},
                new int[]{4, 10},
                new int[]{5, 7},
                new int[]{5, 12},
                new int[]{6, 13},
                new int[]{6, 8},
                new int[]{9, 11},
                new int[] {10, 11});

        assertEquals(edgesList.size(),norineNRPStore.getNRP("NOR00001").structure.getResiduesEdges().size());

        for ( int i=1; i<edgesList.size(); i++){

            assertTrue(Arrays.equals(edgesList.get(i), norineNRPStore.getNRP("NOR00001").structure.getResiduesEdges().get(i).getIdxs()));

        }

    }

    @Test
    public void testgetCorrectness() throws Exception {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        assertEquals(1,norineNRPStore.getNRP("NOR00001").structure.getCorrectness(),0);

    }

    @Test
    public void testgetCoverage() throws Exception {
        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        assertEquals(1,norineNRPStore.getNRP("NOR00001").structure.getCoverage(),0);

    }
//add setters???

}






