package molecules.norine;

import molecules.norine.NorineNRP;
import molecules.norine.NorineNRPChecker;
import molecules.norine.NorineNRPStore;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

/**
 * Created by ericart on 12.03.2016.
 */
public class NorineNRPCheckerTest {


    @Test(expected = NullPointerException.class)
    public void testNonNullResidues1() throws Exception {
        //example with peptide NOT having smiles
        NorineNRP nonSmilesNRP = NorineNRPStore.getInstance().getNRP("NOR00774");
        NorineNRPChecker norineNRPChecker = new NorineNRPChecker(nonSmilesNRP);
        norineNRPChecker.nonNullResidues();
    }

    @Test
    public void testNonNullResidues2() throws Exception {
        //example with peptide having smiles
        NorineNRP nonSmilesNRP = NorineNRPStore.getInstance().getNRP("NOR00211");
        NorineNRPChecker norineNRPChecker = new NorineNRPChecker(nonSmilesNRP);
        norineNRPChecker.nonNullResidues();
    }


    @Test
    public void testCheckResiduesCorrectness1() throws Exception {
        //example with peptide NOT having complete correctness
        NorineNRP norineNRP = NorineNRPStore.getInstance().getNRP("NOR00211");
        NorineNRPChecker norineNRPChecker = new NorineNRPChecker(norineNRP);
        ByteArrayOutputStream outContent =new ByteArrayOutputStream();
        System.setErr(new PrintStream(outContent));
        norineNRPChecker.checkResiduesCorrectness();
        //we should get an error message so the length will be longer than 0
        assertNotSame(0,outContent.toByteArray().length);

    }
    @Test
    public void testCheckResiduesCorrectness2() throws Exception {
        //example with peptide having complete correctness
        NorineNRP norineNRP = NorineNRPStore.getInstance().getNRP("NOR00485");
        NorineNRPChecker norineNRPChecker = new NorineNRPChecker(norineNRP);
        ByteArrayOutputStream outContent =new ByteArrayOutputStream();
        System.setErr(new PrintStream(outContent));
        norineNRPChecker.checkResiduesCorrectness();
        assertEquals(0,outContent.toByteArray().length);
    }

    @Test
    public void testCheckResiduesCoverage1() throws Exception {
        //example with peptide NOT having complete coverage(it can change when we do updates, if some coverages are improved)
        NorineNRP norineNRP = NorineNRPStore.getInstance().getNRP("NOR00412");
        NorineNRPChecker norineNRPChecker = new NorineNRPChecker(norineNRP);
        ByteArrayOutputStream outContent =new ByteArrayOutputStream();
        System.setErr(new PrintStream(outContent));
        norineNRPChecker.checkResiduesCoverage();
        //we should get an error message so the length will be longer than 0
        assertNotSame(0,outContent.toByteArray().length);

    }

    @Test
    public void testCheckResiduesCoverage2() throws Exception {
        //example with peptide having complete coverage
        NorineNRP norineNRP = NorineNRPStore.getInstance().getNRP("NOR00211");
        NorineNRPChecker norineNRPChecker = new NorineNRPChecker(norineNRP);
        ByteArrayOutputStream outContent =new ByteArrayOutputStream();
        System.setErr(new PrintStream(outContent));
        norineNRPChecker.checkResiduesCoverage();
        assertEquals(0,outContent.toByteArray().length);

    }


    @Test
    public void testCheckSmilesVerified1() throws Exception {
        //example with peptide NOT having a verified Smiles
        NorineNRP norineNRP = NorineNRPStore.getInstance().getNRP("NOR00369");
        NorineNRPChecker norineNRPChecker = new NorineNRPChecker(norineNRP);
        ByteArrayOutputStream outContent =new ByteArrayOutputStream();
        System.setErr(new PrintStream(outContent));
        norineNRPChecker.checkSmilesVerified();
        //we should get an error message so the length will be longer than 0
        assertNotSame(0,outContent.toByteArray().length);

    }
    @Test
    public void testCheckSmilesVerified2() throws Exception {
        //example with peptide having a verified Smiles
        NorineNRP norineNRP = NorineNRPStore.getInstance().getNRP("NOR00601");
        NorineNRPChecker norineNRPChecker = new NorineNRPChecker(norineNRP);
        ByteArrayOutputStream outContent =new ByteArrayOutputStream();
        System.setErr(new PrintStream(outContent));
        norineNRPChecker.checkSmilesVerified();
        assertEquals(0,outContent.toByteArray().length);

    }
}