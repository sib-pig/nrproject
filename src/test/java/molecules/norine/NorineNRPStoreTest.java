package molecules.norine;

import molecules.norine.NorineNRP;
import molecules.norine.NorineNRPStore;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ericart on 15.12.2015.
 */
public class NorineNRPStoreTest {

    @Test
    public void testGetInstance() throws Exception {

        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        assertNotNull(norineNRPStore);
        assertEquals(norineNRPStore.getClass(),NorineNRPStore.class);
    }

    @Test
    public void testGetNRP() throws Exception {

        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        assertNotNull(norineNRPStore.getNRP("NOR00228"));
        assertEquals(norineNRPStore.getNRP("NOR00228").getClass(), NorineNRP.class);

    }

    @Test(expected=NullPointerException.class)
    public void testGetNRP2() throws Exception {

        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        norineNRPStore.getNRP("qqq");
    }

    @Test
    public void testGetAllNRPs() throws Exception {

        NorineNRPStore norineNRPStore = NorineNRPStore.getInstance();
        assertNotNull(norineNRPStore.getAllNRPs());
        assertEquals(norineNRPStore.getAllNRPs().get(0).getClass(), NorineNRP.class);
    }


}