package molecules.norine;

import molecules.norine.NorineResidue;
import molecules.norine.NorineResidueStore;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ericart on 08.03.2016.
 */
public class NorineResidueStoreTest {

    @Test
    public void testGetInstance() throws Exception {
        NorineResidueStore norineResidueStore = NorineResidueStore.getInstance();
        assertNotNull(norineResidueStore);
        assertEquals(norineResidueStore.getClass(),NorineResidueStore.class);
    }

    @Test
    public void testGetResidue() throws Exception {
        NorineResidueStore norineResidueStore = NorineResidueStore.getInstance();
        assertNotNull(norineResidueStore.getResidue("108"));
        assertEquals(norineResidueStore.getResidue("108").getClass(), NorineResidue.class);
    }

    @Test(expected=NullPointerException.class)
    public void testGetResidue2() throws Exception {
        NorineResidueStore norineResidueStore = NorineResidueStore.getInstance();
        norineResidueStore.getResidue("qqq");
    }

    @Test
    public void testGetAllResidues() throws Exception {
        NorineResidueStore norineResidueStore = NorineResidueStore.getInstance();
        assertNotNull(norineResidueStore.getAllResidues());
        assertEquals(norineResidueStore.getAllResidues().get(0).getClass(), NorineResidue.class);
    }
}