package molecules.norine;

import molecules.norine.NorineMonomerStore;
import org.expasy.mzjava.core.mol.Composition;
import org.junit.Test;
import java.io.IOException;
import static org.junit.Assert.*;

/**
 * Created by ericart on 11.11.2015.
 */

public class NorineMonomerTest {

    @Test
    public void testgetId() throws IOException {

        NorineMonomerStore norineMonomerStore = NorineMonomerStore.getInstance();
        assertEquals("bMe-Phe", norineMonomerStore.getMonomer("bMe-Phe").getCode());
    }

    @Test
    public void testgetNameAA() throws IOException {
        NorineMonomerStore norineMonomerStore = NorineMonomerStore.getInstance();
        assertEquals("beta-methyl-phenylalanine", norineMonomerStore.getMonomer("bMe-Phe").getNameAA());
    }

    @Test
    public void testgetMolecularFormula() throws IOException {
        NorineMonomerStore norineMonomerStore = NorineMonomerStore.getInstance();
        assertEquals("C10H13NO2", norineMonomerStore.getMonomer("bMe-Phe").getMolecularFormula());
    }

    @Test
    public void testgetPubchem() throws IOException {

        NorineMonomerStore norineMonomerStore = NorineMonomerStore.getInstance();
        assertNull(norineMonomerStore.getMonomer("bMe-Phe").getPubchem());

    }

    @Test
    public void testgetMolecularWeight() throws IOException {

        NorineMonomerStore norineMonomerStore = NorineMonomerStore.getInstance();
        assertEquals(179.2291, norineMonomerStore.getMonomer("bMe-Phe").getMolecularWeight(), 0);
    }

    @Test
    public void testgetSmiles() throws IOException {

        NorineMonomerStore norineMonomerStore = NorineMonomerStore.getInstance();
        assertEquals("C1=CC=C(C=C1)C(C)C(C(=O)O)N", norineMonomerStore.getMonomer("bMe-Phe").getSmiles());
    }

    @Test
    public void testgetIsomeric() throws IOException {

        NorineMonomerStore norineMonomerStore = NorineMonomerStore.getInstance();
        assertNull(norineMonomerStore.getMonomer("bMe-Phe").getIsomeric());
    }

    @Test
    public void testtoString() throws IOException {
        NorineMonomerStore norineMonomerStore = NorineMonomerStore.getInstance();
        assertEquals("bMe-Phe", norineMonomerStore.getMonomer("bMe-Phe").toString());
    }

    @Test
    public void testComposition() throws Exception {
        NorineMonomerStore norineMonomerStore = NorineMonomerStore.getInstance();
        assertEquals(Composition.parseComposition("C10H13NO2"), norineMonomerStore.getMonomer("bMe-Phe").getComposition());

    }


}
