package molecules.norine;

import molecules.norine.NorineResidue;
import molecules.norine.NorineResidueStore;
import org.junit.Test;
import static org.junit.Assert.*;


/**
 * Created by ericart on 21.02.2016.
 */
public class NorineResidueTest {

    @Test
    public void testGetMono() throws Exception {
        NorineResidueStore norineResidueStore = NorineResidueStore.getInstance();
        NorineResidue norineResidue = norineResidueStore.getResidue("188");
        assertEquals("NFo-Gln", norineResidue.getMono());

    }

    @Test
    public void testGetSmarts() throws Exception {
        NorineResidueStore norineResidueStore = NorineResidueStore.getInstance();
        NorineResidue norineResidue = norineResidueStore.getResidue("188");
        assertEquals("[H]NC(=O)C([H])([H])C([H])([H])C([H])(C=O)NC([H])=O", norineResidue.getSmarts());

    }

    @Test
    public void testGetId() throws Exception {
        NorineResidueStore norineResidueStore = NorineResidueStore.getInstance();
        NorineResidue norineResidue = norineResidueStore.getResidue("188");
        assertEquals("188", norineResidue.getId());
    }

    @Test
    public void testGetComposition() throws Exception {
        NorineResidueStore norineResidueStore = NorineResidueStore.getInstance();
        NorineResidue norineResidue = norineResidueStore.getResidue("188");
        assertEquals("C6H7N2O3",  norineResidue.getComposition().getFormula());
    }
}