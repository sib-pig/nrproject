package molecules.modification;

import org.expasy.mzjava.core.mol.Composition;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ericart on 02.05.2016.
 */
public class ModificationTest {

    @Test
    public void testGetComposition() throws Exception {
        Modification modification = new Modification(Composition.parseComposition("H"), Modification.ModificationType.VARIABLE);
        assertEquals(Composition.parseComposition("H"),modification.getComposition());
    }

    @Test
    public void testGetModificationType() throws Exception {
        Modification modification = new Modification(Composition.parseComposition("H"), Modification.ModificationType.VARIABLE);
        assertEquals(Modification.ModificationType.VARIABLE,modification.getModificationType());
    }
}