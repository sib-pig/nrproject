package molecules.modification;

import molecules.Monomer;
import molecules.norine.NorineMonomerStore;
import molecules.norine.NorineResidueStore;
import ms.spectrum.NRPPeaksGenerator;
import org.expasy.mzjava.core.mol.Composition;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ericart on 02.05.2016.
 */
public class ModifiedMonomerTest {
/*
    @Test
    public void testGetMonomer() throws Exception {
        NorineMonomerStore norineMonomerStore = NorineMonomerStore.getInstance();
        NorineResidueStore norineResidueStore = NorineResidueStore.getInstance();
        Monomer nodebLys = new Monomer(norineMonomerStore.getMonomer(norineResidueStore.getResidue("2191").getMono()), norineResidueStore.getResidue("2191"),1);
        Composition composition = Composition.parseComposition("H");
        NRPPeaksGenerator.ModifiedMonomer modifiedMonomer = new NRPPeaksGenerator.ModifiedMonomer(nodebLys, new Modification(composition, Modification.ModificationType.FIX));
        assertEquals(nodebLys,modifiedMonomer.getMonomer());
    }

    @Test
    public void testGetModification() throws Exception {
        NorineMonomerStore norineMonomerStore = NorineMonomerStore.getInstance();
        NorineResidueStore norineResidueStore = NorineResidueStore.getInstance();
        Monomer nodebLys = new Monomer(norineMonomerStore.getMonomer(norineResidueStore.getResidue("2191").getMono()), norineResidueStore.getResidue("2191"),1);
        Modification modification =new Modification(Composition.parseComposition("H"), Modification.ModificationType.FIX);
        NRPPeaksGenerator.ModifiedMonomer modifiedMonomer = new NRPPeaksGenerator.ModifiedMonomer(nodebLys, modification);
        assertEquals(modification,modifiedMonomer.getModification());
    }
    */
}